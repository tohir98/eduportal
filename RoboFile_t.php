<?php

use packager\PackagerTasks;
use Robo\Result;
use Robo\Task\SshExecTask;
use Robo\Tasks;

//crappy codeIgniter check
if (!defined('BASEPATH')) {
    define('BASEPATH', __DIR__ . '/system');
}

//crappy codeIgniter check
if (!defined('APP_ROOT')) {
    define('APP_ROOT', __DIR__ . DIRECTORY_SEPARATOR);
}

define('ENVIRONMENT', getenv('EP_ENV') ? : 'development');
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT);
if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'development':
            ini_set('display_errors', 'On');
            break;
        case 'testing':
        case 'production':
            ini_set('display_errors', 'Off');
            break;

        default:
            exit('The application environment is not set correctly.');
    }
}

define('FCPATH', __DIR__);
require_once 'dbvc/DBMigration.php';
require_once 'application/config/constants.php';
require_once 'application/libraries/MigrateToS3.php';

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends Tasks {

    protected static $deploymentConfig = array(
        'testing' => array(
            'server' => ['159.203.103.194'],
        ),
//        'production' => array(
//            'user' => 'wwwuser',
//            'server' => ['159.203.103.194'],
//            'branch' => 'release'
//        )
    );
    protected static $defaultDeploymentConfig = array(
        'app_path' => '~/html',
        'port' => 22,
        'branch' => 'master',
        'user' => 'portaluser',
        'composer_path' => null,
    );

    // define public methods as commands

    /**
     * Perform database migration,
     * for details on database migration
     * @param string $dbGroup the group to migrate.
     * @option $start int the start version
     * @option $mode the mode in which to run the migration
     *                  Available modes include interactive|non_interactive|dryrun
     * @return int 0 on success any other value otherwise
     */
    public function dbMigrate($dbGroup = 'default', $opts = ['start' => 100, 'mode' => DBMigrationMode::NON_INTERACTIVE]) {
        $mode = $opts['mode'];
        $startVersion = $opts['start'];

        return include('dbvc/migration.php');
    }

    protected function getDeploymentConfig($env) {
        return array_merge(self::$defaultDeploymentConfig, (array) self::$deploymentConfig[$env]);
    }

    protected function taskDeploy($env, $identityFile = '', array $options) {
        $config = $this->getDeploymentConfig($env);

        if (!$options['skip-push']) {
            $this->say('Auto updating your working tree..');
            $result = $this->taskGitStack()
                    ->add('-A')
                    ->commit('"Autoupdate before deployment"')
                    ->pull('origin', $config['branch'])
                    ->run();
            /* @var $result Result */
            if (!$result) {
                $this->yell('Git update failed!');
                return;
            } else {
                try {
                    //migrate db
                    $this->dbMigrate();
                    //$this->dbMigrate(1, 'hired');
                } catch (Exception $ex) {
                    $this->yell('DB Migration failed: [' . $ex->getMessage() . ']');
                    return;
                }
                $result = $this->taskGitStack()
                        ->push('origin', $config['branch'])
                        ->run();

                if (!$result || !$result->wasSuccessful()) {
                    $this->yell('Git update failed!');
                    return;
                }
            }
        } else {
            $this->say('Skipping push');
        }



        $servers = $config['server'];
        if (empty($servers)) {
            $this->yell('Deployment fail, no server specified for : [' . $env . ']');
            return -1;
        }
        if (!is_array($servers)) {
            $servers = [$servers];
        }

        $serverTasks = array();
        //change to app folder
        $serverTasks[] = "export EP_ENV='{$env}'";

        $serverTasks[] = "cd {$config['app_path']}";

        //check out branh
        $serverTasks[] = $this->taskGitStack()
                ->checkout($config['branch'])
                ->pull();

        //run composer install 
        //run db:migrate on default
        $serverTasks[] = 'vendor/bin/robo db:migrate';
        //$serverTasks[] = $this->taskComposerInstall($config['composer_path'] ? : null);
        $serverTasks[] = 'composer install';
        //run composer update
        //$serverTasks[] = $this->taskComposerUpdate($config['composer_path'] ? : null);
        $serverTasks[] = 'composer update';
        //$serverTasks[] = 'whoami';
        //@TODO run other commands, like css compressiion, js, etc
        if (!$identityFile) {
            $identityFile = $this->getIdentityFile();
        }

        if (!$identityFile || !file_exists($identityFile)) {
            $this->yell('No valid identity file was found!');
            return;
        }

        foreach ($servers as $server) {
            $parts = explode(':', $server);
            $port = count($parts) > 1 ? $parts[1] : $config['port'];
            $this->say('Deploying to server: ' . $parts[0] . ' via port : ' . $port);
            $taskSsh = $this->taskSshExec($parts[0], $options['user'] ? : $config['user']);

            /* @var $taskSsh SshExecTask */
            $taskSsh->port($port)
                    ->identityFile($identityFile)
                    ->printed(true)
            ;

            //add tasks to run on server
            foreach ($serverTasks as $serverCmd) {
                $taskSsh->exec($serverCmd);
            }

            //fix for windows:
            $command = str_replace('\'', '"', $taskSsh->getCommand());
            $this->say('Converted command to : `' . $command . '`');
            $result = $this->taskExec($command)->run();
            //run as raw command

            if (!$result || !$result->wasSuccessful()) {
                $this->yell('Deployment failed to server!');
                return false;
            } else {
                $this->say('Deployment done');
            }
        }

        return true;
    }

    /**
     * 
     * Deploys application to testing servers.
     * @param string $identityFile  the path to the identity file for deployment.
     * @option $skip-push skips commit/pull/push cycle before commit
     * @option $user the user to use when logging to SSH
     */
    public function deployTesting($identityFile = null, $options = ['skip-push' => true, 'user' => 'wwwuser']) {
        return $this->taskDeploy('testing', $identityFile, $options);
    }

    /**
     * 
     * Deploys application to production servers.
     * @param string $identityFile  the path to the identity file for deployment.
     * @option $skip-push skips commit/pull/push cycle before commit
     * @option $user the user to use when logging to SSH
     */
    public function deployProduction($identityFile = null, $options = ['skip-push' => true, 'user' => 'wwwuser']) {
        return $this->taskDeploy('production', $identityFile, $options);
    }

    protected function getHomeDir() {
        return strncasecmp(PHP_OS, 'WIN', 3) == 0 ? getenv('USERPROFILE') : getenv('HOME');
    }

    protected function getIdentityFile() {
        $homeDir = $this->getHomeDir();
        $base = $homeDir . DIRECTORY_SEPARATOR . '.ssh/' . DIRECTORY_SEPARATOR;
        $tries = ['id_rsa', 'id_dsa', 'deployment_key'];
        foreach ($tries as $trial) {
            $file = $base . $trial;
            if (file_exists($file)) {
                return $file;
            }
        }
        return null;
    }

    public function moveFiles() {
        $migrate = new MigrateToS3();
        $migrate->start();
    }
   
}
