var staffDirApp = angular.module('staffDir', []);

staffDirApp.controller('staffCtrl', function ($scope) {
    $scope.staff = {first_name: '', employee_id: ''}; 
    $scope.editStatus = function(employee_id, first_name){
        
        $scope.staff.first_name = first_name;
        $scope.staff.employee_id = employee_id ;
        
        $("#modal-editStatus").modal();
    };

    $scope.delete = function (href, firstName) {
        EduPortal.doConfirm({
            title: 'Confirm Staff Delete',
            message: 'Are you sure you want to delete this  ' + firstName,
            onAccept: function () {
                window.location = href;
            }
        });
    };


});

