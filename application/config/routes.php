<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['404_override'] = 'errors/page_missing';
$route['default_controller'] = 'Welcome';
//$route['default_controller'] = 'users/Users_controller';
$route['translate_uri_dashes'] = TRUE;
$route['login'] = 'users/users_controller/login';
$route['logout'] = 'users/users_controller/logout';
$route['forgot_password'] = 'users/users_controller/forgot_password';

$route['admin/dashboard'] = 'users/admin_controller/dashboard';

//Edadmin
$route['eadmin'] = 'ed/users_controller/login';
$route['eadmin/login'] = 'ed/users_controller/login';
$route['eadmin/logout'] = 'ed/users_controller/logout';
$route['eadmin/dashboard'] = 'ed/admin_controller/dashboard';

// schools
$route['eadmin/school'] = 'ed/school_controller';
$route['eadmin/school/(:any)'] = 'ed/school_controller/$1';

// employee
$route['employee'] = 'users/employees_controller';
$route['employee/(:any)'] = 'users/employees_controller/$1';
$route['employee/(:any)/(:any)'] = 'users/employees_controller/$1/$2';
$route['employee/(:any)/(:any)/(:any)'] = 'users/employees_controller/$1/$2/$3';

// Session
$route['settings/session'] = 'settings/session_controller';
$route['settings/session/(:any)'] = 'settings/session_controller/$1';
$route['settings/session/(:any)/(:any)'] = 'settings/session_controller/$1/$2';

// Term
$route['settings/term'] = 'settings/term_controller';
$route['settings/term/(:any)'] = 'settings/term_controller/$1';
$route['settings/term/(:any)/(:any)'] = 'settings/term_controller/$1/$2';

$route['settings/current_session_term'] = 'settings/session_controller/current_session_term';

$route['settings/class'] = 'settings/class_controller';
$route['settings/class/(:any)'] = 'settings/class_controller/$1';
$route['settings/class/(:any)/(:any)'] = 'settings/class_controller/$1/$2';

// Subjects
$route['settings/subject'] = 'settings/subject_controller';
$route['settings/subject/(:any)'] = 'settings/subject_controller/$1';
$route['settings/subject/(:any)/(:num)'] = 'settings/subject_controller/$1/$2';

// calendar
$route['settings/calendar'] = 'settings/calendar_controller';
$route['settings/calendar/(:any)'] = 'settings/calendar_controller/$1';
$route['settings/calendar/(:any)/(:num)'] = 'settings/calendar_controller/$1/$2';


//student 
$route['student/'] = 'users/student_controller';
$route['student/(:any)'] = 'users/student_controller/$1';
$route['student/(:any)/(:any)'] = 'users/student_controller/$1/$2';
$route['student/(:any)/(:any)/(:any)'] = 'users/student_controller/$1/$2/$3';

//Result 
$route['result'] = 'result/result_controller/index';
$route['result/(:any)'] = 'result/result_controller/$1';
$route['result/(:any)/(:any)'] = 'result/result_controller/$1/$2';
$route['result/(:any)/(:any)/(:any)'] = 'result/result_controller/$1/$2/$3';

//Communications
$route['communication/messages'] = 'communication/comm_controller/index';
$route['communication/(:any)'] = 'communication/comm_controller/$1';
$route['communication/(:any)/(:any)'] = 'communication/comm_controller/$1/$2';
$route['communication/(:any)/(:any)/(:any)'] = 'communication/comm_controller/$1/$2/$3';

// Fees
$route['fees/settings'] = 'fees/settings';