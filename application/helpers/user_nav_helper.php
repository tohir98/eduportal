<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Navigation helper
 */
if (!function_exists('build_top_nav_part')) {

    function build_top_nav_part() {
        
    }

}

if (!function_exists('build_sidebar_nav_part')) {

    function build_sidebar_nav_part($nav_menu, $path) {
        $buffer = '';

        $buffer .= '<ul class="sidebar-menu">';

        foreach ($nav_menu as $module => $item) {
            $active = strstr($path, $item['id_string']) ? 'active' : '';
            $buffer .= '<li class="' . $active . '">
                <a href="#">
                <i class="fa fa-' . nav_icon($item['id_string']) . '"></i> <span>' . $module . '</span> <i class="fa fa-angle-left pull-right"></i>
              </a>';
            if (count($item) > 0) {

                $buffer .= '<ul class="treeview-menu">';
                foreach ($item['items'] as $sub) {
                    $buffer .= '<li><a href="' . site_url($sub['module_id_string'] . '/' . $sub['perm_id_string']) . '"><i class="fa fa-circle-o"></i> ' . $sub['perm_subject'] . '</a></li>';
                }
                $buffer .= '</ul>';
            }
            $buffer .= '</li>';
        }
        $buffer .= '</ul>';
        return $buffer;
    }

}

if (!function_exists('user_type_status')) {

    function user_type_status($account_type) {
        $arr = array(
            USER_TYPE_ADMIN => "Admin",
            USER_TYPE_STAFF => "Staff",
            USER_TYPE_PARENT => "Parent",
            USER_TYPE_STUDENT => "Student",
        );

        return $arr[$account_type];
    }

}

if (!function_exists('nav_icon')) {

    function nav_icon($module) {
        $icon = '';
        if ($module === 'employee') {
            $icon = 'dashboard';
        } elseif ($module === 'student') {
            $icon = 'cogs';
        } elseif ($module === 'result') {
            $icon = 'calendar';
        } elseif ($module === 'communication') {
            $icon = 'envelope';
        } elseif ($module === 'payroll') {
            $icon = 'building-o';
        } elseif ($module === 'settings') {
            $icon = 'cog';
        } else {
            $icon = 'dashboard';
        }

        return $icon;
    }

}