<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Navigation helper
 */
if (!function_exists('build_top_nav_part')) {

    function build_top_nav_part() {
        
    }

}

if (!function_exists('build_sidebar_nav_part')) {

    function build_sidebar_nav_part($nav_menu, $user_link) {
        $buffer = '';

        $buffer .= '<ul class="sidebar-menu">';

        foreach ($nav_menu as $module => $item) {
            $buffer .= '<li><a href="#">
                <i class="fa fa-dashboard"></i> <span>' . $module . '</span> <i class="fa fa-angle-left pull-right"></i>
              </a>';
            if (count($item) > 0) {
                $buffer .= '<ul class="treeview-menu">';
                foreach ($item as $sub) {
                    $buffer .= '<li><a href="' . site_url($user_link . '/' . $sub['menu_id_string'] . '/' . $sub['perm_id_string']) . '"><i class="fa fa-circle-o"></i> ' . $sub['perm_subject'] . '</a></li>';
                }
                $buffer .= '</ul>';
            }
            $buffer .= '</li>';
        }
        $buffer .= '</ul>';
        return $buffer;
    }

}

