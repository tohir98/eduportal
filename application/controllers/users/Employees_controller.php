<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Employees_controller
 *
 * @author TOHIR
 * 
 * @property user_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 * @property employee_model $e_model Employee Model
 * @property Reference_model $ref_model Reference Model
 * @property School_model $sch_model Description
 */
class Employees_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->model('user/employee_model', 'e_model');
        $this->load->model('school/school_model', 'sch_model');
        $this->load->model('reference_model', 'ref_model');
    }

    public function add_staff() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->e_model->add_staff($this->user_auth_lib->get('school_id'), request_post_data(), $_FILES['userfile'])) {
                notify('success', 'Staff was created successfully');
            } else {
                notify('error', 'Unable to create staff at moment, Pls try again later');
            }
            redirect(site_url('/employee/staff_directory'));
        }

        $data = array(
            'states' => $this->ref_model->fetch_all_records(TBL_STATE),
            'countries' => $this->ref_model->fetch_all_records(TBL_COUNTRIES),
            'genders' => $this->ref_model->fetch_all_records(TBL_GENDER),
            'marital_statuses' => $this->ref_model->fetch_all_records(TBL_MARITAL_STATUS),
            'account_statuses' => School_model::getAccountStatuses()
        );
        $this->user_nav_lib->run_page('user/employee/add_staff', $data, 'Add New Staff | EduPortal');
    }

    public function staff_directory() {
        $this->user_auth_lib->check_login();
        $data = array(
            'employees' => $this->e_model->fetchEmployee($this->user_auth_lib->get('school_id')),
            'account_statuses' => School_model::getAccountStatuses()
        );
        $this->user_nav_lib->run_page('user/employee/staff_directory', $data, 'Staff Directory | EduPortal');
    }

    public function view_record($staff_id, $full_name) {
        $this->user_auth_lib->check_login();
        $data = array(
            'account_statuses' => School_model::getAccountStatuses(),
            'record' => $this->e_model->fetchEmployee($this->user_auth_lib->get('school_id'), $staff_id)[0]
        );
        $this->user_nav_lib->run_page('user/employee/view_record', $data, 'Staff Record | EduPortal');
    }

    public function delete_staff($staff_id) {
        if ($this->e_model->delete_staff($staff_id)) {
            notify('success', 'Staff record deleted successfully');
        } else {
            notify('success', 'Ops! An error occured while trying to delete staff record');
        }
        redirect(site_url('employee/staff_directory'));
    }

}
