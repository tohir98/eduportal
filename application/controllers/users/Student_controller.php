<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Student Controller
 *
 * @author TOHIR
 * 
 * @property user_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 * @property student_model $student_model Employee Model
 * @property Reference_model $ref_model Reference Model
 * @property School_model $sch_model Description
 * @property Mailer $mailer Description
 * @property Sms_lib $sms_lib Description
 */
class Student_controller extends CI_Controller {

    private $school_id;

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'user_nav_lib', 'mailer',
        ));
        $this->load->model('user/student_model', 'student_model');
        $this->load->model('school/school_model', 'sch_model');
        $this->load->model('reference_model', 'ref_model');

        $this->school_id = $this->user_auth_lib->get('school_id');
    }

    public function add_student() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            $student_id = $this->student_model->save_student($this->user_auth_lib->get('school_id'), request_post_data(), $_FILES['userfile']);

            if (is_numeric($student_id) && $student_id > 0) {
                notify('success', 'Student saved successfully');
                redirect(site_url('student/guardian/' . $student_id));
            } else {
                notify('error', 'Unable to save student information, pls try again');
            }
        }


        $data = array(
            'states' => $this->ref_model->fetch_all_records(TBL_STATE),
            'countries' => $this->ref_model->fetch_all_records(TBL_COUNTRIES),
            'genders' => $this->ref_model->fetch_all_records(TBL_GENDER),
            'blood_groups' => $this->ref_model->fetch_all_records(TBL_BLOOD_GROUPS),
            'marital_statuses' => $this->ref_model->fetch_all_records(TBL_MARITAL_STATUS),
            'classes' => $this->ref_model->fetch_all_records(TBL_CLASSES, ['school_id' => $this->user_auth_lib->get('school_id')]),
            'class_arms' => $this->ref_model->fetch_all_records(TBL_CLASS_ARM),
            'religions' => $this->ref_model->fetch_all_records('religion'),
            'account_statuses' => School_model::getAccountStatuses(),
            'admission_no' => $this->user_auth_lib->generateAdmissionNo()
        );
        $this->user_nav_lib->run_page('user/student/add_student', $data, 'New Student | EduPortal');
    }

    public function guardian($student_id) {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->student_model->save_guardian(request_post_data())) {
                notify('success', 'Guardian saved successfully');
                redirect(site_url('student/emergency_contact/' . $student_id));
            } else {
                notify('error', 'Unable to save guardian information, pls try again');
            }
        }

        $data = array(
            'states' => $this->ref_model->fetch_all_records(TBL_STATE),
            'countries' => $this->ref_model->fetch_all_records(TBL_COUNTRIES),
            'genders' => $this->ref_model->fetch_all_records(TBL_GENDER),
            'blood_groups' => $this->ref_model->fetch_all_records(TBL_BLOOD_GROUPS),
            'relationships' => $this->ref_model->fetch_all_records(TBL_RELATIONSHIPS),
            'marital_statuses' => $this->ref_model->fetch_all_records(TBL_MARITAL_STATUS),
            'classes' => $this->ref_model->fetch_all_records(TBL_CLASSES, ['school_id' => $this->user_auth_lib->get('school_id')]),
            'account_statuses' => School_model::getAccountStatuses()
        );
        $this->user_nav_lib->run_page('user/student/add_guardian', $data, 'Student Guardian | EduPortal');
    }

    public function view_student() {
        $this->user_auth_lib->check_login();
        $data = array(
            'students' => $this->student_model->fetchStudent($this->user_auth_lib->get('school_id')),
            'account_statuses' => School_model::getAccountStatuses()
        );

        $this->user_nav_lib->run_page('user/student/student_directory', $data, 'Student Diectory | EduPortal');
    }

    public function emergency_contact() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->student_model->save_emergency_contact(request_post_data())) {
                notify('success', 'Emergency contacts saved successfully');
                redirect(site_url('student/view_student'));
            } else {
                notify('error', 'Unable to save emergency contacts information, pls try again');
            }
        }
        $data = [];
        $this->user_nav_lib->run_page('user/student/emergency_contact', $data, 'Emergency Contact | EduPortal');
    }

    public function view_record($admission_no, $full_name) {
        $this->user_auth_lib->check_login();
        $emergency_content = $this->load->view('user/student/profile_page_contents/emergency_contacts', array(
            'e_contacts' => $this->student_model->fetch_emergency_contacts($admission_no, $this->school_id)
            ), true);

        $guardian_content = $this->load->view('user/student/profile_page_contents/guardian_details', array(
            'g_details' => $this->student_model->fetch_guardian_details($admission_no, $this->school_id)
            ), true);

        $profile_content = $this->load->view('user/student/profile_page_contents/profile', array(
            'record' => $this->student_model->fetchStudent($this->school_id, $admission_no)[0]
            ), true);

        $data = array(
            'profile_content' => $profile_content,
            'guardian_content' => $guardian_content,
            'emergency_content' => $emergency_content,
            'students' => $this->student_model->fetchStudent($this->user_auth_lib->get('school_id')),
        );
        $this->user_nav_lib->run_page('user/student/profile_page', $data, 'Student Record | EduPortal');
    }

}
