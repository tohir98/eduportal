<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of users_controller
 *
 * @category Controller
 * @author TOHIR Omoloye <otcleantech@gmail.com>
 * 
 * @property user_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 */
class Users_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'User_nav_lib'
        ));
        $this->load->helper('user_nav_helper');
    }

    public function index() {
//        echo 'kfhdjfhkdjfkd';
//        $this->load->view('welcome_message');
        $this->user_nav_lib->run_page('', '');
    }

    public function login() {
        $this->load->library('subdomain');
        $school_id = $this->subdomain->get_school_id();

        $data = array(
            'email_message' => '',
            'password_message' => '',
            'school' => $this->subdomain->get_school(),
        );

        if (request_is_post()) {
            $response = $this->user_auth_lib->login(request_post_data()['email'], request_post_data()['password'], $school_id);
            if (!empty($response) && $response['status'] == USER_STATUS_ACTIVE && $response['school_status'] == ACCOUNT_STATUS_ACTIVE) {
                $this->_redirect_user_by_type($response['account_type']);
            }
        }

        $this->load->view('user/login', $data);
    }

    private function _redirect_user_by_type($accout_type) {
        if ($accout_type == USER_TYPE_ADMIN){
            redirect(site_url('admin/dashboard'));
        }
    }

    public function forgot_password() {
        echo 'Coming soon';
    }

    public function logout() {
        // Call logout function from user_auth library
        $this->user_auth_lib->logout();
        redirect(site_url('login'), 'refresh');
    }

}

// end class : Users_controller
// end file: Users_controller.php