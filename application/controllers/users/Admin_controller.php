<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Admin_controller
 *
 * @author TOHIR
 * 
 * @property user_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 */
class Admin_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'user_nav_lib'
        ));
    }

    public function dashboard() {
        $this->user_auth_lib->check_login();
        $pageData = [];
        $this->user_nav_lib->run_page('dashboard/dashboard', $pageData);
    }

}
