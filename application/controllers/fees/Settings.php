<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Settings
 *
 * @author chuksolloh
 * @property  Settings_model $smodel 
 */

class Settings extends CI_Controller{
	//put your code here
	
	public function __construct() {
		parent::__construct();
		
		$this->load->library(array(
            'user_nav_lib', 'mailer', 'user_auth_lib', 'session'
        ));
		$this->load->helper(['notification_helper']);
		
		$this->user_auth_lib->check_login(); // Check if user is logged-in
		//Load model
		$this->load->model('fees/Settings_model', 'smodel');
	}
	
	public function index() {
		if(request_is_post()) {
			// Add new fee item
			$ret = $this->smodel->addItem($this->input->post());
			$ret > 0 ? notify('success', 'Item added successfully') : notify('error', 'Error! Failed to add new item.');
			// Redirect
			redirect(site_url('fees/settings'));
		} else {
			$this->user_nav_lib->run_page('fees/settings', array(
				'items' => $this->smodel->pullItems() // All listed fee items
			), 'Fess - Settings');
		}
	}
	
	public function updateItem() {
		if(request_is_post()) {		//var_dump($this->input->post()); exit;
			$ret = $this->smodel->editItem($this->input->post());
			$ret > 0 ? notify('success', 'Item updated successfully') : notify('error', 'Error! Failed to update item.');
			// Redirect
			redirect(site_url('fees/settings'));
		}
	}
	
	public function deleteItem() {
		if(request_is_post()) {
			$ret = $this->smodel->deleteItem($this->input->post());
			$ret > 0 ? notify('success', 'Item deleted successfully') : notify('error', 'Error! Failed to delete item.');
			// Redirect
			redirect(site_url('fees/settings'));
		}
	}
}
