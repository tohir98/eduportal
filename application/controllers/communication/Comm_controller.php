<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Comm_controller
 *
 * @author TOHIR
 * @property User_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 * @property Comm_model $comm_model Description
 */
class Comm_controller extends CI_Controller {

    private $school_id;

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'user_nav_lib', 'mailer', 'sms_lib'
        ));

        $this->load->model('reference_model', 'ref_model');
        $this->load->model('communication/comm_model', 'comm_model');

        $this->school_id = $this->user_auth_lib->get('school_id');
    }

    public function index() {
        $this->user_auth_lib->check_login();

        $data = array(
            'emails' => $this->comm_model->fetchMessages($this->school_id),
            'smses' => $this->comm_model->fetchSms($this->school_id)
        );
        $this->user_nav_lib->run_page('communication/messages', $data, 'Communications | EduPortal');
    }

    public function send_sms() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->sms_lib->send_sms(array_merge(request_post_data(), ['module_id' => 1]))) {
                notify('success', 'SMS sent successfully');
            } else {
                notify('error', 'Unable to send SMS at moment, pls try again later');
            }
            redirect($this->input->server('HTTP_REFERER') ? : site_url('/student/view_student'));
        }
    }

    public function send_email() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {
            if ($this->comm_model->process_email(request_post_data())) {
                notify('success', 'Email sent successfully');
            } else {
                notify('error', 'Unable to send email at moment, pls try again later');
            }
            redirect($this->input->server('HTTP_REFERER') ? : site_url('/student/view_student'));
        }
    }

}
