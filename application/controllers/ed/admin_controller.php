<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Eduportal
 * Admin Controller
 * 
 * @category   Controller
 * @package    Admin
 * @subpackage Admin
 * @author     Tohir O <otcleantech@gmail.com>
 * @copyright  Copyright © 2015 Eduportal Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * 
 * @property Admin_nav_lib $admin_nav_lib Description
 * @property Admin_auth_lib $admin_auth_lib Admin Authentication library
 */
class Admin_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'admin_nav_lib',
            'admin_auth_lib',
        ));
        $this->load->helper('admin_nav_helper');
    }
    
    public function dashboard(){
        $this->admin_auth_lib->check_login();
        $data = [];
        $this->admin_nav_lib->run_page('ed/admin/dashboard', $data);
    }

}// end class: Admin_controller
// end file : Admin_controller.php
