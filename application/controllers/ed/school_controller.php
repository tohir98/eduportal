<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of school_controller
 *
 * @author TOHIR
 * 
 * @property Admin_nav_lib $admin_nav_lib Description
 * @property Admin_auth_lib $admin_auth_lib Admin Authentication library
 * @property Reference_model $ref_model Reference Model
 * @property School_model $sch_model Description
 */
class School_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'admin_nav_lib',
            'admin_auth_lib',
        ));
        $this->load->helper(['user_nav_helper', 'notification_helper']);
        $this->load->model('reference_model', 'ref_model');
        $this->load->model('school/school_model', 'sch_model');
    }

    public function add_school() {
        $this->admin_auth_lib->check_login();
        if (request_is_post()) {
            $redirect = site_url('eadmin/school/list_schools');
            if ($this->sch_model->add_school(request_post_data())) {
                notify('success', 'School added successfully', $redirect);
            } else {
                notify('error', 'Unable to add School', $redirect);
            }
        }

        $pageData = array(
            'countries' => $this->ref_model->fetch_all_records(Reference_model::TBL_COUNTRIES),
            'account_types' => School_model::getAccountTypes(),
            'account_statuses' => School_model::getAccountStatuses(),
        );
        $this->admin_nav_lib->run_page('ed/schools/add_school', $pageData);
    }

    public function list_schools() {
        $pageData = array(
            'schools' => $this->sch_model->fetchAllSchools(),
            'account_types' => School_model::getAccountTypes(),
            'account_statuses' => School_model::getAccountStatuses()
        );
        $this->admin_nav_lib->run_page('ed/schools/list_schools', $pageData);
    }

}

// end class: School_controller
