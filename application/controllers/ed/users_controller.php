<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of users_controller
 *
 * @author TOHIR
 * @property Admin_auth_lib $admin_auth_lib Admin Authentication
 */
class users_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'admin_nav_lib',
            'admin_auth_lib',
        ));
    }

    /**
     * Admin login 
     * 
     * @access public
     * @return void
     */
    public function login() {

        $data = array(
            'email_message' => '',
            'password_message' => '',
        );

        // If there are no post request, just display login form.
        if (!request_is_post()) {
            $this->load->view("ed/admin/login", $data);
            return false;
        }

        $valid = true;

        // Define params
        $email = trim(strtolower($this->input->post("email", TRUE)));
        $password = $this->input->post("password", TRUE);
        

        if ($email == '') {
            $data['email_message'] = 'This field is required.';
            $valid = false;
        }

        if ($password == '') {
            $data['password_message'] = 'This field is required.';
            $valid = false;
        }

        if ($valid == false) {
            $this->load->view("ed/admin/login", $data);
            return false;
        }

        // Call login function from user_auth library
        $ret = $this->admin_auth_lib->login($email, $password);

        // User does not exists
        if (!$ret) {
            $data['email_message'] = 'Invalid E-mail/password.';
        } elseif ($ret["status"] == USER_STATUS_INACTIVE) {
            $data['email_message'] = 'Account inactive.';
        } elseif ($ret["status"] == USER_STATUS_ACTIVE) {
            // Log entry
            $this->admin_auth_lib->log_admin_action('Admin logged in successfully', 2300);
            redirect(site_url('eadmin/dashboard'));
        }

        $this->load->view("ed/admin/login", $data);
        return false;
    }
    
       
     public function logout() {
        // Call logout function from user_auth library
        $this->admin_auth_lib->logout();
        redirect(site_url('eadmin/login'), 'refresh');
    }

}

// end class: users_controller
// end file: users_controller.php
