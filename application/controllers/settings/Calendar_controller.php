<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Calendar_controller
 *
 * @author TOHIR
 * @property Reference_model $ref_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property user_nav_lib $user_nav_lib Description
 */
class Calendar_controller extends CI_Controller {

    private $school_id;

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->model('reference_model', 'ref_model');
        $this->load->model('settings/subject_model', 'sub_model');

        $this->school_id = $this->user_auth_lib->get('school_id');
    }

    public function index() {
        $this->user_auth_lib->check_login();
        
        if (request_is_post()){
            var_dump(request_post_data());
            exit;
        }
        
        $data = array(
            'calendar' => $this->ref_model->fetch_all_records(TBL_CALENDAR, ['school_id' => $this->school_id])[0],
            'sessions' => $this->ref_model->fetch_all_records(TBL_SESSION, ['school_id' => $this->school_id]),
            'terms' => $this->ref_model->fetch_all_records(TBL_TERM),
            'school_id' => $this->school_id,
        );

        $this->user_nav_lib->run_page('settings/calendar/list', $data, 'Academic Calendar | EduPortal');
    }

}
