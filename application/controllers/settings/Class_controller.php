<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Class Controller
 *
 * @author TOHIR
 * 
 * @property Reference_model $ref_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property user_nav_lib $user_nav_lib Description
 * @property class_model $class_model Description
 */
class Class_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->model('reference_model', 'ref_model');
        $this->load->model('settings/class_model', 'class_model');
    }
    
    public function index(){
        $this->user_auth_lib->check_login();
        
        if (request_is_post()){
            if ($this->class_model->add_class(request_post_data())) {
                notify('success', 'Class added successfully');
            } else {
                notify('error', 'Unable to add class, Pls try again');
            }
        }
        $data = [
            'classes' => $this->class_model->fetchClasses($this->user_auth_lib->get('school_id')),
            'subjects' => $this->ref_model->fetch_all_records(TBL_SUBJECTS, ['school_id' => $this->user_auth_lib->get('school_id')])
        ];
        $this->user_nav_lib->run_page('settings/classes', $data, 'Classes ! EduPortal');
            
    }
    
    public function delete($class_id) {
        if ($this->class_model->delete_class($class_id)) {
            notify('success', 'Class deleted successfully');
        } else {
            notify('error', 'Unable to delete class, Pls try again');
        }

        redirect(site_url('settings/class'));
    }
    
    public function arm(){
        $this->user_auth_lib->check_login();
        if (request_is_post()){
             if ($this->class_model->add_classarm(request_post_data())) {
                notify('success', 'Class Arm created successfully');
            } else {
                notify('error', 'Unable to create class arm, Pls try again');
            }
            redirect(site_url('settings/class'));
        }
    }

}
