<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of term_controller
 *
 * @author TOHIR
 */
class Term_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->model('reference_model', 'ref_model');
        $this->load->model('settings/session_model', 'session_model');
    }

    public function index() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {

            if ($this->term_model->add_term(request_post_data())) {
                notify('success', 'Term added successfully');
            } else {
                notify('error', 'Unable to add term, Pls try again');
            }
        }

        $data = array(
            'terms' => $this->ref_model->fetch_all_records(TBL_TERM)
        );
        $this->user_nav_lib->run_page('settings/terms', $data, 'Academic Term | EduPortal');
    }

}
