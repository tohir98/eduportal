<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Subject_controller
 *
 * @author TOHIR
 * 
 * @property Reference_model $ref_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property user_nav_lib $user_nav_lib Description
 * @property Subject_model $sub_model Description
 */
class Subject_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->model('reference_model', 'ref_model');
        $this->load->model('settings/subject_model', 'sub_model');
    }

    public function index() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->sub_model->add_subject(request_post_data())) {
                notify('success', 'Subject added successfully');
            } else {
                notify('error', 'Unable to add subject, Pls try again');
            }
        }

        $data = array(
            'subjects' => $this->sub_model->fetch_subjects($this->user_auth_lib->get('school_id')),
            'employees' => $this->ref_model->fetch_all_records(TBL_EMPLOYEES, ['school_id' => $this->user_auth_lib->get('school_id')])
        );

        $this->user_nav_lib->run_page('settings/subject/subjects', $data, 'Subjects ! EduPortal');
    }

    public function assign() {
        $this->user_auth_lib->check_login();

        if (request_is_post()) {
            if ($this->sub_model->add_subject_assignment(request_post_data())) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Unable to assign subject to staff, Pls try again');
            }
            redirect(site_url('settings/subject'));
        }
    }

    public function delete($subject_id) {
        if ($this->ref_model->delete(TBL_SUBJECTS, ['subject_id' => $subject_id])) {
            notify('success', 'Subject deleted successfully');
        } else {
            notify('error', 'Error! : Unable to delete subject at moment, pls try again');
        }

        redirect(site_url('settings/subject'));
    }

    public function edit_subject($subject_id) {
        $data['subject_info'] = $this->ref_model->fetch_all_records(TBL_SUBJECTS, ['subject_id' => $subject_id])[0];

        if (request_is_post()) {
            if ($this->ref_model->update(TBL_SUBJECTS, request_post_data(), ['subject_id' => $subject_id])) {
                notify('success', 'Subject updated successfully');
            } else {
                notify('error', 'Unable to update subject, Pls try again');
            }
            redirect(site_url('settings/subject'));
        }

        $this->load->view('settings/subject/editSubject', $data);
    }

    public function add_class_subject() {
        if (request_is_post()) {
            if ($this->sub_model->saveClassSubjects(request_post_data())) {
                notify('success', 'Operation was successful');
            } else {
                notify('error', 'Operation was not successful');
            }
            redirect(site_url('settings/class'));
        }
    }

}
