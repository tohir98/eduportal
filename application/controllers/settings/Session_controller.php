<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of session_controller
 *
 * @author TOHIR
 * 
 * @property Reference_model $ref_model Description
 * @property User_auth_lib $user_auth_lib Description
 * @property session_model $session_model Description
 */
class Session_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->model('reference_model', 'ref_model');
        $this->load->model('settings/session_model', 'session_model');
    }

    public function index() {
        $this->user_auth_lib->check_login();
        if (request_is_post()) {

            if ($this->session_model->add_session(request_post_data())) {
                notify('success', 'Session added successfully');
            } else {
                notify('error', 'Unable to add session, Pls try again');
            }
        }

        $data = array(
            'sessions' => $this->ref_model->fetch_all_records(TBL_SESSION, ['school_id' => $this->user_auth_lib->get('school_id')])
        );
        $this->user_nav_lib->run_page('settings/sessions', $data, 'Academic Sessions | EduPortal');
    }

    public function delete($session_id) {
        if ($this->session_model->delete_session($session_id)) {
            notify('success', 'Session deleted successfully');
        } else {
            notify('error', 'Unable to delete session, Pls try again');
        }

        redirect(site_url('settings/session'));
    }

    public function current_session_term() {
        if (request_is_post()) {
            
            if ($this->session_model->set_current_session($this->user_auth_lib->get('school_id'), request_post_data()) ){
                notify('success', 'Operation was successfully');
            }else{
                notify('error', 'Unable to complete your request, Pls try again');
            }
            
        }
        $data = array(
            'sessions' => $this->ref_model->fetch_all_records(TBL_SESSION, ['school_id' => $this->user_auth_lib->get('school_id')]),
            'terms' => $this->ref_model->fetch_all_records(TBL_TERM),
            'current_ses_term' => $this->ref_model->fetch_all_records(TBL_CURRENT_SESS_TERM, ['school_id' => $this->user_auth_lib->get('school_id')]),
        );
        $this->user_nav_lib->run_page('settings/current_session_term', $data, 'Academic Sessions | EduPortal');
    }

}
