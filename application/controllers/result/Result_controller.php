<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Ca_controller
 *
 * @author TOHIR
 * @property User_nav_lib $user_nav_lib Description
 * @property User_auth_lib $user_auth_lib Description
 * @property Class_model $class_model Description
 * @property Student_model $student_model Description
 * @property Reference_model $ref_model Description
 * @property CI_Loader $load Description
 * @property Subject_model $sub_model Description
 */
class Result_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array(
            'user_nav_lib'
        ));
        $this->load->model('reference_model', 'ref_model');
        $this->load->model('settings/class_model', 'class_model');
        $this->load->model('settings/subject_model', 'sub_model');
        $this->load->model('user/student_model', 'student_model');
    }

    public function ca() {
        $this->user_auth_lib->check_login();
        $class_id = $_GET['class_id'];
        $class_arm_id = $_GET['class_arm_id'];
        $subject_id = $_GET['subject_id'];
        
        if (request_is_post()){
            var_dump(request_post_data());
            exit;
        }
        
        $data = array(
            'students' => $this->student_model->fetchStudentByClassArm($class_arm_id),   // fet all students in class arm
            'class_info' => $this->ref_model->fetch_all_records(TBL_CLASSES, ['class_id' => $class_id])[0],
            'class_arm' => $this->ref_model->fetch_all_records(TBL_CLASS_ARM, ['class_arm_id' => $class_arm_id])[0]->class_arm,
            'subject_info' => $this->ref_model->fetch_all_records(TBL_SUBJECTS, ['subject_id' => $subject_id])[0],
        );

        $this->user_nav_lib->run_page('result/ca', $data, 'Continous Assessments | EduPortal');
    }

    public function score_entry() {
        $this->user_auth_lib->check_login();
        $data = array(
            'classes' => $this->class_model->fetchClasses($this->user_auth_lib->get('school_id'))
        );

        $this->user_nav_lib->run_page('result/score_entry', $data, 'Score Entry | EduPortal');
    }

    public function view_class_subjects() {
        $this->user_auth_lib->check_login();

        $class_id = $_GET['class_id'];
        $class_arm_id = $_GET['class_arm'];

        $data = array(
            'subjects' => $this->sub_model->fetch_class_subjects($class_id, $this->user_auth_lib->get('school_id')),
            'class_arm_id' => $class_arm_id,
            'class_id' => $class_id,
        );

        $this->load->view('result/view_class_subjects', $data);
    }

    public function exam() {
        
        $this->user_auth_lib->check_login();
        $class_id = $_GET['class_id'];
        $class_arm_id = $_GET['class_arm_id'];
        $subject_id = $_GET['subject_id'];
        
        $data = array(
            'students' => $this->student_model->fetchStudentByClassArm($class_arm_id),
            'class_info' => $this->ref_model->fetch_all_records(TBL_CLASSES, ['class_id' => $class_id])[0],
            'class_arm' => $this->ref_model->fetch_all_records(TBL_CLASS_ARM, ['class_arm_id' => $class_arm_id])[0]->class_arm,
            'subject_info' => $this->ref_model->fetch_all_records(TBL_SUBJECTS, ['subject_id' => $subject_id])[0],
        );
        
        $this->user_nav_lib->run_page('result/exam', $data, 'Score Entry | EduPortal');
    }
    
}
