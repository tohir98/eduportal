<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'page_nav.php';

/**
 * Eduportal
 * Company admin navgation lib
 * 
 * @category   Library
 * @package    Company
 * @subpackage Admin_Nav
 * @author     Tohir O. <otcleantech@gmail.com>
 * @copyright  Copyright Â© 2015 EduPortal Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property Admin_auth_lib $admin_auth_lib Admin Authentication
 * 
 */
class Admin_nav_lib extends page_nav {

    private $admin_id;
    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->library('admin_auth_lib');
        
        $this->admin_id = $this->CI->admin_auth_lib->get("admin_id");
    }

    public function get_assoc() {
        if (!$this->admin_id) {
            return false;
        }
        
        // Fetch user permissions
        $result = $this->CI->admin_model->fetch_admin_groups_modules_perms($this->admin_id);
       
        if (!$result) {
            return false;
        }

        $a = array();

        foreach ($result as $row) {
            if ($row['menu_subject'] != '') {
                $a[$row['menu_subject']][] = $row;
            }
        }

        return $a;
    }

    public function school_name() {
        return EDUPORTAL_BUSINESS_NAME;
    }

    public function get_user_link() {
        return 'eadmin';
    }

    public function get_top_menu() {
        return array(
            'dashboard_url' => 'eadmin/dashboard',
            'logout_url' => site_url('eadmin/logout'),
            'display_name' => $this->CI->admin_auth_lib->get('display_name')
        );
    }

}
