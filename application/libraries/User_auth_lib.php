<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Eduportal
 * User auth lib
 * 
 * @category   Library
 * @package    Users
 * @subpackage Authentication
 * @author     Tohir O. <otcleantech@gmail.com>
 * @copyright  Copyright Â© 2015 EduPortal Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * 
 * @property user_model $user_model Description
 */
class User_auth_lib {

    private $email_user;
    private $user_id;
    private $school_id;

    /**
     * Codeigniter instance
     * 
     * @access private
     * @var object
     */
    private $CI;

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {

        // Load CI object
        $this->CI = get_instance();

        // Load libraries
        $this->CI->load->library(['session', 'user_agent']);

        // Load models
        $this->CI->load->model('user/user_model');

        $this->CI->load->helper(['url', 'notification_helper', 'string']);

        // Set user email value form sessions
        $this->email_user = $this->CI->session->userdata('email');
        $this->id_user = $this->CI->session->userdata('id_user');
        $this->id_company = $this->CI->session->userdata('id_company');

        $this->statuses = array(
            USER_STATUS_INACTIVE => 'Inactive',
            USER_STATUS_ACTIVE => 'Active',
            USER_STATUS_SUSPENDED => 'Suspended',
            USER_STATUS_TERMINATED => 'Terminated',
        );
    }

    /**
     * Login method
     *
     * @access public
     * @param string $email
     * @param string $password
     * @return mixed (bool | array)
     * */
    public function login($email, $password, $school_id, $skipPassword = false) {

        $loginWhere = array('username' => $email, 'school_id' => $school_id);

        if (!$skipPassword) {
            $loginWhere['password'] = $this->encrypt($password);
        }
        // Fetch user data from database by email and password
        $result = $this->CI->user_model->fetch_account($loginWhere);

        if (!$result) {
            // User does not exists
            return FALSE;
        }

        // Define params
        $status = $result[0]->status;
        $account_type = $result[0]->user_type;
        $school_status = $result[0]->school_status;
        $user_id = $result[0]->user_id;

        $basicdata = array(
            'status' => $status,
            'account_type' => $account_type,
            'school_status' => $school_status,
            'user_id' => $user_id
        );
        // Check if user status is ACTIVE
        if ($school_status == USER_STATUS_ACTIVE && $status == USER_STATUS_ACTIVE) {
            if ($result[0]->user_type == USER_TYPE_ADMIN) {
                $this->CI->user_model->assignAllPerm($result[0]->user_id, $result[0]->school_id);
            }
            $key = sha1($result[0]->email . '_' . $status . '_' . $account_type);

            // Build user session array
            $session_vars = array(
                // More session variables to be added later here.
                'user_id' => $result[0]->user_id,
                'staff_id' => $result[0]->staff_id,
                'email' => $result[0]->email,
                'status' => $status,
                'account_type' => $account_type,
                'display_name' => $result[0]->first_name,
                'first_name' => $result[0]->first_name,
                'last_name' => $result[0]->last_name,
                'k' => $key,
                'school_id' => $result[0]->school_id,
                'school_name' => $result[0]->school_name,
                'school_id_string' => $result[0]->school_id_string,
                'cdn_container' => $result[0]->cdn_container,
                'logo_path' => $result[0]->logo_path
            );

            // Add user record details to session
            $this->CI->session->set_userdata($session_vars);
            $this->lastError = '';
//            $this->is_admin() ? $this->CI->user_model->assignAllPermissions($this->get('id_user'), $this->get('school_id')) : '';
            // Load default account notification emails
//            $this->CI->company_model->setAccountNotifications($this->get('school_id'));
//            $this->assignDefaultPermissionsToEmployee(); // Assign default modules to employee --- Obinna
            return $basicdata;
        } else {
            $this->lastError = 'Account inactive.'; //auth_login_error($basicdata);
            return null;
        }
    }

    /**
     * Encrypt string to sha1 
     * 
     * @access public
     * @param string $str
     * @return string
     */
    public static function encrypt($str) {
        return sha1($str);
    }

    /**
     * Check if user logged in
     *
     * @access public
     * @return bool
     * */
    public function logged_in() {

        $cdata = array(
            'email' => $this->CI->session->userdata('email'),
            'status' => $this->CI->session->userdata('status'),
            'account_type' => $this->CI->session->userdata('account_type')
        );

        foreach ($cdata as $data) {
            if (trim($data) == '') {
                return false;
            }
        }

        $s_k = $this->CI->session->userdata('k');
        $c_k = sha1($cdata['email'] . '_' . $cdata['status'] . '_' . $cdata['account_type']);

        if ($s_k != $c_k) {
            return false;
        }

        return true;
    }

    public function logout() {
//        $this->log_user_action('Admin logged out successfully.', 2301);
        // Destroy current user session
        $this->CI->session->sess_destroy();
    }

    /**
     * Get session variable value assigned to user. 
     * 
     * @access public
     * @param string $item
     * @return mixed (bool | string)
     */
    public function get($item = null) {

        if (!$this->logged_in()) {
            return false;
        }

        return $item === null ? $this->CI->session->all_userdata() : $this->CI->session->userdata($item);
    }

    /**
     * Redirect to login page if user not logged in.
     * 
     * @access public
     * @return void
     */
    public function check_login() {

        if (!$this->logged_in()) {
            redirect(site_url('/login'), 'refresh');
        }
    }

    public function generateAdmissionNo() {
        $this->CI->load->model('user/student_model');
        $this->CI->load->model('school/school_model');

        $prefix = $this->CI->school_model->get_prefix($this->get('school_id'));
        $id = $this->CI->student_model->get_admission_no($this->get('school_id'));

        return $prefix . str_pad($id, 5, '0', STR_PAD_LEFT);
    }

}
