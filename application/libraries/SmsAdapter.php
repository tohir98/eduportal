<?php

use sms\Sms_lib;

require 'sms/smslib.php';

/**
 * Description of SmsAdapter
 *
 * @author chuksolloh
 */

class SmsAdapter {
	//put your code here
	protected static $mobiles;
	protected static $message;

	public static function processViaSMSRoute($mobiles, $message){
		if($mobiles != '' &&$message != '') {
			
			self::$mobiles = $mobiles;
			self::$message = $message;
			
			return Sms_lib::sendSms(self::$mobiles, self::$message);
		}
	}
}

var_dump(SmsAdapter::processViaSMSRoute('2348023611841', 'Test message'));