<?= show_notification();
?>
<section class="content-header">
    <h1>
        <a href="<?= site_url('employee/staff_directory') ?>" class="btn btn-flat btn-warning btn-sm">
            <i class="fa fa-chevron-left"></i> Back
        </a>
        <a href="#" class="btn btn-flat btn-primary btn-sm">
            <i class="fa fa-print"></i> Print
        </a>
        <a href="#" class="btn btn-flat btn-primary btn-sm">
            <i class="fa fa-file"></i> PDF
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Records</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">

        <div class="col-md-2">
            <div class="box">
                <div class="box-header">
                    <img src="/files/staff/<?= $record->profile_pic_name; ?>" height="80" width="80">
                </div><!-- /.box-header -->
                <div class="box-body"></div>
            </div><!-- /.box -->
        </div>
        <div class="col-md-10">
            <div class="box box-solid box-primary">
                <div class="box-header" style="padding: 1px;">
                    <h3>
                        &nbsp;&nbsp;Profile
                    </h3>
                    <a class="pull-right" style="
                       margin-top: -35px;
                       margin-right: 5px;
                       " href="<?= site_url('employee/edit_profile/' . $record->staff_id) ?>">Edit now</a>
                </div>
                <div class="box-body">
                    <fieldset>
                        <legend>Personal Information</legend>
                        <table class="table table-striped">
                            <tr>
                                <td width="30%">
                                    Name:
                                </td>
                                <td>
                                    <?= ucfirst($record->first_name) . ' ' . ucfirst($record->last_name); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Email:
                                </td>
                                <td>
                                    <?= $record->email ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Status:
                                </td>
                                <td>
                                    <?= $account_statuses[$record->status] ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Gender:
                                </td>
                                <td>
                                    <?= $record->gender ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Marital Status:
                                </td>
                                <td>
                                    <?= $record->marital_status ?>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Date of Birth:
                                </td>
                                <td>
                                    <?= date('d-M-Y', strtotime($record->dateof_birth)) ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Date of Employment:
                                </td>
                                <td>
                                    <?= date('d-M-Y', strtotime($record->employment_date)) ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Nationality:
                                </td>
                                <td>
                                    <?= $record->country ?>
                                </td>
                            </tr>
                        </table>
                    </fieldset>

                    <fieldset>
                        <legend>Contact Details</legend>
                        <table class="table table-striped">
                            <tr>
                                <td>
                                    Address:
                                </td>
                                <td>
                                    <?= $record->address ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    City:
                                </td>
                                <td>
                                    <?= $record->city ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    State:
                                </td>
                                <td>
                                    <?= $record->state ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Phone Number:
                                </td>
                                <td>
                                    <?= $record->phone ?>
                                </td>
                            </tr>
<!--                            <tr>
                                <td>
                                    Country:
                                </td>
                                <td>
                            <?= $record->country ?>

                                </td>
                            </tr>-->
                        </table>
                    </fieldset>
                </div><!-- /.box-header -->
                <div class="box-body"></div>
            </div><!-- /.box -->
        </div>
    </div>
</section>