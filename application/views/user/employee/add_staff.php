<?= show_notification(); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Add New Staff
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Staff</a></li>
        <li class="active">Add Staff</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <!--<h3 class="box-title">Quick Example</h3>-->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="" enctype="multipart/form-data">

                    <div class="box-body">
                        <table class="table">
                            <tr>
                                <td style="width: 30%">
                                    <label for="first_name">First Name</label>
                                </td>
                                <td>
                                    <input required type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter first name">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="last_name">Last Name</label>
                                </td>
                                <td>
                                    <input required type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter last name">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="staff_id">Staff ID</label>
                                </td>
                                <td>
                                    <input type="text" class="form-control" id="staff_id" name="staff_id" placeholder="Staff ID">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="email">Email</label>
                                </td>
                                <td>
                                    <input required type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="work_number">Work Number</label>
                                </td>
                                <td>
                                    <div class="col-xs-4">
                                        <select required id="country_code" name="country_code" class="form-control">
                                            <option value="">Select Code</option>
                                            <?php
                                            if (!empty($countries)):
                                                foreach ($countries as $country):
                                                    ?>
                                                    <option value="<?= $country->code ?>"><?= $country->country . '-' . $country->code; ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" id="work_number" name="work_number" data-inputmask="'mask': ['999-999-9999', '+099 99 99 9999[9]']" data-mask="">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="address">Address</label>
                                </td>
                                <td>
                                    <input required type="text" class="form-control" id="address" name="address" placeholder="Adress">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="city">City</label>
                                </td>
                                <td>
                                    <input required type="text" class="form-control" id="city" name="city" placeholder="City">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="state_id">State</label>
                                </td>
                                <td>
                                    <select required id="state_id" name="state_id" class="form-control">
                                        <option value="">Select State</option>
                                        <?php
                                        if (!empty($states)):
                                            foreach ($states as $state):
                                                ?>
                                                <option value="<?= $state->state_id ?>"><?= $state->state ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="gender_id">Gender</label>
                                </td>
                                <td>
                                    <select required id="gender_id" name="gender_id" class="form-control">
                                        <option value="">Select Gender</option>
                                        <?php
                                        if (!empty($genders)):
                                            foreach ($genders as $gender):
                                                ?>
                                                <option value="<?= $gender->gender_id ?>"><?= $gender->gender ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="marital_status_id">Marital Status</label>
                                </td>
                                <td>
                                    <select required id="marital_status_id" name="marital_status_id" class="form-control">
                                        <option value="">Select Marital Status</option>
                                        <?php
                                        if (!empty($marital_statuses)):
                                            foreach ($marital_statuses as $status):
                                                ?>
                                                <option value="<?= $status->marital_status_id ?>"><?= $status->marital_status ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="dateof_birth">Date of Birth</label>
                                </td>
                                <td>
                                    <div class="input-group col-xs-5">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input class="form-control" type="text" name="dateof_birth" id="dateof_birth" style="cursor: pointer" >
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="nationality_id">Nationality</label>
                                </td>
                                <td>
                                    <select id="nationality_id" name="nationality_id" class="form-control">
                                        <option value="">Select Nationality</option>
                                        <?php
                                        if (!empty($countries)):
                                            foreach ($countries as $country):
                                                ?>
                                                <option value="<?= $country->country_id ?>"><?= $country->country ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="employment_date">Employment Date</label>
                                </td>
                                <td>
                                    <div class="input-group col-xs-5">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input class="form-control" type="text" name="employment_date" id="employment_date" style="cursor: pointer" >
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="account_status">Account Status</label>
                                </td>
                                <td>
                                    <select required id="account_status" name="account_status" class="form-control">
                                        <option value="">Select Account Status</option>
                                        <?php foreach ($account_statuses as $value => $account_status): ?>
                                            <option value="<?= $value ?>"><?= $account_status ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="profile_pic">Profile Picture</label>
                                </td>
                                <td>
                                    <input class="form-control" type="file" name="userfile" id="userfile" >
                                </td>
                            </tr>
                        </table>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="reset" class="btn btn-warning">Reset</button>
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </form>
            </div><!-- /.box -->


        </div><!--/.col (left) -->
        <!-- right column -->

    </div>   <!-- /.row -->
</section><!-- /.content -->

<script src="/js/school.js"></script>