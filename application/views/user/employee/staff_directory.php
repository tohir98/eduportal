<script src="/js/staff_directory.js" type="text/javascript"></script>
<?= show_notification();
?>
<section class="content-header">
    <h1>
        Staff Directory
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Staff Records</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content" ng-app="staffDir" ng-controller="staffCtrl">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <div class="btn-group">
                        <button type="button" class="btn btn-info">Bulk Action</button>
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" onclick="return false;" class="send_sms">Send SMS</a></li>
                            <li><a href="#" onclick="return false;" class="send_email">Send Email</a></li>
                        </ul>
                    </div>


                </div><!-- /.box-header -->
                <div class="box-body" >
                    <?php
                    if (!empty($employees)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="employees[]" ></th>
                                    <th style="width: 50px">&nbsp;</th>
                                    <th>Employee</th>
                                    <th>Status</th>
                                    <th>Work Info</th>
                                    <th>Date of Birth</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($employees as $employee): ?>
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="employees[]" >
                                        </td>
                                        <td>
                                            <?php
                                            if ($employee->profile_pic_name && $employee->profile_pic_name !== ''):
                                                $filepath = "/files/staff/" . $employee->profile_pic_name;
                                            else:
                                                $filepath = "/img/profile_image.png";
                                            endif;
                                            ?>
                                            <img src="<?= $filepath ?>" width="40px" height="40px">
                                        </td>
                                        <td>
                                            <a href="<?= site_url('employee/view_record/' . $employee->staff_id . '/' . $employee->first_name . '-' . $employee->last_name); ?>">
                                                <?= ucfirst($employee->first_name) . ' ' . ucfirst($employee->last_name) ?>
                                            </a>
                                        </td>
                                        <td><?= $account_statuses[$employee->status] ?></td>
                                        <td>
                                            <?= $employee->email ?> <br>
                                            <?= $employee->phone ?>
                                        </td>
                                        <td>
                                            <?= date('d-M-Y', strtotime($employee->dateof_birth)) ?>
                                        </td>
                                        <td>
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-info">Action</button>
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?= site_url('settings/session/edit/' . $employee->employee_id) ?>" onclick="return false;" class="edit">Edit</a></li>
                                                    <?php if ( (int) $employee->user_type != USER_TYPE_ADMIN): ?>
                                                    <li><a href="#" onclick="return false;" ng-click="editStatus('<?= $employee->employee_id ?>', '<?= ucfirst($employee->first_name)?>')">Edit status</a></li>
                                                        <?php endif; ?>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
    
    <div class="modal" id="modal-editStatus">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="class_arm_title">Edit Status for {{staff.first_name}}</h4>
            </div>

            <form role="form" method="post" action="<?= site_url('/settings/subject/assign') ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="label label-success" id="subjectName"></div>
                        <input type="hidden" id="subject_id" name="subject_id">
                    </div>
                    <div class="form-group">
                        <select required name="employee_id" id="employee_id" class="form-control">
                            <option value="">Select Employee</option>
                            <?php
                            if (!empty($employees)):
                                foreach ($employees as $employee):
                                    ?>
                                    <option value="<?= $employee->employee_id ?>"><?= ucfirst($employee->first_name) . ' ' . ucfirst($employee->last_name) ?></option>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
</section>


