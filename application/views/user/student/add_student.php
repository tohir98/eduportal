<?= show_notification(); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Add New Student
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Student</a></li>
        <li class="active">Add Student</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <!--<h3 class="box-title">Quick Example</h3>-->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="" enctype="multipart/form-data">

                    <div class="box-body">
                        <table class="table">
                            <tr>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="admission_no">Admission No</label>
                                        <input required type="text" class="form-control" id="admission_no" name="admission_no" placeholder="Admission No" value="<?= $admission_no ?>">
                                    </div>
                                </td>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="admission_date">Admission Date</label>
                                        <input type="text" class="form-control" id="admission_date" name="admission_date" placeholder="Admission Date" style="cursor: pointer" value="<?= date('m/d/Y') ?>">
                                    </div>
                                </td>
                                <td style="width: 33%">
                                    &nbsp;
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input required type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                                    </div>
                                </td>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="middle_name">Middle Name</label>
                                        <input type="text" class="form-control" id="middle_name" name="middle_name" placeholder="Middle Name">
                                    </div>
                                </td>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="class_id">Class</label>
                                        <select required name="class_id" id="class_id" class="form-control">
                                            <option value="">Select class</option>
                                            <?php
                                            if (!empty($classes)):
                                                foreach ($classes as $class):
                                                    ?>
                                                    <optgroup label="<?= $class->class_name ?>">
                                                        <?php
                                                        if (!empty($class_arms)):
                                                            foreach ($class_arms as $class_arm):
                                                                if ($class_arm->class_id == $class->class_id):
                                                                    ?>
                                                                    <option value="<?= $class_arm->class_arm_id ?>"><?= strtoupper($class_arm->class_arm) ?></option>
                                                                    <?php
                                                                endif;
                                                            endforeach;

                                                        endif;
                                                        ?>
                                                    </optgroup>

                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="dateof_birth">Date of Birth</label>
                                        <input type="text" class="form-control" id="dateof_birth" name="dateof_birth" placeholder="Date of birth" style="cursor: pointer">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="last_name">Gender</label>
                                        <select required name="gender_id" id="gender_id" class="form-control">
                                            <option value="">Select gender</option>
                                            <?php
                                            if (!empty($genders)):
                                                foreach ($genders as $gender):
                                                    ?>
                                                    <option value="<?= $gender->gender_id ?>"><?= $gender->gender ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="blood_group_id">Blood group</label>
                                        <select required name="blood_group_id" id="blood_group_id" class="form-control">
                                            <option value="">Select blood group</option>
                                            <?php
                                            if (!empty($blood_groups)):
                                                foreach ($blood_groups as $blood_group):
                                                    ?>
                                                    <option value="<?= $blood_group->blood_group_id ?>"><?= $blood_group->blood_group ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="birth_place">Place of Birth</label>
                                        <input type="text" class="form-control" id="birth_place" name="birth_place" placeholder="Place of birth">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="nationality_id">Nationality</label>
                                        <input type="text" name="nationality_id" id="nationality_id" class="form-control" placeholder="Nationality">

                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="language_id">Language</label>
                                        <input type="text" name="language_id" id="language_id" class="form-control" placeholder="Language">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="religion_id">Religion</label>
                                        <select required id="religion_id" name="religion_id" class="form-control">
                                            <option value="">Select Religion</option>
                                            <?php
                                            if (!empty($religions)):
                                                foreach ($religions as $religion):
                                                    ?>
                                                    <option value="<?= $religion->religion_id ?>"><?= $religion->religion ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="address1">Address</label>
                                        <input type="text" name="address1" id="address1" class="form-control" placeholder="Address">

                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="address2">Address 2</label>
                                        <input type="text" name="address2" id="address2" class="form-control" placeholder="Address 2">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <input type="text" class="form-control" id="city" name="city" placeholder="City">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="state_id">State</label>
                                        <select required id="state_id" name="state_id" class="form-control">
                                            <option value="">Select State</option>
                                            <?php
                                            if (!empty($states)):
                                                foreach ($states as $state):
                                                    ?>
                                                    <option value="<?= $state->state_id ?>"><?= $state->state ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="country_id">Country</label>
                                        <select required id="country_id" name="country_id" class="form-control">
                                            <option value="">Select Country</option>
                                            <?php
                                            if (!empty($countries)):
                                                foreach ($countries as $country):
                                                    ?>
                                                    <option value="<?= $country->country_id ?>"><?= $country->country; ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="email">Email Address</label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label for="phone">Phone Number</label>
                                        <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone Number">                                      
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label for="picture">Picture</label>
                                        <input type="file" name="userfile" id="picture" class="form-control">
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right">
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                    <button type="submit" class="btn btn-primary">Next >> Parent Details</button>
                                </td>
                            </tr>

                        </table>
                    </div><!-- /.box-body -->

                </form>
            </div><!-- /.box -->


        </div><!--/.col (left) -->
        <!-- right column -->

    </div>   <!-- /.row -->
</section><!-- /.content -->

<script>
    $('document').ready(function () {
        $('#admission_date').datepicker();
        $('#dateof_birth').datepicker();
    });
</script>