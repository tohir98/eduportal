<div class="box box-solid box-primary student_info_box" id="student_profile_container">
    <div class="box-header" style="padding: 1px;">
        <h3>
            &nbsp;&nbsp;Profile
        </h3>
    </div>
    <div class="box-body">
        <table class="table table-striped">
            <tr>
                <td>
                    Name:
                </td>
                <td>
                    <?= ucfirst($record->first_name) . ' ' . ucfirst($record->middle_name) . ' ' . ucfirst($record->last_name) ?>
                </td>
            </tr>
            <tr>
                <td>
                    Admission Number:
                </td>
                <td>
                    <?= $record->admission_no ?>
                </td>
            </tr>
            <tr>
                <td>
                    Admission Date:
                </td>
                <td>
                    <?= $record->admission_date !== '0000-00-00' ? date('d-M-Y', strtotime($record->admission_date)) : 'NA' ?>
                </td>
            </tr>
            <tr>
                <td>
                    Class:
                </td>
                <td>
                    <?= $record->class_name ?>
                </td>
            </tr>
            <tr>
                <td>
                    Date of Birth:
                </td>
                <td>
                    <?= date('d-M-Y', strtotime($record->dateof_birth)) ?>
                </td>
            </tr>
            <tr>
                <td>
                    Address 1:
                </td>
                <td>
                    <?= $record->address1 ?>
                </td>
            </tr>
            <tr>
                <td>
                    Address 2:
                </td>
                <td>
                    <?= $record->address2 ?>

                </td>
            </tr>
            <tr>
                <td>
                    Blood Group:
                </td>
                <td>
                    <?= $record->blood_group ?>
                </td>
            </tr>
            <tr>
                <td>
                    Gender:
                </td>
                <td>
                    <?= $record->gender ?>
                </td>
            </tr>
            <tr>
                <td>
                    Place of Birth:
                </td>
                <td>
                    <?= $record->birth_place ?>
                </td>
            </tr>
            <tr>
                <td>
                    Nationality:
                </td>
                <td>
                    <?= $record->country ?>
                </td>
            </tr>
            <tr>
                <td>
                    Language:
                </td>
                <td>
                    <?= $record->language ?>
                </td>
            </tr>
            <tr>
                <td>
                    Religion:
                </td>
                <td>
                    <?= $record->religion ?>
                </td>
            </tr>
            <tr>
                <td>
                    City:
                </td>
                <td>
                    <?= $record->city ?>
                </td>
            </tr>
            <tr>
                <td>
                    State:
                </td>
                <td>
                    <?= $record->state ?>
                </td>
            </tr>
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <?= $record->email ?>
                </td>
            </tr>
            <tr>
                <td>
                    Phone Number:
                </td>
                <td>
                    <?= $record->phone ?>
                </td>
            </tr>
        </table>
    </div>
</div>
