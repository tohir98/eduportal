<div class="box box-solid box-primary student_info_box" id="student_emergency_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            Emergency Contacts
        </h3>
    </div>
    <div class="box-body">
        <div class="well">
            <?php if (!empty($e_contacts)): ?>
                <b>Name:</b> <?= $e_contacts[0]->first_name . ' ' . $e_contacts[0]->last_name ?> <br>
                <b>Address:</b> <?= $e_contacts[0]->address ?> <br>
                <b>Email:</b><?= $e_contacts[0]->email ?> <br>
                <b>Phone:</b><?= $e_contacts[0]->phone ?>
                <b>Work Address:</b><?= $e_contacts[0]->work_address ?>
                <hr>
                <b>Name:</b> <?= $e_contacts[0]->first_name2 . ' ' . $e_contacts[0]->last_name2 ?> <br>
                <b>Address:</b> <?= $e_contacts[0]->addresss2 ?> <br>
                <b>Email:</b><?= $e_contacts[0]->email2 ?> <br>
                <b>Phone:</b><?= $e_contacts[0]->phone2 ?>
                <b>Work Address:</b><?= $e_contacts[0]->work_address2 ?>
            <?php
            else:
                echo 'Record not added yet.<a href="#emergency_contacts"> Clich here to add Emergency Contacts<a>';
            endif;
            ?>

        </div>
    </div>
</div>