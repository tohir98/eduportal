<div class="box box-solid box-primary student_info_box" id="student_guardian_container" style="display: none">
    <div class="box-header" style="padding: 1px;">
        <h3>
            Guardian Details
        </h3>
    </div>
    <div class="box-body">
        <div class="well">
            <?php if (!empty($g_details)): ?>
                <?= $g_details[0]->first_name . ' ' . $g_details[0]->last_name ?> <br>
                <?= $g_details[0]->office_address ?> <br>
                <?= $g_details[0]->email ?> <br>
                <?= $g_details[0]->mobile_phone ?>
            <?php else:
                echo 'Record not added yet.<a href="#guardian_details"> Clich here to add Guardian Detais <a>';
            endif;
            ?>

        </div>
    </div>
</div>