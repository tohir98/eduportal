<?= show_notification(); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Guardian
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Student</a></li>
        <li class="active">Guardian</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <!--<h3 class="box-title">Quick Example</h3>-->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="" >

                    <div class="box-body">
                        <fieldset>
                            <legend>
                                Parent - Personal Details
                            </legend>
                            <table class="table">
                            <tr>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input required type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                                    </div>
                                </td>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                                    </div>
                                </td>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="relationship_id">Relation</label>
                                        <select required id="relationship_id" name="relationship_id" class="form-control">
                                            <option value="">Select Relationship</option>
                                            <?php
                                            if (!empty($relationships)):
                                                foreach ($relationships as $relationship):
                                                    ?>
                                                    <option value="<?= $relationship->relationship_id ?>"><?= $relationship->relationship; ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="dateof_birth">Date of Birth</label>
                                        <input type="text" class="form-control" id="dateof_birth" name="dateof_birth" placeholder="Date of Birth" style="cursor: pointer">
                                    </div>
                                </td>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="education">Education</label>
                                        <input type="text" class="form-control" id="education" name="education" placeholder="Education">
                                    </div>
                                </td>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="occupation_id">Occupation</label>
                                        <input type="text" class="form-control" id="occupation_id" name="occupation_id" placeholder="Occupation">
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="income">Income</label>
                                        <input type="text" class="form-control" id="income" name="income" placeholder="Income">
                                    </div>
                                </td>
                                <td style="width: 33%"></td>
                                <td style="width: 33%"></td>

                            </tr>
                            
                            

                        </table>
                        </fieldset>
                        <legend>
                            Parent - Contact Details
                        </legend>
                        
                        
                            <table class="table">
                            <tr>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                    </div>
                                </td>   
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="mobile_phone">Mobile Phone</label>
                                        <input required type="text" class="form-control" id="mobile_phone" name="mobile_phone" placeholder="Mobile Phone">
                                    </div>
                                </td>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="office address">Office Address</label>
                                        <input type="text" class="form-control" id="office address" name="office address" placeholder="Office Address">
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <input required type="text" class="form-control" id="city" name="city" placeholder="City">
                                    </div>
                                </td>   
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="state_id">State</label>
                                        <select required id="state_id" name="state_id" class="form-control">
                                            <option value="">Select State</option>
                                            <?php
                                            if (!empty($states)):
                                                foreach ($states as $state):
                                                    ?>
                                                    <option value="<?= $state->state_id ?>"><?= $state->state ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </td>
                                <td style="width: 33%">
                                    <div class="form-group">
                                        <label for="country_id">Country</label>
                                        <select required id="country_id" name="country_id" class="form-control">
                                            <option value="">Select Country</option>
                                            <?php
                                            if (!empty($countries)):
                                                foreach ($countries as $country):
                                                    ?>
                                                    <option value="<?= $country->country_id ?>"><?= $country->country; ?></option>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </td>

                            </tr>
                            </table>
                        
                        
                        <table class="table">
                            <tr>
                                <td colspan="3" align="right">
                                    <input name="student_id" type="hidden" value="<?= $this->uri->segment(3) ?>" >
                                    <button type="reset" class="btn btn-warning">Reset</button>
                                    <button type="submit" class="btn btn-primary">Next: Emergency Contacts</button>
                                </td>
                            </tr>
                        </table>
                        
                        
                    </div><!-- /.box-body -->

                </form>
            </div><!-- /.box -->


        </div><!--/.col (left) -->
        <!-- right column -->

    </div>   <!-- /.row -->
</section><!-- /.content -->

<script>
    $('document').ready(function () {
        $('#dateof_birth').datepicker();
    });
</script>