<script>
    window.studentDirConfig = window.staffDirConfig || {};
    window.studentDirConfig.students = <?= json_encode($students) ?>;
    window.studentDirConfig.account_statuses = <?= json_encode($account_statuses) ?>;

</script>

<link href="/css/angular-busy.min.css" rel="stylesheet" type="text/css"/>
<script src="/js/angular-busy.min.js"></script>
<script src="/js/angular-datatables-all.js" type="text/javascript"></script>
<script src="/js/student_directory.js" type="text/javascript"></script>

<?= show_notification();
?>
<section class="content-header">
    <h1>
        Student Directory
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Student Records</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content" ng-app="studentDir">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary">Bulk Action</button>
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" onclick="return false;" class="send_sms">Send SMS</a></li>
                            <li><a href="#" onclick="return false;" class="send_email">Send Email</a></li>
                        </ul>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body" ng-controller="listCtrl">
                    <?php
                    if (!empty($students)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped" ng-cloak="">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" ng-click="toggleAll()" ></th>
                                    <th style="width: 50px">&nbsp;</th>
                                    <th>Student</th>
                                    <th>Status</th>
                                    <th>Parent Contact</th>
                                    <th>Date of Birth</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="student in config.students| filter:filter">
                                    <td>
                                        <input type="checkbox" value="{{student.student_id}}" name="employees[]" ng-model="student.is_selected" >
                                    </td>
                                    <td>
                                        <img ng-src="/files/student/{{student.picture}}" width="40px" height="40px">
                                    </td>
                                    <td>
                                        <a href="<?= site_url('student/view_record'); ?>/{{student.admission_no}}/{{student.first_name}}-{{student.last_name}}">
                                            {{student.first_name| uppercase}} {{student.last_name| uppercase}}
                                        </a>
                                    </td>
                                    <td>{{config.account_statuses[student.status]}}</td>
                                    <td>
                                        {{student.g_email}} <br>
                                        {{student.g_phone}}
                                    </td>
                                    <td>{{student.dateof_birth}} </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info">Action</button>
                                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="<?= site_url('student/view_record'); ?>/{{student.admission_no}}/{{student.first_name}}-{{student.last_name}}">View Record</a></li>
                                                <li ng-hide="1"><a href="<?= site_url('student/delete/') ?>/{{student.student_id}}" onclick="return false;" class="delete">Edit Status</a></li>
                                                <li ng-if="student.g_phone !== ''">
                                                    <a href="#" onclick="return false;" ng-click="sendSms(student)">Send SMS to parent</a>

                                                </li>
                                                <li ng-if="student.g_email !== ''">
                                                    <a href="#" onclick="return false;" ng-click="sendEmail(student)">Send Email to parent</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

    <div class="modal" id="modal_send_sms">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Send SMS</h4>
                </div>
                <form role="form" method="post" class="form-horizontal" action="<?= site_url('communication/send_sms') ?>">
                    <div class="modal-body">
                        <span id="lblRecepientName" class=""></span>
                        <span id="lblrecepient" class="label label-warning"></span>
                        <input type="hidden"  id="recepient" name="recepient">
                        <br>
                        <br>

                        <textarea required name="message" id="message" rows="3" style="width: 80%" maxlength="140"></textarea>
                        <div style="color: red">Max. of 140 letters</div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Send</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="modal" id="modal_send_email">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Send Email</h4>
                </div>
                <form role="form" method="post" class="form-horizontal" action="<?= site_url('communication/send_email') ?>">
                    <div class="modal-body">
                        <span id="lblRecepientName_email" class=""></span>
                        <span id="lblrecepient_email" class="label label-warning"></span>
                        <input type="text"  id="recepient_email" name="recepient" style="width: 80%" readonly="">
                        <br>
                        <br>
                        <input type="text"  id="sender" name="sender" style="width: 80%" value="<?= $this->user_auth_lib->get('email') ?>" >
                        <br>
                        <br>
                        <input type="text"  id="email_subject" name="email_subject" style="width: 80%" placeholder="Subject">
                        <br>
                        <br>

                        <textarea required name="message" rows="3" style="width: 80%"></textarea>
                        

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Send</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
