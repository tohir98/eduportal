<?= show_notification(); ?>

<section class = "content-header">
    <h1>
        Emergency Contacts
    </h1>
    <ol class = "breadcrumb">
        <li><a href = "#"><i class = "fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href = "#">Student</a></li>
        <li class = "active">Emergency Contacts</li>
    </ol>
</section>

<section class = "content">
    <div class = "row">
        <!--left column -->
        <div class = "col-md-12">
            <!--general form elements -->
            <div class = "box box-primary">
                <div class = "box-header">
                    <!--<h3 class = "box-title">Quick Example</h3> -->
                </div><!--/.box-header -->
                <!--form start -->
                <form role = "form" method = "post" action = "" >

                    <div class = "box-body">
                        <fieldset>
                            <legend>
                                Emergency Contact 1
                            </legend>
                            <table class = "table">
                                <tr>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "first_name">First Name</label>
                                            <input required type = "text" class = "form-control" id = "first_name" name = "first_name" placeholder = "First Name">
                                        </div>
                                    </td>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "last_name">Last Name</label>
                                            <input required type = "text" class = "form-control" id = "last_name" name = "last_name" placeholder = "Last Name">
                                        </div>
                                    </td>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "address">Address</label>
                                            <input required type = "text" class = "form-control" id = "address" name = "address" placeholder = "Address">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "email">Email</label>
                                            <input type = "email" class = "form-control" id = "email" name = "email" placeholder = "Email">
                                        </div>
                                    </td>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "phone">Phone Number</label>
                                            <input required type = "text" class = "form-control" id = "phone" name = "phone" placeholder = "Phone Number">
                                        </div>
                                    </td>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "employer">Employer</label>
                                            <input type = "text" class = "form-control" id = "employer" name = "employer" placeholder = "Employer">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "work_address">Work Address</label>
                                            <input type = "text" class = "form-control" id = "work_address" name = "work_address" placeholder = "Work Address">
                                        </div>
                                    </td>
                                    <td style = "width: 33%">
                                    </td>
                                    <td style = "width: 33%">
                                    </td>
                                </tr>

                            </table>
                        </fieldset>
                        
                        <fieldset>
                            <legend>
                                Emergency Contact 2
                            </legend>
                            <table class = "table">
                                <tr>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "first_name2">First Name</label>
                                            <input required type = "text" class = "form-control" id = "first_name2" name = "first_name2" placeholder = "First Name">
                                        </div>
                                    </td>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "last_name2">Last Name</label>
                                            <input required type = "text" class = "form-control" id = "last_name2" name = "last_name2" placeholder = "Last Name">
                                        </div>
                                    </td>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "addresss2">Address</label>
                                            <input required type = "text" class = "form-control" id = "addresss2" name = "addresss2" placeholder = "Address">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "email2">Email</label>
                                            <input type = "email" class = "form-control" id = "email2" name = "email2" placeholder = "Email">
                                        </div>
                                    </td>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "phone2">Phone Number</label>
                                            <input required type = "text" class = "form-control" id = "phone2" name = "phone2" placeholder = "Phone Number">
                                        </div>
                                    </td>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "employer2">Employer</label>
                                            <input type = "text" class = "form-control" id = "employer2" name = "employer2" placeholder = "Employer">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style = "width: 33%">
                                        <div class = "form-group">
                                            <label for = "work_address2">Work Address</label>
                                            <input type = "text" class = "form-control" id = "work_address2" name = "work_address2" placeholder = "Work Address">
                                        </div>
                                    </td>
                                    <td style = "width: 33%">
                                    </td>
                                    <td style = "width: 33%">
                                    </td>
                                </tr>

                            </table>
                        </fieldset>
                        <fieldset>
                            <table class = "table">
                                <tr>
                                    <td colspan = "3" align = "right">
                                        <input name = "student_id" type = "hidden" value = "<?= $this->uri->segment(3) ?>" >
                                        <button type = "reset" class = "btn btn-warning">Reset</button>
                                        <button type = "submit" class = "btn btn-primary">Submit</button>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        

                    </div><!--/.box-body -->

                </form>
            </div><!--/.box -->


        </div><!--/.col (left) -->
        <!--right column -->

    </div> <!--/.row -->
</section><!--/.content -->

