<?= show_notification();
?>
<section class="content-header">
    <h1>
        <a href="#" class="btn btn-flat btn-warning btn-sm">
            <i class="fa fa-chevron-left"></i> Back
        </a>
        <a href="#" class="btn btn-flat btn-primary btn-sm">
            <i class="fa fa-print"></i> Print
        </a>
        <a href="#" class="btn btn-flat btn-primary btn-sm">
            <i class="fa fa-file"></i> PDF
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Records</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <select class="form-control" name="cboStudents" id="cboStudents">
                <option></option>
                <?php
                
                if (!empty($students)):
                    $sel = '';
                    foreach ($students as $student):
                        $sel = $student->student_id == $record->student_id ? 'selected' : '';
                        ?>
                        <option value="<?= $student->student_id ?>" <?= $sel ?>><?= ucfirst($student->first_name) . ' ' . ucfirst($student->last_name); ?></option>
                    <?php
                    endforeach;
                endif;
                ?>
            </select>
        </div>
    </div>
    <div class="row" style="margin-top: 20px">
        <div class="col-md-2">
            <div class="box box-solid">
                <div class="box-header with-border" style="border:1px solid #CCCCCC;height:161px;padding:0;">
                    <div class="row-fluid" style="height: 180px;overflow:hidden">
                        <img src="/files/student/<?= $record->picture; ?>" style="max-width: 150px">
                        <br>
                        <!--<button class="btn btn-flat btn-primary">Edit Picture</button>-->
                    </div>
                </div>
                <div class="box-body no-padding" style="margin-top: 10px">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a href="#"> Profile </a></li>
                        <li> <a href="#">Guardian</a></li>
                        <li> <a href="#">Emergency Details</a></li>
                    </ul>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-8">
            <div class="box">
                <div class="box-header">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td>
                                Name:
                            </td>
                            <td>
<?= ucfirst($record->first_name) . ' ' . ucfirst($record->middle_name) . ' ' . ucfirst($record->last_name) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Admission Number:
                            </td>
                            <td>
<?= $record->admission_no ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Admission Date:
                            </td>
                            <td>
<?= $record->admission_date ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Class:
                            </td>
                            <td>
<?= $record->class_name ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Date of Birth:
                            </td>
                            <td>
<?= $record->dateof_birth ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Address 1:
                            </td>
                            <td>
<?= $record->address1 ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Address 2:
                            </td>
                            <td>
<?= $record->address2 ?>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                Blood Group:
                            </td>
                            <td>
<?= $record->blood_group ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Gender:
                            </td>
                            <td>
<?= $record->gender ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Place of Birth:
                            </td>
                            <td>
<?= $record->birth_place ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Nationality:
                            </td>
                            <td>
<?= $record->country ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Language:
                            </td>
                            <td>
<?= $record->language ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Religion:
                            </td>
                            <td>
<?= $record->religion ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                City:
                            </td>
                            <td>
<?= $record->city ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                State:
                            </td>
                            <td>
<?= $record->state ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email:
                            </td>
                            <td>
<?= $record->email ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Phone Number:
                            </td>
                            <td>
<?= $record->phone ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Date Created:
                            </td>
                            <td>
<?= $record->date_created ?>
                            </td>
                        </tr>
                    </table>
                </div><!-- /.box-header -->
                <div class="box-body"></div>
            </div><!-- /.box -->
        </div>
    </div>
</section>