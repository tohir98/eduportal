<?= show_notification();
?>
<section class="content-header">
    <h1>
        <a href="<?= site_url('student/view_student')?>" class="btn btn-flat btn-warning btn-sm">
            <i class="fa fa-chevron-left"></i> Back
        </a>
        <a href="#" class="btn btn-flat btn-primary btn-sm">
            <i class="fa fa-print"></i> Print
        </a>
        <a href="#" class="btn btn-flat btn-primary btn-sm">
            <i class="fa fa-file"></i> PDF
        </a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Records</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <select class="form-control students" name="cboStudents" id="cboStudents">
                <option></option>
                <?php
                if (!empty($students)):
                    $sel = '';
                    foreach ($students as $student):
                        $sel = $student->admission_no == $record->admission_no ? 'selected' : '';
                        ?>
                        <option value="<?= $student->admission_no ?>" data-first_name="<?= ucfirst($student->first_name) ?>" data-last_name="<?= ucfirst($student->last_name) ?>" <?= $sel ?>><?= ucfirst($student->first_name) . ' ' . ucfirst($student->last_name); ?></option>
                        <?php
                    endforeach;
                endif;
                ?>
            </select>
        </div>
    </div>
    <div class="row" style="margin-top: 20px">
        <div class="col-md-2">
            <div class="box box-solid">
                <div class="box-header with-border" style="border:1px solid #CCCCCC;height:161px;padding:0;">
                    <div class="row-fluid" style="height: 180px;overflow:hidden">
                        <img src="/files/student/<?= $record->picture; ?>" style="max-width: 150px">
                        <br>
                        <!--<button class="btn btn-flat btn-primary">Edit Picture</button>-->
                    </div>
                </div>
                <div class="box-body no-padding" style="margin-top: 10px">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a class="left_menu_button" href="#student_profile_container"> Profile </a></li>
                        <li> <a class="left_menu_button" href="#student_guardian_container">Guardian</a></li>
                        <li> <a class="left_menu_button" href="#student_emergency_container">Emergency Details</a></li>
                    </ul>
                </div><!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-8">
            <?php
            echo $profile_content;
            echo $guardian_content;
            echo $emergency_content;
            ?>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function () {

        $('.left_menu_button').click(function () {
            $('.left_menu_button').parent().removeClass('active');
            $(this).parent().addClass('active');

            $('.student_info_box').hide();

            var container_id = $(this).attr('href');
            $(container_id).show();
            window.location.href = container_id;
            return false;
        });

        if (window.location.hash !== '' && window.location.hash !== '#') {
            var k = window.location.hash.substring();
            $('.left_menu_button[href="' + k + '"]').click();
        }


        $('.students').change(function () {
            var id_string = $(this).val();
            var first_name = $(this).find(':selected').attr('data-first_name');
            var last_name = $(this).find(':selected').attr('data-last_name');
            window.location.href = '<?php echo site_url('student/view_record'); ?>/' + id_string + '/' + first_name + '-' + last_name;
        });

       
       

    });


</script>
