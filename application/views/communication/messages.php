<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Message
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Student Records</a></li>
    </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#emails" data-toggle="tab" aria-expanded="false">Sent Emails</a></li>
                    <li class=""><a href="#sms" data-toggle="tab" aria-expanded="true">Sent SMS</a></li>

                    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="emails">
                        <?php include '_emails.php'; ?>
                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="sms">
                        <?php include '_sms.php'; ?>
                    </div><!-- /.tab-pane -->

                </div><!-- /.tab-content -->
            </div>
        </div>
    </div>

    <div class="modal" id="modal_send_email">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Send Email</h4>
                </div>
                <form role="form" method="post" class="form-horizontal" action="<?= site_url('communication/send_email') ?>">
                    <div class="modal-body">
                        <span id="lblRecepientName_email" class=""></span>
                        <span id="lblrecepient_email" class="label label-warning"></span>
                        <table class="table">
                            <tr>
                                <td>Recipient</td>
                                <td>
                                    <input required type="email"  id="recepient_email" name="recepient" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>Sender</td>
                                <td>
                                    <input required type="text"  id="sender" name="sender" value="<?= $this->user_auth_lib->get('email') ?>" class="form-control" >
                                </td>
                            </tr>
                            <tr>
                                <td>Subject</td>
                                <td>
                                    <input required type="text"  id="email_subject" name="email_subject" class="form-control" placeholder="Subject">
                                </td>
                            </tr>
                            <tr>
                                <td>Message</td>
                                <td>
                                    <textarea required name="message" rows="3" class="form-control"></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Send</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- SMS Modal-->
    <div class="modal" id="modal_send_sms">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Send SMS</h4>
                </div>
                <form role="form" method="post" class="form-horizontal" action="<?= site_url('communication/send_sms') ?>">
                    <div class="modal-body">
                        <table class="table">
                            <tr>
                                <td>Recipient</td>
                                <td>
                                    <input required type="tel"  id="recepient_email" name="recepient" class="form-control">
                                </td>
                            </tr>
                            <tr>
                                <td>Message</td>
                                <td>
                                    <textarea required name="message" id="message" rows="3" class="form-control"></textarea>
                                    <div style="color: red">Max. of 140 letters</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Send</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>

