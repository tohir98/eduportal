<div class="box">
    <div class="box-header">
        <h3 class="box-title">
            <a class="btn btn-block btn-primary pull-right" href="#modal_send_sms" data-toggle="modal">
                New SMS 
            </a>
        </h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <?php
        if (!empty($smses)):
            ?>
            <table class="table table-bordered table-striped dataTable">
                <thead>
                    <tr>
                        <th>Sender</th>
                        <th>Recipient</th>
                        <th>Message</th>
                        <th>Status</th>
                        <th>Date Sent</th>
                        <th>Time Sent</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($smses as $sms): ?>
                        <tr>
                            <td><?= ucfirst($sms->first_name . ' ' . $sms->last_name); ?></td>
                            <td> <?= $sms->phone_number ?> </td>
                            <td><?= $email->message ?></td>
                            <td><?= ucfirst($sms->status) ?></td>
                            <td><?= date('d M Y', strtotime($sms->date_sent)) ?></td>
                            <td><?= date('h:i:A', strtotime($sms->time_sent)) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php
        else:
            $msg = "No sms has been sent. <a href=#>Click here to add one.</a>";
            echo show_no_data($msg);
        endif;
        ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->