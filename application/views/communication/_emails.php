<div class="box">
    <div class="box-header">
            <h3 class="box-title">
                <a class="btn btn-block btn-primary pull-right" href="#modal_send_email" data-toggle="modal">
                    New Email 
                </a>
            </h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <?php
        if (!empty($emails)):
            ?>
            <table class="table table-bordered table-striped dataTable">
                <thead>
                    <tr>
                        <th>Sender</th>
                        <th>Recipient</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Date Sent</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($emails as $email): ?>
                        <tr>
                            <td><?= ucfirst($email->first_name . ' ' . $email->last_name); ?></td>
                            <td> <?= $email->recepient ?> </td>
                            <td><?= ucfirst($email->subject) ?></td>
                            <td><?= $email->message ?></td>
                            <td><?= date('d M Y', strtotime($email->date_sent)) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php
        else:
            $msg = "No email has been sent. <a href='#modal_send_email'>Click here to add one.</a>";
            echo show_no_data($msg);
        endif;
        ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->