<?= show_notification();
?>
<section class="content-header">
    <h1>
        Exam | <?= $class_info->class_name; 0 ?> | <?= $class_arm ?> | <?= $subject_info->subject_name; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Score Entry</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content" >
    <div class="row">
        <div class="col-md-12">

            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body" ng-controller="scoreCtrl">
                    <form method="post">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Exam</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($students as $student):
                                    ?>
                                    <tr>
                                        <td>
                                            <?= ucfirst($student->first_name . ' ' . $student->last_name) ?>
                                            <input type="hidden" name="student_id[]" value="<?= $student->student_id; ?>"/>
                                        </td>
                                        <td>
                                            <input required type="number" max="60" name="score[]" id="score" maxlength="2" />
                                        </td>
                                    </tr><?php endforeach; ?>

                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <button type="cancel" class="btn btn-warning">Cancel</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>