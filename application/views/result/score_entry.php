<?= show_notification();
?>
<section class="content-header">
    <h1>
        Result
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Score Entry</a></li>
    </ol>
</section>



<!-- Main content -->
<section class="content" ng-app="score">
    <div class="row">
        <div class="col-md-12">
            <!--            <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#" data-toggle="tab" aria-expanded="false">Score Entry</a></li>
                            </ul>
            
                        </div>-->
            <div class="box">
                <div class="box-header">
                    <h3>Select Class</h3>
                </div><!-- /.box-header -->
                <div class="box-body" ng-controller="scoreCtrl">
                    <?php
                    if (!empty($classes)):
                        ?>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Class</th>
                                    <th>Arm</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cnt = 0;
                                foreach ($classes as $class) :
//                                    var_dump($class['arms']); exit;
                                    if (!empty($class['arms'])):
                                        foreach ($class['arms'] as $arm):
                                            ?>
                                            <tr>
                                                <td><?= ++$cnt; ?></td>
                                                <td><?= $class['class_name'] ?></td>
                                                <td><?= $arm['class_arm'] ?></td>
                                                <td>
                                                    <a ng-click="viewClassSubjects('<?= site_url('result/view_class_subjects?class_id='.$class['class_id'].'&class_arm=' . $arm['class_arm_id']) ?>')" href="#" onclick="return false;">Subjects</a>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                endforeach;
                                ?>

                            </tbody>
                        </table>
                        <?php
                    else:
                        echo show_no_data('No class was found');
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Edit Modal -->
<div class="modal" id="modal-view_subject">

</div>

<script>

    var scoreApp = angular.module('score', []);

    scoreApp.controller('scoreCtrl', function ($scope) {
        
        $scope.viewClassSubjects = function (page) {
            console.log(page);
            $('#modal-view_subject').modal('show').fadeIn();

            $.get(page, function (html) {

                $('#modal-view_subject').html('');
                $('#modal-view_subject').html(html).show();

            });
        };
    });


</script>