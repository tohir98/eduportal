<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Class Subjects</h4>
        </div>
        <form role="form" method="post" action="">
            <div class="modal-body">
                <?php if (!empty($subjects)): ?>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>SN</th>
                                <th>Subject</th>
                                <th>...</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cnt = 0;
                            foreach ($subjects as $subject):
                                ?>
                                <tr>
                                    <td><?= ++$cnt; ?></td>
                                    <td><?= $subject->subject_name; ?></td>
                                    <td>
                                        <a href="<?= site_url('result/ca?class_id=' . $class_id . '&class_arm_id=' . $class_arm_id . '&subject_id=' . $subject->subject_id) ?>">CA</a> &nbsp;|&nbsp;
                                        <a href="<?= site_url('result/exam?class_id=' . $class_id . '&class_arm_id=' . $class_arm_id . '&subject_id=' . $subject->subject_id) ?>">Exam</a>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php
                else:
                    $link = site_url('settings/class');
                    echo show_no_data("No subject has been added for this class. <a href=$link>Click here to add subjects</a>");
                endif;
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning btn-flat" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
</div>
