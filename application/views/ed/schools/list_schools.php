<?= show_notification(); ?>
<section class="content-header">
    <h1>
        List School
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Schools</a></li>
        <li class="active">List Schools</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <a class="btn btn-block btn-primary" href="<?= site_url('eadmin/school/add_school'); ?>">Add School</a>
                    </h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php if(!empty($schools)): ?>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Account Type</th>
                                <th>Last Log</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                         <tbody>
                             <?php foreach ($schools as $school): ?>
                            <tr>
                                <td><?= $school->school_name ?></td>
                                <td>Active</td>
                                <td><?= $account_types[$school->account_type_id] ?></td>
                                <td> &nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php endforeach; ?>
                         </tbody>
                    </table>
                    <?php endif; ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>