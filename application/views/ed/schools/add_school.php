<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Add New School
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Schools</a></li>
        <li class="active">Add School</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <!--<h3 class="box-title">Quick Example</h3>-->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="">
                    <div class="box-body">
                        <div class="form-group ">
                            <label for="first_name">First Name</label>
                            <input required type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter first name">
                        </div>
                        <div class="form-group ">
                            <label for="last_name">Last Name</label>
                            <input required type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter last name">
                        </div>
                        <div class="form-group ">
                            <label for="school_name">School Name</label>
                            <input type="text" class="form-control" id="school_name" name="school_name" placeholder="Enter School Name">
                        </div>
                        <div class="form-group ">
                            <label for="email">Email</label>
                            <input required type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                        </div>
                        <div class="form-group ">

                            <div class="col-xs-3">
                                <label for="work_number">Work Number</label>
                            </div>
                            <div class="col-xs-4">
                                <select required id="country_code" name="country_code" class="form-control">
                                    <option value="">Select Code</option>
                                    <?php
                                    if (!empty($countries)):
                                        foreach ($countries as $country):
                                            ?>
                                            <option value="<?= $country->code ?>"><?= $country->country . '-' . $country->code; ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                            <div class="col-xs-5">
                                <input type="text" class="form-control" id="work_number" name="work_number" data-inputmask="'mask': ['999-999-9999', '+099 99 99 9999[9]']" data-mask="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password1">Number of Staffs</label>
                            <input type="text" class="form-control" id="n_users" name="n_users" placeholder="Number of Staffs">
                        </div>
                        <div class="form-group">
                            <label for="subdomain">Subdomain</label>
                            <input type="text" class="form-control" id="subdomain" name="subdomain" placeholder="subdomain">
                        </div>
                        <div class="form-group">
                            <label for="subdomain">Account Status</label>
                            <select required id="account_status" name="account_status" class="form-control">
                                <option value="">Select Account Status</option>
                                <?php foreach ($account_statuses as $value => $account_status): ?>
                                    <option value="<?= $value ?>"><?= $account_status ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="subdomain">Account Type</label>
                            <select required id="account_type" name="account_type" class="form-control">
                                <option value="">Select Account Type</option>
                                <?php foreach ($account_types as $value => $account_type): ?>
                                    <option value="<?= $value ?>"><?= $account_type ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Background Color:</label>
                            <div class="input-group my-colorpicker2 colorpicker-element">
                                <input type="text" name="bg_color" id="bg_color" class="form-control">
                                <div class="input-group-addon">
                                    <i></i>
                                </div>
                            </div><!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label for="password1">Password</label>
                            <input type="password" class="form-control" id="password1" name="password1" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label for="password2">Confirm Password</label>
                            <input type="password" class="form-control" id="password2" name="password2" placeholder="Password">
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div><!-- /.box -->


        </div><!--/.col (left) -->
        <!-- right column -->

    </div>   <!-- /.row -->
</section><!-- /.content -->

<script src="/js/school.js"></script>