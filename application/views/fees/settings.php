<?php show_notification(); ?>

<section class="content-header">
	<h1>
		Settings
		<small>(Defines each fee items)</small>
	</h1>
	<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Fees</a></li>
		<li><a href="#">Settings</a></li>
    </ol>
</section>

<section class="content">
	<?php
		if(!empty($items)) :
	?>
	
	<div class="box">
		<div class="box-header">
		<h3 class="box-title">Available Items</h3>
		</div>

		<div class="box-body">
			<table class="table table-bordered">
				<tr>
					<th style="width: 10px">#</th>
					<th>Items</th>
					<th>Amount(N)</th>
					<th style="width: 40%">Action</th>
				</tr>
				<?php 
					$cnt = 1;
					foreach ($items as $i) : 
				?>
				<tr>
					<td><?= $cnt ?></td>
					<td><?= $i->item ?></td>
					<td><?= $i->amount ?></td>
					<td>
						<a class="edit-item" data-id="<?= $i->id ?>" data-item="<?= $i->item ?>" data-descr="<?= $i->description ?>" data-amt="<?= $i->amount ?>"><div class="col-md-3 col-sm-4"><i class="fa fa-fw fa-pencil-square-o"></i> Edit</div></a> 
						<a class="remove-item" data-id="<?= $i->id ?>"><div class="col-md-3 col-sm-4"><i class="fa fa-fw fa-trash-o"></i> Delete</div></a>
					</td>
				</tr>
				<?php 
					$cnt++; // Increase counter
					endforeach; 
				?>
			</table>
		</div>
	</div>
	
	<?php endif; ?>

	<a class="btn btn-app" href="#" id="add-fee" data-toggle="modal">
		<i class="fa fa-edit"></i> Add Item
	</a>
</section>

<!-- Modals -->
<section class="content">
	<div id="add-fee-box" class="modal fade" role="dialog">
		<form method="post" id="frmsettings">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Add New Fee Item</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="item">Item Name</label>
							<input type="text" name="item" class="form-control" id="item" placeholder="Item">
						</div>
						<div class="form-group">
							<label for="description">Description</label>
							<textarea class="form-control" name="description" id="description" placeholder="Description" rows="5"></textarea>
						</div>
						<div class="form-group">
							<label for="item">Amount</label>
							<input type="text" name="amount" class="form-control" id="amount" placeholder="Amount">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="btn-save" class="btn btn-default" data-dismiss="modal">Save Changes</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

<!-- Delete item -->
<section class="content">
	<div id="remove-fee-box" class="modal fade" role="dialog">
		<form method="post" id="frmremoveitem" action="<?= site_url('fees/settings/deleteItem') ?>">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Remove Fee Item</h4>
					</div>
					<div class="modal-body">
						Do you wish to remove Item?
					</div>
					<input type="hidden" name="id" id="itemid"/>
					<div class="modal-footer">
						<button type="button" id="btn-remove" class="btn btn-default" data-dismiss="modal">Yes, continue</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

<!-- Edit existing item -->
<section class="content">
	<div id="edit-fee-box" class="modal fade" role="dialog">
		<form method="post" id="frmedititem" action="<?= site_url('fees/settings/updateItem') ?>">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Edit Fee Item</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="item">Item Name</label>
							<input type="text" name="item" class="form-control" id="eitem" placeholder="Item">
						</div>
						<div class="form-group">
							<label for="description">Description</label>
							<textarea class="form-control" name="description" id="edescription" placeholder="Description" rows="5"></textarea>
						</div>
						<div class="form-group">
							<label for="item">Amount</label>
							<input type="text" name="amount" class="form-control" id="eamount" placeholder="Amount">
						</div>
						<input type="hidden" id="eid" name="id"/>
					</div>
					<div class="modal-footer">
						<button type="button" id="btn-edit" class="btn btn-default" data-dismiss="modal">Save Changes</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>

<script>
	$('#add-fee').click(function(){
		$('#add-fee-box').modal('show');
	});
	
	$('#btn-save').click(function(){
		$('form#frmsettings').submit();
	});
	
	// Remove item from fee list
	$('.remove-item').click(function(){
		$('#itemid').val($(this).data('id'));
		$('#remove-fee-box').modal('show');
	});
	
	$('#btn-remove').click(function(){
		$('form#frmremoveitem').submit();
	});
	
	// Edit exsiting item
	$('.edit-item').click(function(){
		$('#eitem').val($(this).data('item'));
		$('#edescription').val($(this).data('descr'));
		$('#eamount').val($(this).data('amt'));
		$('#eid').val($(this).data('id'));
		$('#edit-fee-box').modal('show');
	});
	
	$('#btn-edit').click(function(){
		$('form#frmedititem').submit();
	});
</script>