<?=
show_notification();

if (!empty($current_ses_term)):
    $session_id = $current_ses_term[0]->session_id;
    $term_id = $current_ses_term[0]->term_id;
else:
    $session_id = '';
    $term_id = '';
endif;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Set Current Session &amp; Term
    </h1>

</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <!--<h3 class="box-title">Quick Example</h3>-->
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post">

                    <div class="box-body">
                        <table class="table">
                            <tr>
                                <td>
                                    <label for="session_id">Session</label>
                                </td>
                                <td>
                                    <select class="form-control" name="session_id" id="session_id">
                                        <option value="0" selected>Select Session </option>
                                        <?php
                                        if (!empty($sessions)):
                                            $sel = 'selected';
                                            foreach ($sessions as $session):
                                                if ($session->session_id == $session_id):
                                                    $sel = 'selected';
                                                else:
                                                    $sel = '';
                                                endif;
                                                ?>
                                                <option value="<?= $session->session_id ?>" <?= $sel; ?>><?= $session->session_name ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="session_id">Term</label>
                                </td>
                                <td>
                                    <select class="form-control" name="term_id" id="term_id">
                                        <option value="" selected>Select Term </option>
                                        <?php
                                        if (!empty($terms)):
                                            $sel = '';
                                            foreach ($terms as $term):
                                                if ($term->term_id == $term_id):
                                                    $sel = 'selected';
                                                else:
                                                    $sel = '';
                                                endif;
                                                ?>
                                                <option value="<?= $term->term_id ?>" <?= $sel?>><?= $term->term_name ?></option>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </td>
                            </tr>

                        </table>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div><!-- /.box -->


        </div><!--/.col (left) -->
        <!-- right column -->

    </div>   <!-- /.row -->
</section><!-- /.content -->

<script src="/js/school.js"></script>