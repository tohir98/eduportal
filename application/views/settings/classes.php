<?= show_notification(); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Classes
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active">Add Class</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">
                        <a href="#modal_add_class" data-toggle="modal" class="btn btn-success">Add New Class</a>
                    </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">


                    <?php
                    if (!empty($classes)):
                        foreach ($classes as $class):
                            ?>
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <?= $class['class_name'] ?> | <a href="#" onclick="return false;" data-class_id ="<?= $class['class_id'] ?>" data-class_name ="<?= $class['class_name'] ?>" class="classarm">Add Class Arm</a> | 

                                        <a href="#" onclick="return false;" class="edit">Edit</a> |
                                        <a href="<?= site_url('settings/class/delete/' . $class['class_id']) ?>" onclick="return false;" class="delete">Delete</a>
                                        <a href="#" onclick="return false;" class="edit">Edit</a> | <a href="#" onclick="return false;" data-class_id ="<?= $class['class_id'] ?>" data-class_name ="<?= $class['class_name'] ?>" class="addsubject">Add Subject</a> |
                                        <a href="#" onclick="return false;" class="edit">Delete</a> 
                                    </h4>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                                    </div><!-- /.box-tools -->
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <?php
//                            var_dump($rc['statuses']);
                                    if (!empty($class['arms'])):
                                        $cnt = 0;
                                        ?>
                                        <table class="table table-condensed table-striped table-bordered">
                                            <tr>
                                                <th>S/N</th>
                                                <th>Class Arm</th>
                                                <th>Action</th>
                                            </tr>
                                            <?php foreach ($class['arms'] as $arm): ?>
                                                <tr>
                                                    <td><?= ++$cnt; ?></td>
                                                    <td><?= $arm['class_arm'] ?></td>
                                                    <td>
                                                        <a class="tagToggle" data-enabled="<?= intval($arm['status']) ?>" href="<?= site_url('/settings/class/edit_arm_status' . $arm['class_arm_id']); ?>">
                                                            <?= $arm['status'] ? 'Disable' : 'Enable' ?>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php endforeach;
                                            ?>
                                        </table>
                                    <?php endif;
                                    ?>


                                </div><!-- /.box-body -->
                            </div>
                            <?php
                        endforeach;
                    endif;
                    ?>

                </div><!-- /.box-body -->
            </div><!-- /.box -->


        </div><!--/.col (left) -->
        <!-- right column -->

    </div>   <!-- /.row -->
</section><!-- /.content -->

<div class="modal" id="modal_add_class">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New Class</h4>
            </div>
            <form role="form" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="class_name">Class Name</label>
                        <input required type="text" class="form-control" id="class_name" name="class_name" placeholder="Class Name">
                    </div>
                    <div class="form-group">
                        <label for="class_desc">Description</label>
                        <input type="text" class="form-control" id="class_desc" name="class_desc" placeholder="Description">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-classarm" ng-app="class" ng-controller="armCtrl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="class_arm_title">New Class Arm</h4>
            </div>
            <form role="form" method="post" action="<?= site_url('/settings/class/arm') ?>">
                <div class="modal-body">
                    <table class="table">
                        <tr ng-repeat="status in data.statuses">
                            <td>Arm {{$index + 1}}</td>
                            <td><input required type="text" class="form-control" name="arm[{{$index}}]" value=""></td>
                            <td>
                                <a href="#" title="Remove this step" onclick="return false;" ng-click="removeItem($index)">
                                    <i class="fa fa-fw fa-trash-o"></i> remove
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <a title="Click here to add" class="btn btn-warning pull-right" onclick="return false;" ng-click="addItem()">
                                    <i class="icons icon-plus"></i>
                                    Add more
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="class_id" id="class_id" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="modal-addsubject" ng-app="class" ng-controller="subjectCtrl">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="class_arm_title">Add Class Subjects</h4>
            </div>
            <form role="form" method="post" action="<?= site_url('/settings/subject/add_class_subject') ?>">
                <div class="modal-body">
                    <?php if (!empty($subjects)): ?>
                        <table class="table">
                            <?php
                            foreach ($subjects as $subject):
                                ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="subject[]" id="subject<?= $subject->subject_id ?>" value="<?= $subject->subject_id ?>" > <label for="subject<?= $subject->subject_id ?>"><?= $subject->subject_name; ?></label>
                                    </td>
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        </table>
                    <?php
                    else:
                        $sub_link = site_url('settings/subject');
                        echo show_no_data("No subject has been added. <a href=$link>Click here to add subject</a>");
                    endif;
                    ?>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="class_id" id="subject_class_id" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Add Subjects</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('.classarm').click(function () {
        var class_name_ = $(this).data('class_name');
        var class_id_ = $(this).data('class_id');

        var text_ = 'Class Arm for ' + class_name_;

        $('#class_arm_title').text(text_);
        $('#class_id').val(class_id_);
        $('#modal-classarm').modal();
    });

    $('.addsubject').click(function () {
        var class_name_ = $(this).data('class_name');
        var class_id_ = $(this).data('class_id');

        var text_ = 'Subject for ' + class_name_;

        $('#subject_title').text(text_);
        $('#subject_class_id').val(class_id_);
        $('#modal-addsubject').modal();
    });

    var classApp = angular.module('class', []);

    classApp.controller('armCtrl', function ($scope) {

        $scope.data = {statuses: []};

        $scope.blankResult = {status: ''};

        $scope.addItem = function () {
            $scope.data.statuses.push(angular.copy($scope.blankResult));
        };

        $scope.getSittingNo = function () {
            return $scope.data.sittings.length;
        };

        $scope.removeItem = function (idx) {
            if ($scope.data.statuses.length > 0) {
                $scope.data.statuses.splice(idx, 1);
            }
            $scope.check();
        };

        $scope.check = function () {
            if ($scope.data.statuses.length == 0) {
                $scope.addItem();
            }
        };

        $scope.check();



    });
</script>
<script>
    $(function () {
        $('.delete').click(function (e) {
            e.preventDefault();
            var h = this.href;
            var message = parseInt($(this).data('enabled')) ? 'If you disable this Institution, applicants \'ll no longer be able to apply to it, Are you sure you want to continue? ' : 'Are you sure you want to delete class ?';
            EduPortal.doConfirm({
                title: 'Delete Class',
                message: message,
                onAccept: function () {
                    window.location = h;
                }
            });
        });
    });
</script>