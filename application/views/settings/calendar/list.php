<?= show_notification(); ?>
<section class="content-header">
    <h1>
        Academic Calendar
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('/admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active">Academic Calendar</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Calendar
                    </h3>
                    <a href="#modal-calendar" data-toggle="modal" class="btn btn-warning pull-right"> Add Calendar</a>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <?php
                    if (!empty($calendar)):
                        ?>
                        <div class="well">
                            Term Begin: <?= $calendar->term_begin ?><br>
                            Term Ends on : <?= $calendar->term_end ?><br>
                            Mid Term: <?= $calendar->midterm_start ?> to <?= $calendar->midterm_end ?>
                        </div>
                        <?php
                    else:
                        echo show_no_data('Academic calendar not set');
                    endif;
                    ?>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>

<div class="modal" id="modal-calendar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="class_arm_title">Set Academic Calendar</h4>
            </div>

            <form role="form" method="post" action="<?= site_url('/settings/calendar') ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="label label-success" id="subjectName"></div>
                        <select required="" name="session_id" id="session_id" class="form-control">
                            <option value="">Select Session</option>
                            <?php
                            if (!empty($sessions)):
                                foreach ($sessions as $session):
                                    ?>
                                    <option value="<?= $session->session_id ?>"><?= $session->session_name ?></option>
                                <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select required name="term_id" id="term_id" class="form-control">
                            <option value="">Select Term</option>
                            <?php
                            if (!empty($terms)):
                                foreach ($terms as $term):
                                    ?>
                                    <option value="<?= $term->term_id ?>"><?= ucfirst($term->term_name) ?></option>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control datepick" name="term_begin" id="term_begin" placeholder="Date Term Begin" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control datepick" name="term_end" id="term_end" placeholder="Date term will end" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control datepick" name="midterm_start" id="midterm_start" placeholder="Mid term start date" />
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control datepick" name="midterm_end" id="midterm_end" placeholder="Mid term end date" />
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="hidden" id="school_id" name="school_id" value="<?= $school_id ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>