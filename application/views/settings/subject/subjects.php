<?= show_notification(); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Subjects
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Settings</a></li>
        <li class="active">Add Subject</li>
    </ol>
</section>

<section class="content" ng-app="subject" ng-controller="subjectCtrl">
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">
                        <a href="#modal_add_subject" data-toggle="modal" class="btn btn-success">Add New Subject</a>
                    </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">


                    <?php if (!empty($subjects)): ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>SN  </th>
                                    <th>Subject Name  </th>
                                    <th>Description</th>
                                    <th>Subject Teacher</th>
                                    <th>Action  </th>
                                </tr>
                            </thead>
                            <?php
                            $j = 0;
                            foreach ($subjects as $subject):
                                ?>
                                <tr>
                                    <td><?= ++$j ?></td>
                                    <td><?= $subject->subject_name ?></td>
                                    <td><?= $subject->subject_desc ?></td>
                                    <td><?= ucfirst($subject->first_name) . ' ' . ucfirst($subject->last_name) ?></td>
                                    <td>
                                        <a href="#" onclick="return false;" ng-click="assign_subject_teacher('<?= $subject->subject_name ?>', '<?= $subject->subject_id ?>')" >Assign Class Teacher</a> | 
                                        <a href="#" onclick="return false;" ng-click="editSubject('<?= site_url('settings/subject/edit_subject/'.$subject->subject_id) ?>')">Edit</a> | 
                                        <a href="#" onclick="return false" ng-click="delete('<?= site_url('settings/subject/delete/' . $subject->subject_id) ?>', '<?= $subject->subject_name ?>')" class="delete">Delete</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>

                    <?php endif;
                    ?>

                </div><!-- /.box-body -->
            </div><!-- /.box -->


        </div><!--/.col (left) -->
        <!-- right column -->

    </div>   <!-- /.row -->
</section><!-- /.content -->

<div class="modal" id="modal_add_subject">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">New Subject</h4>
            </div>
            <form role="form" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="class_name">Subject Name</label>
                        <input required type="text" class="form-control" id="subject_name" name="subject_name" placeholder="Subject Name">
                    </div>
                    <div class="form-group">
                        <label for="class_desc">Description</label>
                        <input type="text" class="form-control" id="subject_desc" name="subject_desc" placeholder="Description">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal" id="modal-subject_teacher">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="class_arm_title">Assign Subject</h4>
            </div>

            <form role="form" method="post" action="<?= site_url('/settings/subject/assign') ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="label label-success" id="subjectName"></div>
                        <input type="hidden" id="subject_id" name="subject_id">
                    </div>
                    <div class="form-group">
                        <select required name="employee_id" id="employee_id" class="form-control">
                            <option value="">Select Employee</option>
                            <?php
                            if (!empty($employees)):
                                foreach ($employees as $employee):
                                    ?>
                                    <option value="<?= $employee->employee_id ?>"><?= ucfirst($employee->first_name) . ' ' . ucfirst($employee->last_name) ?></option>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary" >Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal" id="modal-edit_subject">

</div>


<script>

    $('body').delegate('.view_details', 'click', function (evt) {

        evt.preventDefault();
        $('#viewDetailsModal').modal('show').fadeIn();
        $('#viewDetailsModal').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#viewDetailsModal').html('');
            $('#viewDetailsModal').html(html).show();

        });

        return false;
    });

    var subjectApp = angular.module('subject', []);

    subjectApp.controller('subjectCtrl', function ($scope) {

        $scope.delete = function (href, subjectName) {
            EduPortal.doConfirm({
                title: 'Confirm Subject Delete',
                message: 'Are you sure you want to delete this subject (' + subjectName + ')',
                onAccept: function () {
                    window.location = href;
                }
            });
        };

        $scope.assign_subject_teacher = function (subjectName, subjectId) {
            $('#subjectName').text(subjectName);
            $('#subject_id').val(subjectId);
            $('#modal-subject_teacher').modal();
        };

        $scope.editSubject = function (page) {
            $('#modal-edit_subject').modal('show').fadeIn();

            $.get(page, function (html) {

                $('#modal-edit_subject').html('');
                $('#modal-edit_subject').html(html).show();

            });
        };


    });


</script>