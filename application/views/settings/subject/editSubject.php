<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Edit Subject</h4>
        </div>
        <form role="form" method="post" action="<?= site_url('settings/subject/edit_subject/'.$subject_info->subject_id)?>">
            <div class="modal-body">
                <div class="form-group">
                    <label for="class_name">Subject Name</label>
                    <input required type="text" class="form-control" id="subject_name" name="subject_name" placeholder="Subject Name" value="<?= $subject_info->subject_name ?>">
                </div>
                <div class="form-group">
                    <label for="class_desc">Description</label>
                    <input type="text" class="form-control" id="subject_desc" name="subject_desc" placeholder="Description" value="<?= $subject_info->subject_desc ?>" >
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary btn-flat" >Update</button>
            </div>
        </form>
    </div>
</div>
