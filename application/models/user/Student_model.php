<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of student_model
 *
 * @author user
 * 
 * @property User_auth_lib $user_auth_lib Description
 */
class Student_model extends CI_Model {

    const MODULE_ID = 1;

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_admission_no($school_id) {

        $student = $this->db->get_where(TBL_STUDENTS, ['school_id' => $school_id])->result();
        if (empty($student)) {
            return 1;
        }

        return (int) count($student) + 1;
    }

    public function save_student($school_id, $data, $profile) {
        $this->db->trans_start();

        if (!empty($profile) && $profile['name'] !== '') {

            $ext = end((explode(".", $profile['name'])));

            $config = array(
                'upload_path' => FILE_PATH_STUDENT_PROFILE,
                'allowed_types' => "jpg|jpeg|gif|png",
                'overwrite' => TRUE,
                'max_size' => "102400", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                'max_height' => "300",
                'max_width' => "300",
            );

            $profile_pic_name = md5(microtime()) . '.' . $ext;
            $config['file_name'] = $profile_pic_name;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                $error = array('error' => $this->upload->display_errors());
                notify('error', 'Invalid file uploaded');
                return FALSE;
            }

            $data['picture'] = $profile_pic_name;
        }
        $data['school_id'] = $school_id;
        $data['date_created'] = date('Y-m-d h:i:s');
        $data['dateof_birth'] = date('Y-m-d', strtotime($data['dateof_birth']));
        $data['created_by'] = $this->user_auth_lib->get('user_id');
        $this->db->insert(TBL_STUDENTS, $data);
        $student_id = (int) $this->db->insert_id();

        $this->db->trans_complete();

        return $student_id;
    }

    public function save_emergency_contact($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        return $this->db->insert(TBL_EMERGENCY, $data);
    }

    public function save_guardian($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        return $this->db->insert(TBL_GUARDIAN, $data);
    }

    public function fetchStudent($school_id, $admission_no = '') {
        if ($admission_no !== '') {
            $this->db->where('s.admission_no', $admission_no);
        }
        $students = $this->db->select('s.*, r.religion, g.gender, c.class_name, b.blood_group, c_t.country, st.state, l.language, guard.first_name g_fname, guard.last_name g_lname, guard.mobile_phone g_phone, guard.email g_email')
                ->from(TBL_STUDENTS . ' as s')
                ->join(TBL_GUARDIAN . ' as guard', 'guard.student_id=s.student_id', 'left')
                ->join('religion as r', 'r.religion_id=s.religion_id', 'left')
                ->join('gender as g', 'g.gender_id=s.gender_id', 'left')
                ->join('classes as c', 'c.class_id=s.class_id', 'left')
                ->join('blood_groups as b', 'b.blood_group_id=s.blood_group_id', 'left')
                ->join('countries as c_t', 'c_t.country_id=s.country_id', 'left')
                ->join('states as st', 'st.state_id=s.state_id', 'left')
                ->join('languages as l', 'l.language_id=s.language_id', 'left')
                ->where('s.school_id', $school_id)
                ->where('s.status', 1)
                ->get()->result();

        return $students;
    }

    public function getStudentIdByAdmNo($admission_no, $school_id) {
        return $this->db->select('student_id')
                ->from(TBL_STUDENTS)
                ->where('admission_no', $admission_no)
                ->where('school_id', $school_id)
                ->get()->row()->student_id;
    }

    public function fetch_emergency_contacts($admission_no, $school_id) {
        $student_id = $this->getStudentIdByAdmNo($admission_no, $school_id);
        return $this->db->get_where(TBL_EMERGENCY, ['student_id' => $student_id])->result();
    }

    public function fetch_guardian_details($admission_no, $school_id) {
        $student_id = $this->getStudentIdByAdmNo($admission_no, $school_id);
        return $this->db->get_where(TBL_GUARDIAN, ['student_id' => $student_id])->result();
    }

    public static function get_guardian_info($email) {
        return $this->db->get_where(TBL_GUARDIAN, ['email' => $email])->row();
    }

    public function fetchStudentByClassArm($class_id) {
        return $this->db
            ->select('s.*')
            ->from(TBL_STUDENTS . ' as s')
            ->where('s.class_id', $class_id)
            ->get()->result();
    }

}
