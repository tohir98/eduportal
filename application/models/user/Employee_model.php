<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of employee_model
 *
 * @author TOHIR
 * @property User_auth_lib $user_auth_lib Description
 */
class Employee_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetchEmployee($school_id, $staff_id = null) {
        if ($staff_id) {
            $this->db->where('e.staff_id', $staff_id);
        }
        return $this->db->select('e.*, s.state, c.country, g.gender, m.marital_status, u.user_type')
                ->from(TBL_EMPLOYEES . ' as e')
                ->join(TBL_USERS . ' as u', 'u.user_id=e.user_id')
                ->join(TBL_STATE . ' as s', 's.state_id=e.state_id', 'left')
                ->join('countries as c', 'c.country_id=e.country_id', 'left')
                ->join(TBL_GENDER . ' as g', 'g.gender_id=e.gender_id', 'left')
                ->join(TBL_MARITAL_STATUS . ' as m', 'm.marital_status_id=e.marital_status_id', 'left')
                ->where('e.school_id', $school_id)
                ->get()->result();
    }

    public function add_staff($school_id, $data, $profile) {
        $this->db->trans_start();
        // create default users
        $user_data = array(
            'username' => $data['email'],
            'password' => $this->user_auth_lib->encrypt("password"),
            'created_at' => date('Y-m-d h:i:s'),
            'status' => USER_STATUS_ACTIVE,
            'user_type' => USER_TYPE_STAFF,
            'school_id' => $school_id
        );
        $this->db->insert(TBL_USERS, $user_data);
        $user_id = $this->db->insert_id();

        $employee_data = array(
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'address' => $data['address'],
            'city' => $data['city'],
            'state_id' => $data['state_id'],
            'gender_id' => $data['gender_id'],
            'marital_status_id' => $data['marital_status_id'],
            'dateof_birth' => date('Y-m-d', strtotime($data['dateof_birth'])),
            'employment_date' => date('Y-m-d', strtotime($data['employment_date'])),
            'status' => $data['account_status'],
            'nationality_id' => $data['nationality_id'],
            'school_id' => $school_id,
            'phone' => $data['country_code'] . trim(str_replace('-', '', $data['work_number'])),
            'user_id' => $user_id,
            'staff_id' => $data['staff_id'],
        );

        if (!empty($profile) && $profile['name'] !== '') {

            $ext = end((explode(".", $profile['name'])));

            $config = array(
                'upload_path' => FILE_PATH_STAFF_PROFILE,
                'allowed_types' => "jpg|jpeg|gif|png",
                'overwrite' => TRUE,
                'max_size' => "102400", // Can be set to particular file size , here it is 2 MB(2048 Kb)
                'max_height' => "300",
                'max_width' => "300",
            );

            $profile_pic_name = md5(microtime()) . '.' . $ext;
            $config['file_name'] = $profile_pic_name;

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload()) {
                $error = array('error' => $this->upload->display_errors());

                notify('error', 'Invalid file uploaded');
                return FALSE;
            }

            $employee_data['profile_pic_name'] = $profile_pic_name;
        }

        $this->db->insert(TBL_EMPLOYEES, $employee_data);
        $this->db->trans_complete();

        return True;
    }
    
    public function delete_staff($staff_id) {
       
    }

}
