<?php

/**
 * Description of Settings_model
 *
 * @author chuksolloh
 * @property CI_DB_active_record $db
 */

class Settings_model extends CI_Model {
	//put your code here
	
	public function __construct() {
		parent::__construct();
		
		$this->load->database();
	}
	
	public function addItem($params = array()) {
		if(!empty($params)) {
			$ret = $this->db->insert('feeitems', array(
				'item' => $params['item'],
				'description' => $params['description'],
				'amount' => floatval($params['amount']),
				'school_id' => $this->user_auth_lib->get('school_id')
			));
			
			return $ret ? : false;
		}
	}
	
	public function editItem($params = array()) {
		if(!empty($params)) {
			$ret = $this->db->update('feeitems', array(
				'item' => $params['item'],
				'description' => $params['description'],
				'amount' => $params['amount']
			), array(
				'id' => $params['id']
			));
			
			return $ret ? : false;
		}
	}
	
	public function deleteItem($params = array()) {
		if(!empty($params)) {
			$ret = $this->db->delete('feeitems', array('id' => $params['id']));
			return $ret ? : false;
		}
	}
	
	public function pullItems() {
		return $this->db->get_where('feeitems', array('school_id' => $this->user_auth_lib->get('school_id')))->result();
	}
}
