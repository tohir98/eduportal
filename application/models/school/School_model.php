<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Euportal
 * School Model
 * 
 * @category   Model
 * @package    School
 * @subpackage Profile
 * @author     Tohir O. <otcleantech@gmail.com>
 * @copyright  Copyright © 2015 Eduportal Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 * @property CI_DB_active_record $db database
 */
class School_model extends CI_Model {

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    protected static $account_type;
    protected static $statuses;

    const TBL_SCHOOLS = 'schools';

    public function __construct() {
        parent::__construct();
        $this->load->database();

        static::$account_type = [ACCOUNT_TYPE_FREE_TRIAL => 'Free Trial', ACCOUNT_TYPE_PREMIUM => 'Premium'];
        static::$statuses = [ACCOUNT_STATUS_ACTIVE => 'Active', ACCOUNT_STATUS_INACTIVE => 'Inactive'];
    }

    public function load_school($subdomain) {

        $sql = "select * from schools where string_id = '" . $subdomain . "' and status = 1 ";

        $result = $this->db->query($sql)->result();

        if (!$result) {
            return false;
        }

        $result[0]->subdomain = $subdomain;
        $result[0]->child_subdomain = '';

        return $result;
    }

    public function add_school(array $data = []) {
        $this->db->trans_start();
        $school_data = array(
            'school_name' => $data['school_name'],
            'string_id' => $data['subdomain'],
            'range_of_employees' => $data['n_users'],
            'account_type_id' => $data['account_type'],
            'status' => $data['account_status'],
            'date_registered' => date('Y-m-d h:i:s'),
            'bg_color' => $data['bg_color'],
            'prefix_id' => strtoupper(substr($data['school_name'], 0, 3))
        );

        $this->db->insert(self::TBL_SCHOOLS, $school_data);
        $school_id = $this->db->insert_id();

        $user_data = array(
            'username' => $data['email'],
            'password' => sha1($data['password']),
            'created_at' => date('Y-m-d h:i:s'),
            'status' => $data['account_status'],
            'user_type' => USER_TYPE_ADMIN,
            'school_id' => $school_id
        );
        
        $this->db->insert(TBL_USERS, $user_data);
        $user_id = $this->db->insert_id();
        
        
        $employee_data = array(
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'status' => $data['account_status'],
            'school_id' => $school_id,
            'user_id' => $user_id,
            'staff_id' => strtoupper(substr($data['school_name'], 0, 3)).'00001',
        );
        
        $this->db->insert(TBL_EMPLOYEES, $employee_data);
        $this->db->trans_complete();
        
        return TRUE;

    }

    public function fetchAllSchools() {
        return $this->db->get_where(self::TBL_SCHOOLS)->result();
    }

    public static function getAccountTypes() {
        return static::$account_type;
    }
    
    public static function getAccountStatuses() {
        return static::$statuses;
    }
    
    public function get_prefix($school_id){
        return $this->db->get_where(TBL_SCHOOLS, ['school_id' => $school_id])->row()->prefix_id;
    }

}

// end class: School_model
