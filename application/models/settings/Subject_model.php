<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Subject_model
 *
 * @author TOHIR
 */
class Subject_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function subjectExist($subject_name, $school_id) {
        return $this->db->get_where(TBL_SUBJECTS, ['subject_name' => $subject_name, 'school_id' => $school_id])->result();
    }

    public function add_subject($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }
        if (!$this->subjectExist($data['subject_name'], $this->user_auth_lib->get('school_id'))) {
            return $this->db->insert(TBL_SUBJECTS, array_merge($data, ['school_id' => $this->user_auth_lib->get('school_id')]));
        } else {
            return FALSE;
        }
    }

    public function fetch_subjects($school_id) {
        return $this->db
                ->select('s.*, e.first_name, e.last_name')
                ->from(TBL_SUBJECTS . ' as s')
                ->join(TBL_SUBJECTS_ASSIGNMENT . ' as sa', 's.subject_id=sa.subject_id', 'left')
                ->join(TBL_EMPLOYEES . ' as e', 'e.employee_id=sa.employee_id', 'left')
                ->where('s.school_id', $school_id)
                ->get()->result();
    }

    public function add_subject_assignment($data) {
        return $this->db->insert(TBL_SUBJECTS_ASSIGNMENT, $data);
    }

    public function saveClassSubjects($param) {
        if (empty($param)) {
            return FALSE;
        }

        $data = [];
        $data_db = ['class_id' => $param['class_id'], 'date_created' => date('Y-m-d h:i:s'), 'created_by' => $this->user_auth_lib->get('user_id'), 'school_id' => $this->user_auth_lib->get('school_id')];

        if (!empty($param['subject'])) {
            foreach ($param['subject'] as $subject_id) {
                $data[] = array_merge($data_db, ['subject_id' => $subject_id]);
            }
        }

        return !empty($data) ? $this->db->insert_batch(TBL_CLASS_SUBJECTS, $data) : FALSE;
    }

    public function fetch_class_subjects($class_id, $school_id) {
        return $this->db->select('c_sub.*, sub.subject_name')
                ->from(TBL_CLASS_SUBJECTS . ' as c_sub')
                ->join(TBL_SUBJECTS . ' as sub', 'c_sub.subject_id=sub.subject_id')
                ->where('c_sub.class_id', $class_id)
                ->where('c_sub.school_id', $school_id)
                ->get()->result();
    }

}
