<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of session_model
 *
 * @author TOHIR
 */
class session_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function sessionExist($session_name, $school_id) {
        return $this->db->get_where(TBL_SESSION, ['session_name' => $session_name, 'school_id' => $school_id])->result();
    }

    public function add_session($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }
        if (!$this->sessionExist($data['session_name'], $this->user_auth_lib->get('school_id'))) {
            return $this->db->insert(TBL_SESSION, array_merge($data, ['school_id' => $this->user_auth_lib->get('school_id')]));
        } else {
            return FALSE;
        }
    }

    public function delete_session($id) {
        return $this->db->where('session_id', $id)->delete(TBL_SESSION);
    }

    public function set_current_session($school_id, array $data = []) {
        if (empty($data)) {
            return FALSE;
        }

        $check = $this->db->get_where(TBL_CURRENT_SESS_TERM, ['school_id' => $school_id])->result();

        if (empty($check)) {
            $this->db->insert(TBL_CURRENT_SESS_TERM, array_merge($data, ['school_id' => $school_id]));
        } else {
            $this->db->where('school_id', $school_id)
                ->update(TBL_CURRENT_SESS_TERM, $data);
        }
        return;
    }

}
