<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of class_model
 *
 * @author TOHIR
 */
class Class_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function classExist($class_name, $school_id) {
        return $this->db->get_where(TBL_CLASSES, ['class_name' => $class_name, 'school_id' => $school_id])->result();
    }

    public function add_class($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }
        if (!$this->classExist($data['class_name'], $this->user_auth_lib->get('school_id'))) {
            return $this->db->insert(TBL_CLASSES, array_merge($data, ['school_id' => $this->user_auth_lib->get('school_id')]));
        } else {
            return FALSE;
        }
        
    }
    public function delete_class($id) {
        return $this->db->where('class_id', $id)->delete(TBL_CLASSES);
    }

    public function add_classarm($data) {
        if (!is_array($data) || empty($data)) {
            return FALSE;
        }

        if (!is_array($data['arm']) || empty($data['arm'])) {
            return FALSE;
        }

        $data_db = [];
        foreach ($data['arm'] as $arm) {
            $data_db[] = ['class_id' => $data['class_id'], 'class_arm' => $arm];
        }
        !empty($data_db) ? $this->db->insert_batch(TBL_CLASS_ARM, $data_db) : '';
        
        return TRUE;
    }

    public function get_classes($school_id) {
        return $this->db->get_where(TBL_CLASSES, ['school_id' => $school_id])->result();
    }

    public function get_class_arms($class_id) {
        return $this->db->get_where(TBL_CLASS_ARM, ['class_id' => $class_id])->result_array();
    }

    public function fetchClasses($school_id) {
        $classes = $this->get_classes($school_id);
        if (empty($classes)) {
            return FALSE;
        }

        $result = [];

        foreach ($classes as $class) {
            $result[] = array_merge((array) $class, ['arms' => $this->get_class_arms($class->class_id)]);
        }


        return $result;
    }

}
