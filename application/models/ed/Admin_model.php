<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Eduportal
 * Admin Model
 * 
 * @category   Model
 * @package    Admin
 * @subpackage Admin
 * @author     Tohir O <otcleantech@gmail.com>
 * @copyright  Copyright © 2015 Eduportal Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class Admin_model extends CI_Model {

    const TBL_SCHOOLS = 'schools';
    const TBL_ADMINS = 'ed_admins';

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

// End func __construct

    public function fetch_account($query_fields) {

        if (empty($query_fields)) {
            trigger_error('query fields cannot be empty!', E_USER_WARNING);
        }

        $sql = "SELECT * "
            . "FROM " . self::TBL_ADMINS
            . " WHERE admin_id <> -99 ";

        foreach ($query_fields as $field => $value) {

            $sql .= "AND " . $field . " = '" . $value . "' ";
        }

        $result = $this->db->query($sql)->result();

        if (empty($result)) {
            return FALSE;
        } else {
            return $result;
        }
    }
    
    public function fetch_admin_groups_modules_perms($id_admin) {
       
        if (empty($id_admin)) {
            return false;
        }

        $sql = "SELECT 
            rp.role_id,
            rp.in_menu,
            rp.subject AS perm_subject,
            rp.id_string AS perm_id_string,
            m.menu_id,
            m.subject AS menu_subject,
            m.id_string AS menu_id_string 
          FROM
            ed_role_perms AS rp 
            INNER JOIN ed_admins AS a
            INNER JOIN ed_menu m 
              ON m.menu_id = rp.menu_id 
            INNER JOIN ed_roles r1 
              ON r1.`role_id` = a.`role_id` 
          WHERE a.admin_id = '" . intval($id_admin) . "'
			AND rp.role_id = r1.role_id
            AND rp.`role_id` IN 
            (SELECT role_id FROM ed_roles AS r WHERE r.auth_level <= r1.auth_level) 
        ORDER BY m.sort_id, rp.menu_id";

        $result = $this->db->query($sql)->result_array();

        if (empty($result)) {
            return false;
        }
        return $result;
    }

}   // end class: admin_model
