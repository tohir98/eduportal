<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * EduPortal
 * Reference Model - Fetching reference data from any table.
 * 
 * @category   Model
 * @package    Reference
 * @subpackage Reference
 * @author     Tohir O. <otcleantech@gmail.com>
 * @copyright  Copyright © 2015 Eduportal Nigeria Ltd.
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */
class Reference_model extends CI_Model {

    const TBL_COUNTRIES = 'countries';

    /**
     * Class constructor
     * 
     * @access public
     * @return void
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetch_all_records($table, $where = NULL) {

        if (!$this->db->table_exists($table)) {
            trigger_error("Table `" . $table . "` not exists.", E_USER_ERROR);
        }

        if (is_null($where)) {
            $result = $this->db->get($table)->result();
        } else {
            $result = $this->db->get_where($table, $where)->result();
        }

        if (empty($result)) {
            return false;
        }

        return $result;
    }

    /**
     * Class class_name
     * 
     * @access public
     * @return void
     */
    public function delete($table, $array) {
        $this->db->where($array);
        $q = $this->db->delete($table);
        return $q;
    }

    public function update($table, $data, $where) {
        $this->db->where($where);
        $query = $this->db->update($table, $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

}

// End class : reference_model
