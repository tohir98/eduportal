<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Comm_model
 *
 * @author TOHIR
 */
class Comm_model extends CI_Model {

    const MODULE_ID = 4;
    const TBL_SENT_EMAIL = 'sent_emails';
    const TBL_SENT_SMS = 'sms_messages';

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetchMessages($school_id) {
        return $this->db->select('se.*, u.first_name, u.last_name')
                ->from(self::TBL_SENT_EMAIL . ' as se')
                ->join(TBL_EMPLOYEES . ' as u', 'se.sender_id=u.user_id')
                ->where('se.school_id', $school_id)
                ->get()->result();
    }

    public function fetchSms($school_id) {
        return $this->db->select('se.*, u.first_name, u.last_name')
                ->from(self::TBL_SENT_SMS . ' as se')
                ->join(TBL_EMPLOYEES . ' as u', 'se.sender_id=u.user_id')
                ->where('se.school_id', $school_id)
                ->get()->result();
    }

    public function process_email($data) {
        $this->db->insert('sent_emails', array(
            'sender_id' => $this->user_auth_lib->get('user_id'),
            'recepient' => $data['recepient'],
            'subject' => $data['email_subject'],
            'message' => $data['message'],
            'school_id' => $this->user_auth_lib->get('school_id'),
            'date_sent' => date('Y-m-d h:i:s')
        ));
        return $this->_sendEmail(['email' => $data['recepient'], 'message' => $data['message'], 'subject' => $data['email_subject']]);
    }

    private function _sendEmail($params) {
        $g_info = Student_model::get_guardian_info($params['email']);

        $mail_data = array(
            'header' => REG_CONFIRMATION,
            'first_name' => $g_info->first_name ? : $data['first_name'],
            'message' => $params['message'],
            'school_name' => $this->user_auth_lib->get('school_name'),
        );

        $msg = $this->load->view('email_templates/parent_email', $mail_data, true);

        return $this->mailer
                ->sendMessage($params['subject'], $msg, $params['email']);
    }

}
