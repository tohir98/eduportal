CREATE TABLE IF NOT EXISTS `class_subject` (
  `class_subject_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `class_id` INT UNSIGNED,
  `subject_id` INT UNSIGNED,
  `school_id` INT UNSIGNED,
  `date_created` DATETIME,
  `created_by` INT UNSIGNED,
  PRIMARY KEY (`class_subject_id`),
  CONSTRAINT `FK_class_subject_class_id` FOREIGN KEY `FK_class_subject_class_id` (`class_id`)
    REFERENCES `classes` (`class_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_class_subject_subject` FOREIGN KEY `FK_class_subject_subject` (`subject_id`)
    REFERENCES `subjects` (`subject_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_class_subject_school` FOREIGN KEY `FK_class_subject_school` (`school_id`)
    REFERENCES `schools` (`school_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE = InnoDB;
