CREATE TABLE IF NOT EXISTS `sent_emails` (
  `sent_email_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `sender_id` INT UNSIGNED,
  `recepient` VARCHAR(45),
  `subject` VARCHAR(45),
  `message` LONGTEXT,
  `school_id` INT UNSIGNED,
  `date_sent` DATETIME,
  PRIMARY KEY (`sent_email_id`)
)
ENGINE = InnoDB;