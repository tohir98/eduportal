SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `academic_calender`
-- ----------------------------
DROP TABLE IF EXISTS `academic_calender`;
CREATE TABLE `academic_calender` (
  `academic_calender_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(25) unsigned DEFAULT NULL,
  `term_id` int(25) unsigned DEFAULT NULL,
  `term_begin` date DEFAULT NULL,
  `term_end` date DEFAULT NULL,
  `midterm_start` varchar(45) DEFAULT NULL,
  `midterm_end` varchar(45) DEFAULT NULL,
  `school_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`academic_calender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of academic_calender
-- ----------------------------

-- ----------------------------
-- Table structure for `academic_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `academic_sessions`;
CREATE TABLE `academic_sessions` (
  `session_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_name` varchar(150) DEFAULT NULL,
  `session_desc` varchar(150) DEFAULT NULL,
  `school_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of academic_sessions
-- ----------------------------
INSERT INTO `academic_sessions` VALUES ('1', '2003', '2003/2004', '1');

-- ----------------------------
-- Table structure for `blood_groups`
-- ----------------------------
DROP TABLE IF EXISTS `blood_groups`;
CREATE TABLE `blood_groups` (
  `blood_group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blood_group` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`blood_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of blood_groups
-- ----------------------------
INSERT INTO `blood_groups` VALUES ('1', 'A+');
INSERT INTO `blood_groups` VALUES ('2', 'A-');
INSERT INTO `blood_groups` VALUES ('3', 'B+');
INSERT INTO `blood_groups` VALUES ('4', 'B-');
INSERT INTO `blood_groups` VALUES ('5', 'O+');
INSERT INTO `blood_groups` VALUES ('6', 'O-');
INSERT INTO `blood_groups` VALUES ('7', 'AB+');
INSERT INTO `blood_groups` VALUES ('8', 'AB-');

-- ----------------------------
-- Table structure for `classes`
-- ----------------------------
DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `class_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_name` varchar(45) DEFAULT NULL,
  `class_desc` varchar(150) DEFAULT NULL,
  `school_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `class_arm`
-- ----------------------------
DROP TABLE IF EXISTS `class_arm`;
CREATE TABLE `class_arm` (
  `class_arm_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned DEFAULT NULL,
  `class_arm` varchar(45) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`class_arm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for `countries`
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `country_id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(200) DEFAULT NULL,
  `iso_alpha2` varchar(2) DEFAULT NULL,
  `iso_alpha3` varchar(3) DEFAULT NULL,
  `iso_numeric` int(11) DEFAULT NULL,
  `currency_code` char(3) DEFAULT NULL,
  `currency_name` varchar(32) DEFAULT NULL,
  `currrency_symbol` varchar(3) DEFAULT NULL,
  `flag` varchar(6) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES ('1', 'Afghanistan', 'AF', 'AFG', '4', 'AFN', 'Afghani', '؋', 'AF.png', '93');
INSERT INTO `countries` VALUES ('2', 'Albania', 'AL', 'ALB', '8', 'ALL', 'Lek', 'Lek', 'AL.png', '355');
INSERT INTO `countries` VALUES ('3', 'Algeria', 'DZ', 'DZA', '12', 'DZD', 'Dinar', null, 'DZ.png', '213');
INSERT INTO `countries` VALUES ('4', 'American Samoa', 'AS', 'ASM', '16', 'USD', 'Dollar', '$', 'AS.png', null);
INSERT INTO `countries` VALUES ('5', 'Andorra', 'AD', 'AND', '20', 'EUR', 'Euro', '€', 'AD.png', null);
INSERT INTO `countries` VALUES ('6', 'Angola', 'AO', 'AGO', '24', 'AOA', 'Kwanza', 'Kz', 'AO.png', '244');
INSERT INTO `countries` VALUES ('7', 'Anguilla', 'AI', 'AIA', '660', 'XCD', 'Dollar', '$', 'AI.png', null);
INSERT INTO `countries` VALUES ('8', 'Antarctica', 'AQ', 'ATA', '10', '', '', null, 'AQ.png', null);
INSERT INTO `countries` VALUES ('9', 'Antigua and Barbuda', 'AG', 'ATG', '28', 'XCD', 'Dollar', '$', 'AG.png', '1268');
INSERT INTO `countries` VALUES ('10', 'Argentina', 'AR', 'ARG', '32', 'ARS', 'Peso', '$', 'AR.png', '54');
INSERT INTO `countries` VALUES ('11', 'Armenia', 'AM', 'ARM', '51', 'AMD', 'Dram', null, 'AM.png', '374');
INSERT INTO `countries` VALUES ('12', 'Aruba', 'AW', 'ABW', '533', 'AWG', 'Guilder', 'ƒ', 'AW.png', null);
INSERT INTO `countries` VALUES ('13', 'Australia', 'AU', 'AUS', '36', 'AUD', 'Dollar', '$', 'AU.png', null);
INSERT INTO `countries` VALUES ('14', 'Austria', 'AT', 'AUT', '40', 'EUR', 'Euro', '€', 'AT.png', '43');
INSERT INTO `countries` VALUES ('15', 'Azerbaijan', 'AZ', 'AZE', '31', 'AZN', 'Manat', 'ман', 'AZ.png', '994');
INSERT INTO `countries` VALUES ('16', 'Bahamas', 'BS', 'BHS', '44', 'BSD', 'Dollar', '$', 'BS.png', null);
INSERT INTO `countries` VALUES ('17', 'Bahrain', 'BH', 'BHR', '48', 'BHD', 'Dinar', null, 'BH.png', '973');
INSERT INTO `countries` VALUES ('18', 'Bangladesh', 'BD', 'BGD', '50', 'BDT', 'Taka', null, 'BD.png', '880');
INSERT INTO `countries` VALUES ('19', 'Barbados', 'BB', 'BRB', '52', 'BBD', 'Dollar', '$', 'BB.png', null);
INSERT INTO `countries` VALUES ('20', 'Belarus', 'BY', 'BLR', '112', 'BYR', 'Ruble', 'p.', 'BY.png', '1246');
INSERT INTO `countries` VALUES ('21', 'Belgium', 'BE', 'BEL', '56', 'EUR', 'Euro', '€', 'BE.png', '32');
INSERT INTO `countries` VALUES ('22', 'Belize', 'BZ', 'BLZ', '84', 'BZD', 'Dollar', 'BZ$', 'BZ.png', '501');
INSERT INTO `countries` VALUES ('23', 'Benin', 'BJ', 'BEN', '204', 'XOF', 'Franc', null, 'BJ.png', '229');
INSERT INTO `countries` VALUES ('24', 'Bermuda', 'BM', 'BMU', '60', 'BMD', 'Dollar', '$', 'BM.png', null);
INSERT INTO `countries` VALUES ('25', 'Bhutan', 'BT', 'BTN', '64', 'BTN', 'Ngultrum', null, 'BT.png', '975');
INSERT INTO `countries` VALUES ('26', 'Bolivia', 'BO', 'BOL', '68', 'BOB', 'Boliviano', '$b', 'BO.png', '591');
INSERT INTO `countries` VALUES ('27', 'Bosnia and Herzegovina', 'BA', 'BIH', '70', 'BAM', 'Marka', 'KM', 'BA.png', '387');
INSERT INTO `countries` VALUES ('28', 'Botswana', 'BW', 'BWA', '72', 'BWP', 'Pula', 'P', 'BW.png', '267');
INSERT INTO `countries` VALUES ('29', 'Bouvet Island', 'BV', 'BVT', '74', 'NOK', 'Krone', 'kr', 'BV.png', null);
INSERT INTO `countries` VALUES ('30', 'Brazil', 'BR', 'BRA', '76', 'BRL', 'Real', 'R$', 'BR.png', '55');
INSERT INTO `countries` VALUES ('31', 'British Indian Ocean Territory', 'IO', 'IOT', '86', 'USD', 'Dollar', '$', 'IO.png', null);
INSERT INTO `countries` VALUES ('32', 'British Virgin Islands', 'VG', 'VGB', '92', 'USD', 'Dollar', '$', 'VG.png', null);
INSERT INTO `countries` VALUES ('33', 'Brunei', 'BN', 'BRN', '96', 'BND', 'Dollar', '$', 'BN.png', null);
INSERT INTO `countries` VALUES ('34', 'Bulgaria', 'BG', 'BGR', '100', 'BGN', 'Lev', 'лв', 'BG.png', '359');
INSERT INTO `countries` VALUES ('35', 'Burkina Faso', 'BF', 'BFA', '854', 'XOF', 'Franc', null, 'BF.png', '226');
INSERT INTO `countries` VALUES ('36', 'Burundi', 'BI', 'BDI', '108', 'BIF', 'Franc', null, 'BI.png', '257');
INSERT INTO `countries` VALUES ('37', 'Cambodia', 'KH', 'KHM', '116', 'KHR', 'Riels', '៛', 'KH.png', '855');
INSERT INTO `countries` VALUES ('38', 'Cameroon', 'CM', 'CMR', '120', 'XAF', 'Franc', 'FCF', 'CM.png', '237');
INSERT INTO `countries` VALUES ('39', 'Canada', 'CA', 'CAN', '124', 'CAD', 'Dollar', '$', 'CA.png', '1');
INSERT INTO `countries` VALUES ('40', 'Cape Verde', 'CV', 'CPV', '132', 'CVE', 'Escudo', null, 'CV.png', '238');
INSERT INTO `countries` VALUES ('41', 'Cayman Islands', 'KY', 'CYM', '136', 'KYD', 'Dollar', '$', 'KY.png', null);
INSERT INTO `countries` VALUES ('42', 'Central African Republic', 'CF', 'CAF', '140', 'XAF', 'Franc', 'FCF', 'CF.png', '236');
INSERT INTO `countries` VALUES ('43', 'Chad', 'TD', 'TCD', '148', 'XAF', 'Franc', null, 'TD.png', '235');
INSERT INTO `countries` VALUES ('44', 'Chile', 'CL', 'CHL', '152', 'CLP', 'Peso', null, 'CL.png', '56');
INSERT INTO `countries` VALUES ('45', 'China', 'CN', 'CHN', '156', 'CNY', 'Yuan Renminbi', '¥', 'CN.png', '86');
INSERT INTO `countries` VALUES ('46', 'Christmas Island', 'CX', 'CXR', '162', 'AUD', 'Dollar', '$', 'CX.png', null);
INSERT INTO `countries` VALUES ('47', 'Cocos Islands', 'CC', 'CCK', '166', 'AUD', 'Dollar', '$', 'CC.png', null);
INSERT INTO `countries` VALUES ('48', 'Colombia', 'CO', 'COL', '170', 'COP', 'Peso', '$', 'CO.png', '57');
INSERT INTO `countries` VALUES ('49', 'Comoros', 'KM', 'COM', '174', 'KMF', 'Franc', null, 'KM.png', '269');
INSERT INTO `countries` VALUES ('50', 'Cook Islands', 'CK', 'COK', '184', 'NZD', 'Dollar', '$', 'CK.png', null);
INSERT INTO `countries` VALUES ('51', 'Costa Rica', 'CR', 'CRI', '188', 'CRC', 'Colon', '₡', 'CR.png', '506');
INSERT INTO `countries` VALUES ('52', 'Croatia', 'HR', 'HRV', '191', 'HRK', 'Kuna', 'kn', 'HR.png', '385');
INSERT INTO `countries` VALUES ('53', 'Cuba', 'CU', 'CUB', '192', 'CUP', 'Peso', '₱', 'CU.png', null);
INSERT INTO `countries` VALUES ('54', 'Cyprus', 'CY', 'CYP', '196', 'CYP', 'Pound', null, 'CY.png', null);
INSERT INTO `countries` VALUES ('55', 'Czech Republic', 'CZ', 'CZE', '203', 'CZK', 'Koruna', 'Kč', 'CZ.png', '420');
INSERT INTO `countries` VALUES ('56', 'Democratic Republic of the Congo', 'CD', 'COD', '180', 'CDF', 'Franc', null, 'CD.png', null);
INSERT INTO `countries` VALUES ('57', 'Denmark', 'DK', 'DNK', '208', 'DKK', 'Krone', 'kr', 'DK.png', '45');
INSERT INTO `countries` VALUES ('58', 'Djibouti', 'DJ', 'DJI', '262', 'DJF', 'Franc', null, 'DJ.png', '253');
INSERT INTO `countries` VALUES ('59', 'Dominica', 'DM', 'DMA', '212', 'XCD', 'Dollar', '$', 'DM.png', '1767');
INSERT INTO `countries` VALUES ('60', 'Dominican Republic', 'DO', 'DOM', '214', 'DOP', 'Peso', 'RD$', 'DO.png', '1809');
INSERT INTO `countries` VALUES ('61', 'East Timor', 'TL', 'TLS', '626', 'USD', 'Dollar', '$', 'TL.png', null);
INSERT INTO `countries` VALUES ('62', 'Ecuador', 'EC', 'ECU', '218', 'USD', 'Dollar', '$', 'EC.png', '593');
INSERT INTO `countries` VALUES ('63', 'Egypt', 'EG', 'EGY', '818', 'EGP', 'Pound', '£', 'EG.png', '20');
INSERT INTO `countries` VALUES ('64', 'El Salvador', 'SV', 'SLV', '222', 'SVC', 'Colone', '$', 'SV.png', '503');
INSERT INTO `countries` VALUES ('65', 'Equatorial Guinea', 'GQ', 'GNQ', '226', 'XAF', 'Franc', 'FCF', 'GQ.png', '240');
INSERT INTO `countries` VALUES ('66', 'Eritrea', 'ER', 'ERI', '232', 'ERN', 'Nakfa', 'Nfk', 'ER.png', '291');
INSERT INTO `countries` VALUES ('67', 'Estonia', 'EE', 'EST', '233', 'EEK', 'Kroon', 'kr', 'EE.png', '372');
INSERT INTO `countries` VALUES ('68', 'Ethiopia', 'ET', 'ETH', '231', 'ETB', 'Birr', null, 'ET.png', '251');
INSERT INTO `countries` VALUES ('69', 'Falkland Islands', 'FK', 'FLK', '238', 'FKP', 'Pound', '£', 'FK.png', null);
INSERT INTO `countries` VALUES ('70', 'Faroe Islands', 'FO', 'FRO', '234', 'DKK', 'Krone', 'kr', 'FO.png', null);
INSERT INTO `countries` VALUES ('71', 'Fiji', 'FJ', 'FJI', '242', 'FJD', 'Dollar', '$', 'FJ.png', '679');
INSERT INTO `countries` VALUES ('72', 'Finland', 'FI', 'FIN', '246', 'EUR', 'Euro', '€', 'FI.png', '358');
INSERT INTO `countries` VALUES ('73', 'France', 'FR', 'FRA', '250', 'EUR', 'Euro', '€', 'FR.png', '33');
INSERT INTO `countries` VALUES ('74', 'French Guiana', 'GF', 'GUF', '254', 'EUR', 'Euro', '€', 'GF.png', null);
INSERT INTO `countries` VALUES ('75', 'French Polynesia', 'PF', 'PYF', '258', 'XPF', 'Franc', null, 'PF.png', null);
INSERT INTO `countries` VALUES ('76', 'French Southern Territories', 'TF', 'ATF', '260', 'EUR', 'Euro  ', '€', 'TF.png', null);
INSERT INTO `countries` VALUES ('77', 'Gabon', 'GA', 'GAB', '266', 'XAF', 'Franc', 'FCF', 'GA.png', '241');
INSERT INTO `countries` VALUES ('78', 'Gambia', 'GM', 'GMB', '270', 'GMD', 'Dalasi', 'D', 'GM.png', '220');
INSERT INTO `countries` VALUES ('79', 'Georgia', 'GE', 'GEO', '268', 'GEL', 'Lari', null, 'GE.png', '995');
INSERT INTO `countries` VALUES ('80', 'Germany', 'DE', 'DEU', '276', 'EUR', 'Euro', '€', 'DE.png', '49');
INSERT INTO `countries` VALUES ('81', 'Ghana', 'GH', 'GHA', '288', 'GHC', 'Cedi', '¢', 'GH.png', '233');
INSERT INTO `countries` VALUES ('82', 'Gibraltar', 'GI', 'GIB', '292', 'GIP', 'Pound', '£', 'GI.png', null);
INSERT INTO `countries` VALUES ('83', 'Greece', 'GR', 'GRC', '300', 'EUR', 'Euro', '€', 'GR.png', '30');
INSERT INTO `countries` VALUES ('84', 'Greenland', 'GL', 'GRL', '304', 'DKK', 'Krone', 'kr', 'GL.png', null);
INSERT INTO `countries` VALUES ('85', 'Grenada', 'GD', 'GRD', '308', 'XCD', 'Dollar', '$', 'GD.png', '1473');
INSERT INTO `countries` VALUES ('86', 'Guadeloupe', 'GP', 'GLP', '312', 'EUR', 'Euro', '€', 'GP.png', null);
INSERT INTO `countries` VALUES ('87', 'Guam', 'GU', 'GUM', '316', 'USD', 'Dollar', '$', 'GU.png', null);
INSERT INTO `countries` VALUES ('88', 'Guatemala', 'GT', 'GTM', '320', 'GTQ', 'Quetzal', 'Q', 'GT.png', '502');
INSERT INTO `countries` VALUES ('89', 'Guinea', 'GN', 'GIN', '324', 'GNF', 'Franc', null, 'GN.png', '224');
INSERT INTO `countries` VALUES ('90', 'Guinea-Bissau', 'GW', 'GNB', '624', 'XOF', 'Franc', null, 'GW.png', '245');
INSERT INTO `countries` VALUES ('91', 'Guyana', 'GY', 'GUY', '328', 'GYD', 'Dollar', '$', 'GY.png', '592');
INSERT INTO `countries` VALUES ('92', 'Haiti', 'HT', 'HTI', '332', 'HTG', 'Gourde', 'G', 'HT.png', '509');
INSERT INTO `countries` VALUES ('93', 'Heard Island and McDonald Islands', 'HM', 'HMD', '334', 'AUD', 'Dollar', '$', 'HM.png', null);
INSERT INTO `countries` VALUES ('94', 'Honduras', 'HN', 'HND', '340', 'HNL', 'Lempira', 'L', 'HN.png', '504');
INSERT INTO `countries` VALUES ('95', 'Hong Kong', 'HK', 'HKG', '344', 'HKD', 'Dollar', '$', 'HK.png', null);
INSERT INTO `countries` VALUES ('96', 'Hungary', 'HU', 'HUN', '348', 'HUF', 'Forint', 'Ft', 'HU.png', '36');
INSERT INTO `countries` VALUES ('97', 'Iceland', 'IS', 'ISL', '352', 'ISK', 'Krona', 'kr', 'IS.png', '354');
INSERT INTO `countries` VALUES ('98', 'India', 'IN', 'IND', '356', 'INR', 'Rupee', '₹', 'IN.png', '91');
INSERT INTO `countries` VALUES ('99', 'Indonesia', 'ID', 'IDN', '360', 'IDR', 'Rupiah', 'Rp', 'ID.png', '62');
INSERT INTO `countries` VALUES ('100', 'Iran', 'IR', 'IRN', '364', 'IRR', 'Rial', '﷼', 'IR.png', '98');
INSERT INTO `countries` VALUES ('101', 'Iraq', 'IQ', 'IRQ', '368', 'IQD', 'Dinar', null, 'IQ.png', '964');
INSERT INTO `countries` VALUES ('102', 'Ireland', 'IE', 'IRL', '372', 'EUR', 'Euro', '€', 'IE.png', null);
INSERT INTO `countries` VALUES ('103', 'Israel', 'IL', 'ISR', '376', 'ILS', 'Shekel', '₪', 'IL.png', '972');
INSERT INTO `countries` VALUES ('104', 'Italy', 'IT', 'ITA', '380', 'EUR', 'Euro', '€', 'IT.png', '39');
INSERT INTO `countries` VALUES ('105', 'Ivory Coast', 'CI', 'CIV', '384', 'XOF', 'Franc', null, 'CI.png', null);
INSERT INTO `countries` VALUES ('106', 'Jamaica', 'JM', 'JAM', '388', 'JMD', 'Dollar', '$', 'JM.png', '1876');
INSERT INTO `countries` VALUES ('107', 'Japan', 'JP', 'JPN', '392', 'JPY', 'Yen', '¥', 'JP.png', '81');
INSERT INTO `countries` VALUES ('108', 'Jordan', 'JO', 'JOR', '400', 'JOD', 'Dinar', null, 'JO.png', '962');
INSERT INTO `countries` VALUES ('109', 'Kazakhstan', 'KZ', 'KAZ', '398', 'KZT', 'Tenge', 'лв', 'KZ.png', '7');
INSERT INTO `countries` VALUES ('110', 'Kenya', 'KE', 'KEN', '404', 'KES', 'Shilling', null, 'KE.png', '254');
INSERT INTO `countries` VALUES ('111', 'Kiribati', 'KI', 'KIR', '296', 'AUD', 'Dollar', '$', 'KI.png', '686');
INSERT INTO `countries` VALUES ('112', 'Kuwait', 'KW', 'KWT', '414', 'KWD', 'Dinar', null, 'KW.png', '965');
INSERT INTO `countries` VALUES ('113', 'Kyrgyzstan', 'KG', 'KGZ', '417', 'KGS', 'Som', 'лв', 'KG.png', null);
INSERT INTO `countries` VALUES ('114', 'Laos', 'LA', 'LAO', '418', 'LAK', 'Kip', '₭', 'LA.png', null);
INSERT INTO `countries` VALUES ('115', 'Latvia', 'LV', 'LVA', '428', 'LVL', 'Lat', 'Ls', 'LV.png', '371');
INSERT INTO `countries` VALUES ('116', 'Lebanon', 'LB', 'LBN', '422', 'LBP', 'Pound', '£', 'LB.png', '961');
INSERT INTO `countries` VALUES ('117', 'Lesotho', 'LS', 'LSO', '426', 'LSL', 'Loti', 'L', 'LS.png', '266');
INSERT INTO `countries` VALUES ('118', 'Liberia', 'LR', 'LBR', '430', 'LRD', 'Dollar', '$', 'LR.png', '231');
INSERT INTO `countries` VALUES ('119', 'Libya', 'LY', 'LBY', '434', 'LYD', 'Dinar', null, 'LY.png', '218');
INSERT INTO `countries` VALUES ('120', 'Liechtenstein', 'LI', 'LIE', '438', 'CHF', 'Franc', 'CHF', 'LI.png', null);
INSERT INTO `countries` VALUES ('121', 'Lithuania', 'LT', 'LTU', '440', 'LTL', 'Litas', 'Lt', 'LT.png', '370');
INSERT INTO `countries` VALUES ('122', 'Luxembourg', 'LU', 'LUX', '442', 'EUR', 'Euro', '€', 'LU.png', '352');
INSERT INTO `countries` VALUES ('123', 'Macao', 'MO', 'MAC', '446', 'MOP', 'Pataca', 'MOP', 'MO.png', null);
INSERT INTO `countries` VALUES ('124', 'Macedonia', 'MK', 'MKD', '807', 'MKD', 'Denar', 'ден', 'MK.png', null);
INSERT INTO `countries` VALUES ('125', 'Madagascar', 'MG', 'MDG', '450', 'MGA', 'Ariary', null, 'MG.png', '261');
INSERT INTO `countries` VALUES ('126', 'Malawi', 'MW', 'MWI', '454', 'MWK', 'Kwacha', 'MK', 'MW.png', '265');
INSERT INTO `countries` VALUES ('127', 'Malaysia', 'MY', 'MYS', '458', 'MYR', 'Ringgit', 'RM', 'MY.png', '60');
INSERT INTO `countries` VALUES ('128', 'Maldives', 'MV', 'MDV', '462', 'MVR', 'Rufiyaa', 'Rf', 'MV.png', '960');
INSERT INTO `countries` VALUES ('129', 'Mali', 'ML', 'MLI', '466', 'XOF', 'Franc', null, 'ML.png', '223');
INSERT INTO `countries` VALUES ('130', 'Malta', 'MT', 'MLT', '470', 'MTL', 'Lira', null, 'MT.png', null);
INSERT INTO `countries` VALUES ('131', 'Marshall Islands', 'MH', 'MHL', '584', 'USD', 'Dollar', '$', 'MH.png', '692');
INSERT INTO `countries` VALUES ('132', 'Martinique', 'MQ', 'MTQ', '474', 'EUR', 'Euro', '€', 'MQ.png', null);
INSERT INTO `countries` VALUES ('133', 'Mauritania', 'MR', 'MRT', '478', 'MRO', 'Ouguiya', 'UM', 'MR.png', '222');
INSERT INTO `countries` VALUES ('134', 'Mauritius', 'MU', 'MUS', '480', 'MUR', 'Rupee', '₨', 'MU.png', '230');
INSERT INTO `countries` VALUES ('135', 'Mayotte', 'YT', 'MYT', '175', 'EUR', 'Euro', '€', 'YT.png', null);
INSERT INTO `countries` VALUES ('136', 'Mexico', 'MX', 'MEX', '484', 'MXN', 'Peso', '$', 'MX.png', '52');
INSERT INTO `countries` VALUES ('137', 'Micronesia', 'FM', 'FSM', '583', 'USD', 'Dollar', '$', 'FM.png', null);
INSERT INTO `countries` VALUES ('138', 'Moldova', 'MD', 'MDA', '498', 'MDL', 'Leu', null, 'MD.png', '373');
INSERT INTO `countries` VALUES ('139', 'Monaco', 'MC', 'MCO', '492', 'EUR', 'Euro', '€', 'MC.png', null);
INSERT INTO `countries` VALUES ('140', 'Mongolia', 'MN', 'MNG', '496', 'MNT', 'Tugrik', '₮', 'MN.png', '976');
INSERT INTO `countries` VALUES ('141', 'Montserrat', 'MS', 'MSR', '500', 'XCD', 'Dollar', '$', 'MS.png', null);
INSERT INTO `countries` VALUES ('142', 'Morocco', 'MA', 'MAR', '504', 'MAD', 'Dirham', null, 'MA.png', '212');
INSERT INTO `countries` VALUES ('143', 'Mozambique', 'MZ', 'MOZ', '508', 'MZN', 'Meticail', 'MT', 'MZ.png', '258');
INSERT INTO `countries` VALUES ('144', 'Myanmar', 'MM', 'MMR', '104', 'MMK', 'Kyat', 'K', 'MM.png', '0');
INSERT INTO `countries` VALUES ('145', 'Namibia', 'NA', 'NAM', '516', 'NAD', 'Dollar', '$', 'NA.png', '264');
INSERT INTO `countries` VALUES ('146', 'Nauru', 'NR', 'NRU', '520', 'AUD', 'Dollar', '$', 'NR.png', null);
INSERT INTO `countries` VALUES ('147', 'Nepal', 'NP', 'NPL', '524', 'NPR', 'Rupee', '₨', 'NP.png', '977');
INSERT INTO `countries` VALUES ('148', 'Netherlands', 'NL', 'NLD', '528', 'EUR', 'Euro', '€', 'NL.png', '31');
INSERT INTO `countries` VALUES ('149', 'Netherlands Antilles', 'AN', 'ANT', '530', 'ANG', 'Guilder', 'ƒ', 'AN.png', null);
INSERT INTO `countries` VALUES ('150', 'New Caledonia', 'NC', 'NCL', '540', 'XPF', 'Franc', null, 'NC.png', null);
INSERT INTO `countries` VALUES ('151', 'New Zealand', 'NZ', 'NZL', '554', 'NZD', 'Dollar', '$', 'NZ.png', null);
INSERT INTO `countries` VALUES ('152', 'Nicaragua', 'NI', 'NIC', '558', 'NIO', 'Cordoba', 'C$', 'NI.png', '505');
INSERT INTO `countries` VALUES ('153', 'Niger', 'NE', 'NER', '562', 'XOF', 'Franc', null, 'NE.png', '227');
INSERT INTO `countries` VALUES ('154', 'Nigeria', 'NG', 'NGA', '566', 'NGN', 'Naira', '₦', 'NG.png', '234');
INSERT INTO `countries` VALUES ('155', 'Niue', 'NU', 'NIU', '570', 'NZD', 'Dollar', '$', 'NU.png', null);
INSERT INTO `countries` VALUES ('156', 'Norfolk Island', 'NF', 'NFK', '574', 'AUD', 'Dollar', '$', 'NF.png', null);
INSERT INTO `countries` VALUES ('157', 'North Korea', 'KP', 'PRK', '408', 'KPW', 'Won', '₩', 'KP.png', null);
INSERT INTO `countries` VALUES ('158', 'Northern Mariana Islands', 'MP', 'MNP', '580', 'USD', 'Dollar', '$', 'MP.png', null);
INSERT INTO `countries` VALUES ('159', 'Norway', 'NO', 'NOR', '578', 'NOK', 'Krone', 'kr', 'NO.png', '47');
INSERT INTO `countries` VALUES ('160', 'Oman', 'OM', 'OMN', '512', 'OMR', 'Rial', '﷼', 'OM.png', '968');
INSERT INTO `countries` VALUES ('161', 'Pakistan', 'PK', 'PAK', '586', 'PKR', 'Rupee', '₨', 'PK.png', '92');
INSERT INTO `countries` VALUES ('162', 'Palau', 'PW', 'PLW', '585', 'USD', 'Dollar', '$', 'PW.png', '680');
INSERT INTO `countries` VALUES ('163', 'Palestinian Territory', 'PS', 'PSE', '275', 'ILS', 'Shekel', '₪', 'PS.png', null);
INSERT INTO `countries` VALUES ('164', 'Panama', 'PA', 'PAN', '591', 'PAB', 'Balboa', 'B/.', 'PA.png', '507');
INSERT INTO `countries` VALUES ('165', 'Papua New Guinea', 'PG', 'PNG', '598', 'PGK', 'Kina', null, 'PG.png', '675');
INSERT INTO `countries` VALUES ('166', 'Paraguay', 'PY', 'PRY', '600', 'PYG', 'Guarani', 'Gs', 'PY.png', '595');
INSERT INTO `countries` VALUES ('167', 'Peru', 'PE', 'PER', '604', 'PEN', 'Sol', 'S/.', 'PE.png', '51');
INSERT INTO `countries` VALUES ('168', 'Philippines', 'PH', 'PHL', '608', 'PHP', 'Peso', 'Php', 'PH.png', '63');
INSERT INTO `countries` VALUES ('169', 'Pitcairn', 'PN', 'PCN', '612', 'NZD', 'Dollar', '$', 'PN.png', null);
INSERT INTO `countries` VALUES ('170', 'Poland', 'PL', 'POL', '616', 'PLN', 'Zloty', 'zł', 'PL.png', '48');
INSERT INTO `countries` VALUES ('171', 'Portugal', 'PT', 'PRT', '620', 'EUR', 'Euro', '€', 'PT.png', '351');
INSERT INTO `countries` VALUES ('172', 'Puerto Rico', 'PR', 'PRI', '630', 'USD', 'Dollar', '$', 'PR.png', null);
INSERT INTO `countries` VALUES ('173', 'Qatar', 'QA', 'QAT', '634', 'QAR', 'Rial', '﷼', 'QA.png', '974');
INSERT INTO `countries` VALUES ('174', 'Republic of the Congo', 'CG', 'COG', '178', 'XAF', 'Franc', 'FCF', 'CG.png', null);
INSERT INTO `countries` VALUES ('175', 'Reunion', 'RE', 'REU', '638', 'EUR', 'Euro', '€', 'RE.png', null);
INSERT INTO `countries` VALUES ('176', 'Romania', 'RO', 'ROU', '642', 'RON', 'Leu', 'lei', 'RO.png', '40');
INSERT INTO `countries` VALUES ('177', 'Russia', 'RU', 'RUS', '643', 'RUB', 'Ruble', 'руб', 'RU.png', '7');
INSERT INTO `countries` VALUES ('178', 'Rwanda', 'RW', 'RWA', '646', 'RWF', 'Franc', null, 'RW.png', '250');
INSERT INTO `countries` VALUES ('179', 'Saint Helena', 'SH', 'SHN', '654', 'SHP', 'Pound', '£', 'SH.png', null);
INSERT INTO `countries` VALUES ('180', 'Saint Kitts and Nevis', 'KN', 'KNA', '659', 'XCD', 'Dollar', '$', 'KN.png', null);
INSERT INTO `countries` VALUES ('181', 'Saint Lucia', 'LC', 'LCA', '662', 'XCD', 'Dollar', '$', 'LC.png', null);
INSERT INTO `countries` VALUES ('182', 'Saint Pierre and Miquelon', 'PM', 'SPM', '666', 'EUR', 'Euro', '€', 'PM.png', null);
INSERT INTO `countries` VALUES ('183', 'Saint Vincent and the Grenadines', 'VC', 'VCT', '670', 'XCD', 'Dollar', '$', 'VC.png', null);
INSERT INTO `countries` VALUES ('184', 'Samoa', 'WS', 'WSM', '882', 'WST', 'Tala', 'WS$', 'WS.png', '685');
INSERT INTO `countries` VALUES ('185', 'San Marino', 'SM', 'SMR', '674', 'EUR', 'Euro', '€', 'SM.png', null);
INSERT INTO `countries` VALUES ('186', 'Sao Tome and Principe', 'ST', 'STP', '678', 'STD', 'Dobra', 'Db', 'ST.png', '239');
INSERT INTO `countries` VALUES ('187', 'Saudi Arabia', 'SA', 'SAU', '682', 'SAR', 'Rial', '﷼', 'SA.png', null);
INSERT INTO `countries` VALUES ('188', 'Senegal', 'SN', 'SEN', '686', 'XOF', 'Franc', null, 'SN.png', '221');
INSERT INTO `countries` VALUES ('189', 'Serbia and Montenegro', 'CS', 'SCG', '891', 'RSD', 'Dinar', 'Дин', 'CS.png', null);
INSERT INTO `countries` VALUES ('190', 'Seychelles', 'SC', 'SYC', '690', 'SCR', 'Rupee', '₨', 'SC.png', '248');
INSERT INTO `countries` VALUES ('191', 'Sierra Leone', 'SL', 'SLE', '694', 'SLL', 'Leone', 'Le', 'SL.png', '232');
INSERT INTO `countries` VALUES ('192', 'Singapore', 'SG', 'SGP', '702', 'SGD', 'Dollar', '$', 'SG.png', '65');
INSERT INTO `countries` VALUES ('193', 'Slovakia', 'SK', 'SVK', '703', 'SKK', 'Koruna', 'Sk', 'SK.png', null);
INSERT INTO `countries` VALUES ('194', 'Slovenia', 'SI', 'SVN', '705', 'EUR', 'Euro', '€', 'SI.png', '386');
INSERT INTO `countries` VALUES ('195', 'Solomon Islands', 'SB', 'SLB', '90', 'SBD', 'Dollar', '$', 'SB.png', '677');
INSERT INTO `countries` VALUES ('196', 'Somalia', 'SO', 'SOM', '706', 'SOS', 'Shilling', 'S', 'SO.png', '252');
INSERT INTO `countries` VALUES ('197', 'South Africa', 'ZA', 'ZAF', '710', 'ZAR', 'Rand', 'R', 'ZA.png', '27');
INSERT INTO `countries` VALUES ('198', 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', '239', 'GBP', 'Pound', '£', 'GS.png', null);
INSERT INTO `countries` VALUES ('199', 'South Korea', 'KR', 'KOR', '410', 'KRW', 'Won', '₩', 'KR.png', null);
INSERT INTO `countries` VALUES ('200', 'Spain', 'ES', 'ESP', '724', 'EUR', 'Euro', '€', 'ES.png', '34');
INSERT INTO `countries` VALUES ('201', 'Sri Lanka', 'LK', 'LKA', '144', 'LKR', 'Rupee', '₨', 'LK.png', '94');
INSERT INTO `countries` VALUES ('202', 'Sudan', 'SD', 'SDN', '736', 'SDD', 'Dinar', null, 'SD.png', '249');
INSERT INTO `countries` VALUES ('203', 'Suriname', 'SR', 'SUR', '740', 'SRD', 'Dollar', '$', 'SR.png', '597');
INSERT INTO `countries` VALUES ('204', 'Svalbard and Jan Mayen', 'SJ', 'SJM', '744', 'NOK', 'Krone', 'kr', 'SJ.png', null);
INSERT INTO `countries` VALUES ('205', 'Swaziland', 'SZ', 'SWZ', '748', 'SZL', 'Lilangeni', null, 'SZ.png', '268');
INSERT INTO `countries` VALUES ('206', 'Sweden', 'SE', 'SWE', '752', 'SEK', 'Krona', 'kr', 'SE.png', '46');
INSERT INTO `countries` VALUES ('207', 'Switzerland', 'CH', 'CHE', '756', 'CHF', 'Franc', 'CHF', 'CH.png', '41');
INSERT INTO `countries` VALUES ('208', 'Syria', 'SY', 'SYR', '760', 'SYP', 'Pound', '£', 'SY.png', '963');
INSERT INTO `countries` VALUES ('209', 'Taiwan', 'TW', 'TWN', '158', 'TWD', 'Dollar', 'NT$', 'TW.png', null);
INSERT INTO `countries` VALUES ('210', 'Tajikistan', 'TJ', 'TJK', '762', 'TJS', 'Somoni', null, 'TJ.png', '992');
INSERT INTO `countries` VALUES ('211', 'Tanzania', 'TZ', 'TZA', '834', 'TZS', 'Shilling', null, 'TZ.png', '255');
INSERT INTO `countries` VALUES ('212', 'Thailand', 'TH', 'THA', '764', 'THB', 'Baht', '฿', 'TH.png', '66');
INSERT INTO `countries` VALUES ('213', 'Togo', 'TG', 'TGO', '768', 'XOF', 'Franc', null, 'TG.png', '228');
INSERT INTO `countries` VALUES ('214', 'Tokelau', 'TK', 'TKL', '772', 'NZD', 'Dollar', '$', 'TK.png', null);
INSERT INTO `countries` VALUES ('215', 'Tonga', 'TO', 'TON', '776', 'TOP', 'Pa\'anga', 'T$', 'TO.png', '676');
INSERT INTO `countries` VALUES ('216', 'Trinidad and Tobago', 'TT', 'TTO', '780', 'TTD', 'Dollar', 'TT$', 'TT.png', '1868');
INSERT INTO `countries` VALUES ('217', 'Tunisia', 'TN', 'TUN', '788', 'TND', 'Dinar', null, 'TN.png', '216');
INSERT INTO `countries` VALUES ('218', 'Turkey', 'TR', 'TUR', '792', 'TRY', 'Lira', 'YTL', 'TR.png', null);
INSERT INTO `countries` VALUES ('219', 'Turkmenistan', 'TM', 'TKM', '795', 'TMM', 'Manat', 'm', 'TM.png', '993');
INSERT INTO `countries` VALUES ('220', 'Turks and Caicos Islands', 'TC', 'TCA', '796', 'USD', 'Dollar', '$', 'TC.png', null);
INSERT INTO `countries` VALUES ('221', 'Tuvalu', 'TV', 'TUV', '798', 'AUD', 'Dollar', '$', 'TV.png', '688');
INSERT INTO `countries` VALUES ('222', 'U.S. Virgin Islands', 'VI', 'VIR', '850', 'USD', 'Dollar', '$', 'VI.png', null);
INSERT INTO `countries` VALUES ('223', 'Uganda', 'UG', 'UGA', '800', 'UGX', 'Shilling', null, 'UG.png', '256');
INSERT INTO `countries` VALUES ('224', 'Ukraine', 'UA', 'UKR', '804', 'UAH', 'Hryvnia', '₴', 'UA.png', '380');
INSERT INTO `countries` VALUES ('225', 'United Arab Emirates', 'AE', 'ARE', '784', 'AED', 'Dirham', null, 'AE.png', '971');
INSERT INTO `countries` VALUES ('226', 'United Kingdom', 'GB', 'GBR', '826', 'GBP', 'Pound', '£', 'GB.png', '44');
INSERT INTO `countries` VALUES ('227', 'United States', 'US', 'USA', '840', 'USD', 'Dollar', '$', 'US.png', null);
INSERT INTO `countries` VALUES ('228', 'United States Minor Outlying Islands', 'UM', 'UMI', '581', 'USD', 'Dollar ', '$', 'UM.png', null);
INSERT INTO `countries` VALUES ('229', 'Uruguay', 'UY', 'URY', '858', 'UYU', 'Peso', '$U', 'UY.png', '598');
INSERT INTO `countries` VALUES ('230', 'Uzbekistan', 'UZ', 'UZB', '860', 'UZS', 'Som', 'лв', 'UZ.png', '998');
INSERT INTO `countries` VALUES ('231', 'Vanuatu', 'VU', 'VUT', '548', 'VUV', 'Vatu', 'Vt', 'VU.png', '678');
INSERT INTO `countries` VALUES ('232', 'Vatican', 'VA', 'VAT', '336', 'EUR', 'Euro', '€', 'VA.png', null);
INSERT INTO `countries` VALUES ('233', 'Venezuela', 'VE', 'VEN', '862', 'VEF', 'Bolivar', 'Bs', 'VE.png', '58');
INSERT INTO `countries` VALUES ('234', 'Vietnam', 'VN', 'VNM', '704', 'VND', 'Dong', '₫', 'VN.png', '84');
INSERT INTO `countries` VALUES ('235', 'Wallis and Futuna', 'WF', 'WLF', '876', 'XPF', 'Franc', null, 'WF.png', null);
INSERT INTO `countries` VALUES ('236', 'Western Sahara', 'EH', 'ESH', '732', 'MAD', 'Dirham', null, 'EH.png', null);
INSERT INTO `countries` VALUES ('237', 'Yemen', 'YE', 'YEM', '887', 'YER', 'Rial', '﷼', 'YE.png', '967');
INSERT INTO `countries` VALUES ('238', 'Zambia', 'ZM', 'ZMB', '894', 'ZMK', 'Kwacha', 'ZK', 'ZM.png', '260');
INSERT INTO `countries` VALUES ('239', 'Zimbabwe', 'ZW', 'ZWE', '716', 'ZWD', 'Dollar', 'Z$', 'ZW.png', '263');

-- ----------------------------
-- Table structure for `current_session_term`
-- ----------------------------
DROP TABLE IF EXISTS `current_session_term`;
CREATE TABLE `current_session_term` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `session_id` int(10) unsigned DEFAULT NULL,
  `term_id` int(10) unsigned DEFAULT NULL,
  `school_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of current_session_term
-- ----------------------------
INSERT INTO `current_session_term` VALUES ('1', '1', '2', '1');

-- ----------------------------
-- Table structure for `dvbc__schema_version`
-- ----------------------------
DROP TABLE IF EXISTS `dvbc__schema_version`;
CREATE TABLE `dvbc__schema_version` (
  `version_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `checksum` varchar(42) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `ed_admins`
-- ----------------------------
DROP TABLE IF EXISTS `ed_admins`;
CREATE TABLE `ed_admins` (
  `admin_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `last_login_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role_id` int(255) DEFAULT '0',
  PRIMARY KEY (`admin_id`,`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ed_admins
-- ----------------------------
INSERT INTO `ed_admins` VALUES ('1', 'admin1@aol.com', '96c98f1469295420ce4661bca997c5e97e66f448', '1', 'Super', 'Admin', '2015-10-03 08:01:13', '1');

-- ----------------------------
-- Table structure for `ed_menu`
-- ----------------------------
DROP TABLE IF EXISTS `ed_menu`;
CREATE TABLE `ed_menu` (
  `menu_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `sort_id` int(255) NOT NULL DEFAULT '0',
  `id_string` varchar(255) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ed_menu
-- ----------------------------
INSERT INTO `ed_menu` VALUES ('1', 'Access Control', '0', 'access_control');
INSERT INTO `ed_menu` VALUES ('2', 'Schools', '0', 'school');

-- ----------------------------
-- Table structure for `ed_roles`
-- ----------------------------
DROP TABLE IF EXISTS `ed_roles`;
CREATE TABLE `ed_roles` (
  `role_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `auth_level` smallint(6) DEFAULT '10',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ed_roles
-- ----------------------------
INSERT INTO `ed_roles` VALUES ('1', 'Super Admin', '1000');
INSERT INTO `ed_roles` VALUES ('2', 'Admin', '900');
INSERT INTO `ed_roles` VALUES ('3', 'Customer Support', '800');
INSERT INTO `ed_roles` VALUES ('4', 'Developer', '800');
INSERT INTO `ed_roles` VALUES ('5', 'Consultant', '10');
INSERT INTO `ed_roles` VALUES ('6', 'HR Associate', '10');

-- ----------------------------
-- Table structure for `ed_role_perms`
-- ----------------------------
DROP TABLE IF EXISTS `ed_role_perms`;
CREATE TABLE `ed_role_perms` (
  `perm_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `menu_id` int(255) unsigned NOT NULL,
  `in_menu` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`perm_id`,`id_string`),
  KEY `id_menu` (`menu_id`,`in_menu`),
  KEY `id_menu_2` (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ed_role_perms
-- ----------------------------
INSERT INTO `ed_role_perms` VALUES ('1', '1', 'Add School', 'add_school', '2', '1');
INSERT INTO `ed_role_perms` VALUES ('2', '1', 'View Access Control', 'view_access_control', '1', '1');
INSERT INTO `ed_role_perms` VALUES ('3', '1', 'List Schools', 'list_schools', '2', '1');

-- ----------------------------
-- Table structure for `ed_user_logs`
-- ----------------------------
DROP TABLE IF EXISTS `ed_user_logs`;
CREATE TABLE `ed_user_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` int(255) unsigned NOT NULL,
  `message` text NOT NULL,
  `type` int(1) unsigned NOT NULL DEFAULT '1',
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_address` bigint(20) DEFAULT NULL,
  `school_id` int(10) unsigned NOT NULL,
  `user_agent` text,
  `session_id` text,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `url` text,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `token` text,
  `proxy_user_string` varchar(255) DEFAULT NULL,
  `proxy_company_string` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for `emergency_contact`
-- ----------------------------
DROP TABLE IF EXISTS `emergency_contact`;
CREATE TABLE `emergency_contact` (
  `emergency_contact_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `employer` varchar(45) DEFAULT NULL,
  `work_address` varchar(200) DEFAULT NULL,
  `relationship_id` int(10) DEFAULT NULL,
  `first_name2` varchar(45) DEFAULT NULL,
  `last_name2` varchar(45) DEFAULT NULL,
  `addresss2` varchar(200) DEFAULT NULL,
  `email2` varchar(45) DEFAULT NULL,
  `phone2` varchar(45) DEFAULT NULL,
  `employer2` varchar(150) DEFAULT NULL,
  `work_address2` varchar(200) DEFAULT NULL,
  `relationship_id2` int(10) unsigned DEFAULT NULL,
  `student_id` int(10) DEFAULT NULL,
  `school_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`emergency_contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for `employees`
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `employee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `staff_id` varchar(10) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `gender_id` int(10) unsigned DEFAULT NULL,
  `marital_status_id` int(10) unsigned DEFAULT NULL,
  `nationality_id` int(10) unsigned DEFAULT NULL,
  `school_id` int(10) unsigned DEFAULT NULL,
  `profile_pic_name` varchar(150) DEFAULT NULL,
  `profile_pic_uri` varchar(150) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `dateof_birth` date DEFAULT NULL,
  `employment_date` date DEFAULT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of employees
-- ----------------------------
INSERT INTO `employees` VALUES ('1', 'femi', 'brown', 'fem001@yahoo.com', 'ABC00001', '1', null, null, null, null, null, null, null, '1', null, null, '4', null, null, null);

-- ----------------------------
-- Table structure for `employee_phone`
-- ----------------------------
DROP TABLE IF EXISTS `employee_phone`;
CREATE TABLE `employee_phone` (
  `employee_phone_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(255) unsigned NOT NULL,
  `phone_type_id` int(255) unsigned NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `primary` int(255) unsigned DEFAULT '0',
  `country_code` int(255) unsigned DEFAULT NULL,
  `school_id` int(255) unsigned DEFAULT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  `verified` tinyint(1) DEFAULT '0',
  `verify_request_id` varchar(64) DEFAULT NULL,
  `verify_attempts` smallint(6) DEFAULT '0',
  PRIMARY KEY (`employee_phone_id`),
  KEY `verify_request_id` (`verify_request_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of employee_phone
-- ----------------------------

-- ----------------------------
-- Table structure for `gender`
-- ----------------------------
DROP TABLE IF EXISTS `gender`;
CREATE TABLE `gender` (
  `gender_id` int(25) NOT NULL AUTO_INCREMENT,
  `gender` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gender
-- ----------------------------
INSERT INTO `gender` VALUES ('1', 'Male');
INSERT INTO `gender` VALUES ('2', 'Female');

-- ----------------------------
-- Table structure for `guardian`
-- ----------------------------
DROP TABLE IF EXISTS `guardian`;
CREATE TABLE `guardian` (
  `guardian_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `relationship_id` int(10) unsigned DEFAULT NULL,
  `dateof_birth` varchar(45) DEFAULT NULL,
  `education` varchar(45) DEFAULT NULL,
  `occupation_id` int(10) unsigned DEFAULT NULL,
  `income` int(10) unsigned DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `mobile_phone` varchar(45) DEFAULT NULL,
  `office_address` varchar(150) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `student_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`guardian_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `languages`
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of languages
-- ----------------------------

-- ----------------------------
-- Table structure for `marital_status`
-- ----------------------------
DROP TABLE IF EXISTS `marital_status`;
CREATE TABLE `marital_status` (
  `marital_status_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `marital_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`marital_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of marital_status
-- ----------------------------
INSERT INTO `marital_status` VALUES ('1', 'Single');
INSERT INTO `marital_status` VALUES ('2', 'Married');
INSERT INTO `marital_status` VALUES ('3', 'Separated');
INSERT INTO `marital_status` VALUES ('4', 'Divorced');

-- ----------------------------
-- Table structure for `modules`
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `module_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(150) DEFAULT NULL,
  `id_string` varchar(150) DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `requires_login` int(10) unsigned DEFAULT NULL,
  `menu_order` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO `modules` VALUES ('1', 'Staff Records', 'employee', '1', '1', '100');
INSERT INTO `modules` VALUES ('2', 'Student', 'student', '1', '1', '200');
INSERT INTO `modules` VALUES ('3', 'Results', 'result', '1', '1', '300');
INSERT INTO `modules` VALUES ('4', 'Communication', 'communication', '1', '1', '400');
INSERT INTO `modules` VALUES ('5', 'Payroll', 'payroll', '1', '1', '500');
INSERT INTO `modules` VALUES ('6', 'library', 'library', '1', '1', '600');
INSERT INTO `modules` VALUES ('7', 'Alumni', 'alumni', '1', '1', '700');
INSERT INTO `modules` VALUES ('8', 'Fees', 'fees', '1', '1', '800');
INSERT INTO `modules` VALUES ('9', 'Time Table', 'time_table', '1', '1', '900');
INSERT INTO `modules` VALUES ('10', 'Settings', 'settings', '1', '1', '990');

-- ----------------------------
-- Table structure for `module_perms`
-- ----------------------------
DROP TABLE IF EXISTS `module_perms`;
CREATE TABLE `module_perms` (
  `perm_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  `menu_order` smallint(6) DEFAULT '100',
  PRIMARY KEY (`perm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of module_perms
-- ----------------------------
INSERT INTO `module_perms` VALUES ('1', '1', 'Staff Directory', 'staff_directory', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('2', '1', 'Add Staff', 'add_staff', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('3', '10', 'Session', 'session', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('4', '10', 'Term', 'term', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('5', '10', 'Add Session', 'add_session', '0', '1', '100');
INSERT INTO `module_perms` VALUES ('6', '10', 'Add Term', 'add_term', '0', '1', '100');
INSERT INTO `module_perms` VALUES ('7', '10', 'Current Session/Term', 'current_session_term', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('8', '2', 'Add Student', 'add_student', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('9', '2', 'Student Directory', 'view_student', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('10', '10', 'Classes', 'class', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('11', '4', 'Messages', 'messages', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('12', '3', 'Continous Accessments', 'ca', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('13', '3', 'Exam', 'exam', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('14', '10', 'Subjects', 'subject', '1', '1', '100');
INSERT INTO `module_perms` VALUES ('15', '10', 'Academic Calendar', 'calendar', '1', '1', '100');

-- ----------------------------
-- Table structure for `phone_types`
-- ----------------------------
DROP TABLE IF EXISTS `phone_types`;
CREATE TABLE `phone_types` (
  `phone_type_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `phone_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`phone_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of phone_types
-- ----------------------------
INSERT INTO `phone_types` VALUES ('1', 'Work');
INSERT INTO `phone_types` VALUES ('2', 'Home');
INSERT INTO `phone_types` VALUES ('3', 'Personal');

-- ----------------------------
-- Table structure for `relationship`
-- ----------------------------
DROP TABLE IF EXISTS `relationship`;
CREATE TABLE `relationship` (
  `relationship_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `relationship` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`relationship_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of relationship
-- ----------------------------
INSERT INTO `relationship` VALUES ('1', 'MOTHER');
INSERT INTO `relationship` VALUES ('2', 'SON');
INSERT INTO `relationship` VALUES ('3', 'SPOUSE');
INSERT INTO `relationship` VALUES ('4', 'TWIN SISTER');
INSERT INTO `relationship` VALUES ('5', 'TEACHER');
INSERT INTO `relationship` VALUES ('6', 'SIBLING');
INSERT INTO `relationship` VALUES ('7', 'BROTHER');
INSERT INTO `relationship` VALUES ('8', 'HUSBAND');
INSERT INTO `relationship` VALUES ('9', 'SISTER');
INSERT INTO `relationship` VALUES ('10', 'DAUGHTER');
INSERT INTO `relationship` VALUES ('11', 'WIFE');
INSERT INTO `relationship` VALUES ('12', 'UNCLE');
INSERT INTO `relationship` VALUES ('13', 'NIECE');
INSERT INTO `relationship` VALUES ('14', 'PARENT');
INSERT INTO `relationship` VALUES ('15', 'FATHER');
INSERT INTO `relationship` VALUES ('16', 'KID');
INSERT INTO `relationship` VALUES ('17', 'YOUNGER BROTHER');
INSERT INTO `relationship` VALUES ('19', 'COUSIN');
INSERT INTO `relationship` VALUES ('20', 'NEPHEW');
INSERT INTO `relationship` VALUES ('22', 'FAMILY');
INSERT INTO `relationship` VALUES ('23', 'FIANCE');
INSERT INTO `relationship` VALUES ('24', 'ELDER SISTER');
INSERT INTO `relationship` VALUES ('25', 'BLOOD BROTHER');
INSERT INTO `relationship` VALUES ('26', 'GUARDIAN UNCLE');
INSERT INTO `relationship` VALUES ('27', 'ELDER BROTHER');

-- ----------------------------
-- Table structure for `religion`
-- ----------------------------
DROP TABLE IF EXISTS `religion`;
CREATE TABLE `religion` (
  `religion_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `religion` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`religion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of religion
-- ----------------------------
INSERT INTO `religion` VALUES ('1', 'Islam');
INSERT INTO `religion` VALUES ('2', 'Christianity');
INSERT INTO `religion` VALUES ('3', 'Others');

-- ----------------------------
-- Table structure for `schools`
-- ----------------------------
DROP TABLE IF EXISTS `schools`;
CREATE TABLE `schools` (
  `school_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `school_name` varchar(255) DEFAULT NULL,
  `about_us` text NOT NULL,
  `banner_text` text NOT NULL,
  `description` text,
  `state_id` int(255) DEFAULT '0',
  `industry_id` int(255) DEFAULT '0',
  `website` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `range_of_employees` int(255) DEFAULT '0',
  `date_registered` timestamp NULL DEFAULT NULL,
  `date_modified` timestamp NULL DEFAULT NULL,
  `activated_by_id` int(255) DEFAULT '0',
  `string_id` varchar(255) NOT NULL,
  `logo_path` varchar(255) DEFAULT NULL,
  `logo_text` varchar(255) DEFAULT NULL,
  `banner_image_path` varchar(255) DEFAULT NULL,
  `account_type_id` int(255) unsigned NOT NULL,
  `account_manager_id` int(255) unsigned NOT NULL,
  `refid` varchar(255) NOT NULL DEFAULT '41020001',
  `cdn_container` varchar(255) NOT NULL,
  `bg_color` varchar(255) DEFAULT 'rgb(23, 176, 47)',
  `prefix_id` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`school_id`,`string_id`),
  UNIQUE KEY `prefix_id` (`prefix_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of schools
-- ----------------------------
INSERT INTO `schools` VALUES ('1', 'Kings College', 'Kings College', 'Kings College', null, '0', '0', null, '1', '10', null, null, '0', 'kc', null, null, null, '2', '0', '41020001', '', 'rgb(23, 176, 47)', 'KIN');
INSERT INTO `schools` VALUES ('6', 'femo@aol.com', '', '', null, '0', '0', null, '1', '20', '2015-10-05 11:02:26', null, '0', 'advent', null, null, null, '2', '0', '41020001', '', '#5b3939', 'FEM');
INSERT INTO `schools` VALUES ('7', 'Syscope', '', '', null, '0', '0', null, '1', '26', '2015-10-06 12:33:06', null, '0', 'syscope', null, null, null, '1', '0', '41020001', '', '#ad8383', 'SYS');

-- ----------------------------
-- Table structure for `school_modules`
-- ----------------------------
DROP TABLE IF EXISTS `school_modules`;
CREATE TABLE `school_modules` (
  `module_id` int(255) unsigned NOT NULL,
  `school_id` int(255) unsigned NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`module_id`,`school_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of school_modules
-- ----------------------------
INSERT INTO `school_modules` VALUES ('1', '1', '1');
INSERT INTO `school_modules` VALUES ('2', '1', '1');
INSERT INTO `school_modules` VALUES ('3', '1', '1');
INSERT INTO `school_modules` VALUES ('4', '1', '1');
INSERT INTO `school_modules` VALUES ('5', '1', '1');
INSERT INTO `school_modules` VALUES ('6', '1', '1');
INSERT INTO `school_modules` VALUES ('7', '1', '1');
INSERT INTO `school_modules` VALUES ('8', '1', '1');
INSERT INTO `school_modules` VALUES ('10', '1', '1');

-- ----------------------------
-- Table structure for `sms_messages`
-- ----------------------------
DROP TABLE IF EXISTS `sms_messages`;
CREATE TABLE `sms_messages` (
  `message_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(255) unsigned NOT NULL,
  `school_id` int(255) unsigned NOT NULL,
  `sender_id` int(255) unsigned NOT NULL,
  `msg_type` varchar(20) NOT NULL,
  `message` text NOT NULL,
  `date_sent` date NOT NULL,
  `time_sent` time NOT NULL,
  `units` decimal(6,2) DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  `delivery_report` text,
  `phone_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  KEY `module_id` (`module_id`),
  KEY `school_id` (`school_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sms_messages
-- ----------------------------

-- ----------------------------
-- Table structure for `states`
-- ----------------------------
DROP TABLE IF EXISTS `states`;
CREATE TABLE `states` (
  `state_id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `state` varchar(100) NOT NULL,
  `id_country` int(255) unsigned NOT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of states
-- ----------------------------
INSERT INTO `states` VALUES ('1', 'Abia', '154');
INSERT INTO `states` VALUES ('2', 'Abuja', '154');
INSERT INTO `states` VALUES ('3', 'Adamawa', '154');
INSERT INTO `states` VALUES ('4', 'Akwa Ibom', '154');
INSERT INTO `states` VALUES ('5', 'Anambara', '154');
INSERT INTO `states` VALUES ('6', 'Bauchi', '154');
INSERT INTO `states` VALUES ('7', 'Bayelsa', '154');
INSERT INTO `states` VALUES ('8', 'Benue', '154');
INSERT INTO `states` VALUES ('9', 'Borno', '154');
INSERT INTO `states` VALUES ('10', 'Cross River', '154');
INSERT INTO `states` VALUES ('11', 'Delta', '154');
INSERT INTO `states` VALUES ('12', 'Ebonyi', '154');
INSERT INTO `states` VALUES ('13', 'Edo', '154');
INSERT INTO `states` VALUES ('14', 'Ekiti', '154');
INSERT INTO `states` VALUES ('15', 'Enugu', '154');
INSERT INTO `states` VALUES ('16', 'Gombe', '154');
INSERT INTO `states` VALUES ('17', 'Imo', '154');
INSERT INTO `states` VALUES ('18', 'Jigawa', '154');
INSERT INTO `states` VALUES ('19', 'Kaduna', '154');
INSERT INTO `states` VALUES ('20', 'Kano', '154');
INSERT INTO `states` VALUES ('21', 'Katsina', '154');
INSERT INTO `states` VALUES ('22', 'Kebbi', '154');
INSERT INTO `states` VALUES ('23', 'Kogi', '154');
INSERT INTO `states` VALUES ('24', 'Kwara', '154');
INSERT INTO `states` VALUES ('25', 'Lagos', '154');
INSERT INTO `states` VALUES ('26', 'Nassarawa', '154');
INSERT INTO `states` VALUES ('27', 'Niger', '154');
INSERT INTO `states` VALUES ('28', 'Ogun', '154');
INSERT INTO `states` VALUES ('29', 'Ondo', '154');
INSERT INTO `states` VALUES ('30', 'Osun', '154');
INSERT INTO `states` VALUES ('31', 'Oyo', '154');
INSERT INTO `states` VALUES ('32', 'Plateau', '154');
INSERT INTO `states` VALUES ('33', 'Rivers', '154');
INSERT INTO `states` VALUES ('34', 'Sokoto', '154');
INSERT INTO `states` VALUES ('35', 'Taraba', '154');
INSERT INTO `states` VALUES ('36', 'Yobe', '154');
INSERT INTO `states` VALUES ('37', 'Zamfara', '154');

-- ----------------------------
-- Table structure for `students`
-- ----------------------------
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `student_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `admission_no` varchar(45) DEFAULT NULL,
  `admission_date` date DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `class_id` int(10) unsigned DEFAULT NULL,
  `dateof_birth` date DEFAULT NULL,
  `gender_id` int(10) unsigned DEFAULT NULL,
  `blood_group_id` int(10) unsigned DEFAULT NULL,
  `birth_place` varchar(150) DEFAULT NULL,
  `nationality_id` int(10) unsigned DEFAULT NULL,
  `language_id` int(10) unsigned DEFAULT NULL,
  `religion_id` int(10) unsigned DEFAULT NULL,
  `address1` varchar(150) DEFAULT NULL,
  `address2` varchar(150) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `picture` varchar(150) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `school_id` int(10) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `subjects`
-- ----------------------------
DROP TABLE IF EXISTS `subjects`;
CREATE TABLE `subjects` (
  `subject_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(150) DEFAULT NULL,
  `subject_desc` varchar(150) DEFAULT NULL,
  `school_id` int(11) NOT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `subject_teacher`
-- ----------------------------
DROP TABLE IF EXISTS `subject_teacher`;
CREATE TABLE `subject_teacher` (
  `subject_teacher_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject_id` int(10) unsigned DEFAULT NULL,
  `employee_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`subject_teacher_id`),
  KEY `FK_subject_teacher_subject_id` (`subject_id`),
  KEY `FK_subject_teacher_employee_id` (`employee_id`),
  CONSTRAINT `FK_subject_teacher_employee_id` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_subject_teacher_subject_id` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `terms`
-- ----------------------------
DROP TABLE IF EXISTS `terms`;
CREATE TABLE `terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term_name` varchar(45) DEFAULT NULL,
  `term_desc` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`term_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of terms
-- ----------------------------
INSERT INTO `terms` VALUES ('1', '1st', 'First Term');
INSERT INTO `terms` VALUES ('2', '2nd', 'Second Term');
INSERT INTO `terms` VALUES ('3', '3rd', 'Third Term');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `last_visit` datetime DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  `user_type` int(10) unsigned DEFAULT NULL,
  `school_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('4', 'fem001@yahoo.com', '96c98f1469295420ce4661bca997c5e97e66f448', '2015-10-05 11:02:26', null, '1', '1', '1');

-- ----------------------------
-- Table structure for `user_logs`
-- ----------------------------
DROP TABLE IF EXISTS `user_logs`;
CREATE TABLE `user_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) unsigned NOT NULL,
  `message` text CHARACTER SET latin1 NOT NULL,
  `type` int(1) unsigned NOT NULL DEFAULT '1',
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_address` bigint(20) DEFAULT NULL,
  `school_id` int(10) unsigned NOT NULL,
  `user_agent` text,
  `session_id` text,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `url` text,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `token` text,
  `adminid` int(11) DEFAULT NULL,
  `user_visibility` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_logs
-- ----------------------------

-- ----------------------------
-- Table structure for `user_perms`
-- ----------------------------
DROP TABLE IF EXISTS `user_perms`;
CREATE TABLE `user_perms` (
  `user_id` int(255) unsigned NOT NULL,
  `perm_id` int(255) unsigned NOT NULL,
  `module_id` int(255) unsigned NOT NULL,
  `school_id` int(255) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_perms
-- ----------------------------
INSERT INTO `user_perms` VALUES ('4', '1', '1', '1');
