CREATE TABLE IF NOT EXISTS `subjects` (
  `subject_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(150) DEFAULT NULL,
  `subject_desc` varchar(150) DEFAULT NULL,
  `school_id` int(11) NOT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;