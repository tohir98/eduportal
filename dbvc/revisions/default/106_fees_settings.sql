CREATE TABLE IF NOT EXISTS `feeitems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `amount` float NOT NULL,
  `school_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;